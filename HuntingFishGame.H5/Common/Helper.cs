﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Web.UI;
using System.Web.Security;
using System.Configuration;

namespace HuntingFishGame.H5
{
    public static class Helper
    {
        #region 通过键，查询对应配置节点的值
        /// <summary>
        /// 通过键，查询对应配置节点的值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetConfig(string key)
        {
            XmlDocument doc = new XmlDocument();
            string url = System.Web.HttpContext.Current.Server.MapPath("~/Config/Data.config");
            doc.Load(url);
            XmlNode dataNode = doc.SelectSingleNode("data");
            XmlNode GreetingNode = dataNode.SelectSingleNode(key);
            XmlElement xe = (XmlElement)GreetingNode;
            string Value = xe.GetAttribute("value").ToString();
            return Value;
        }
        #endregion

        #region 操作日记
        /// <summary>
        /// 操作日记
        /// </summary>
        /// <param name="logcontent"></param>
        public static void ExtLog(string logcontent)
        {
            LogClass log = new LogClass();
            string url = "log\\" + DateTime.Now.ToString("yyyy-MM-dd") + "log.txt";
            string logContent = logcontent;
            log.WriteLog(logContent, System.Web.HttpContext.Current.Server.MapPath(url));
        }
        #endregion

        #region 弹出对话框
        /// <summary>
        /// 弹出对话框
        /// </summary>
        /// <param name="page"></param>
        /// <param name="msgText"></param>
        public static void JsMsg(this Page page, string msgText)
        {
            string script = "alert('" + msgText + "')";
            page.ClientScript.RegisterStartupScript(page.GetType(), "", script, true);
        }
        #endregion

        #region 判断非法字符，防止SQL注入
        /// <summary>
        /// 判断非法字符，防止SQL注入
        /// </summary>
        /// <param name="str1"></param>
        /// <returns></returns>
        public static bool CheckValSql(string str1)
        {
            string str = "$,*,',#,>,<,%,^,&,*,!,@,~";
            string[] sqlStr = str.Split(',');

            bool flag = false;

            for (var i = 0; i < sqlStr.Length; i++)
            {
                if (str1.Contains(sqlStr[i]))
                {
                    flag = true;
                    break;
                }
            }
            return flag;
        }

        /// <summary>
        /// //检查特殊字符
        /// </summary>
        /// <param name="str1"></param>
        /// <returns></returns>
        public static bool CheckValChar(string str1)
        {
            string str = "#,$,%,^,~,`,!,@,&,(,)";
            string[] sqlStr = str.Split(',');

            bool flag = false;

            for (var i = 0; i < sqlStr.Length; i++)
            {
                if (str1.Contains(sqlStr[i]))
                {
                    flag = true;
                    break;
                }
            }
            return flag;
        }
        #endregion

        #region md5加密
        /// <summary>
        /// MD5加密
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string md5(string str)
        {
            return FormsAuthentication.HashPasswordForStoringInConfigFile(str, "md5").ToLower();
        }
        #endregion
    }
}