﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace HuntingFishGame.H5
{
    /// <summary>
    /// 记录日志
    /// </summary>
    public class LogClass
    {
        #region 写日志(用于跟踪) ＋　WriteLog(string strMemo, string path = "*****")
        /// <summary>
        /// 写日志(用于跟踪)
        /// 如果log的路径修改,更改path的默认值
        /// </summary>
        public void WriteLog(string strMemo, string path)
        {
            string filename = path; 
            StreamWriter sr = null;
            try
            {
                if (!File.Exists(filename))
                {
                    sr = File.CreateText(filename);
                }
                else
                {
                    sr = File.AppendText(filename);
                }
                sr.WriteLine(strMemo);
            }
            catch
            {}
            finally
            {
                if (sr != null)
                    sr.Close();
            }
        }
        #endregion
    }
}