﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HuntingFishGame.H5
{
    /// <summary>
    /// 站点的Url配置
    /// Author：Abel
    /// CreateDate：2014-3-31
    /// </summary>
    public class SiteUrls
    {
        private static volatile SiteUrls _instance = null;
        private static readonly object LockObject = new object();

        /// <summary>
        /// 创建实例
        /// </summary>
        /// <returns></returns>
        public static SiteUrls Instance()
        {
            if (_instance == null)
            {
                lock (LockObject)
                {
                    if (_instance == null)
                    {
                        _instance = new SiteUrls();
                    }
                }
            }
            return _instance;
        }

        private SiteUrls() { }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string ActivityCenter()
        {
            RouteValueDictionary routeValueDictionary = new RouteValueDictionary();
            return UrlHelper.GenerateUrl(null, "Index", "ActivityCenter", routeValueDictionary, RouteTable.Routes, GetRequestContext(), true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public string ActivityDetails(string type)
        {
            RouteValueDictionary routeValueDictionary = new RouteValueDictionary();
            routeValueDictionary.Add("type", type);
            return UrlHelper.GenerateUrl(null, "Index", "ActivityDetails", routeValueDictionary, RouteTable.Routes, GetRequestContext(), true);
        }

        /// <summary>
        /// 成功页
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        public string Success(string title = "")
        {
            RouteValueDictionary routeValueDictionary = new RouteValueDictionary();
            routeValueDictionary.Add("title", title);
            return UrlHelper.GenerateUrl(null, "Success", "Home", routeValueDictionary, RouteTable.Routes, GetRequestContext(), true);
        }

        /// <summary>
        /// 错误页
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        public string Error(string title = "")
        {
            RouteValueDictionary routeValueDictionary = new RouteValueDictionary();
            routeValueDictionary.Add("title", title);
            return UrlHelper.GenerateUrl(null, "Error", "Home", routeValueDictionary, RouteTable.Routes, GetRequestContext(), true);
        }
        #region Helper Methods

        /// <summary>
        /// 获取RequestContext
        /// </summary>
        /// <returns></returns>
        private static RequestContext GetRequestContext()
        {
            HttpContext httpContext = HttpContext.Current;
            if (httpContext == null)
            {
                var httpRequest = new HttpRequest("", "http://a.com/", "");
                var httpResponse = new HttpResponse(new System.IO.StringWriter(new StringBuilder()));
                httpContext = new HttpContext(httpRequest, httpResponse);
            }
            RequestContext requestContext = new RequestContext(new HttpContextWrapper(httpContext), new RouteData());
            return requestContext;
        }

        /// <summary>
        /// 获取url中的查询字符串参数
        /// </summary>
        /// <param name="url">url</param>
        /// <returns></returns>
        public static NameValueCollection ExtractQueryParams(string url)
        {
            int startIndex = url.IndexOf("?");
            NameValueCollection values = new NameValueCollection();
            if (startIndex <= 0)
                return values;
            string[] nameValues = url.Substring(startIndex + 1).Split('&');
            foreach (string s in nameValues)
            {
                string[] pair = s.Split('=');
                string name = pair[0];
                string value = string.Empty;
                if (pair.Length > 1)
                    value = pair[1];
                values.Add(name, value);
            }
            return values;
        }

        /// <summary>
        /// 获取完整的Url
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string FullUrl(string url)
        {
            return url.MainSiteFullUrl();
        }
        #endregion
    }
}