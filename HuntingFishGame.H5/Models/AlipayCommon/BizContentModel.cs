﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HuntingFishGame.H5.Models.AlipayCommon
{
    public class BizContentModel
    {
        public string body { get; set; }
        public string subject { get; set; }
        public string out_trade_no { get; set; }
        public string timeout_express { get; set; }
        public string total_amount { get; set; }
        public string seller_id { get; set; }
        public string auth_token { get; set; }
        public string product_code { get; set; }
        public string goods_type { get; set; }
        public string passback_params { get; set; }
        public string promo_params { get; set; }
        public string extend_params { get; set; }
        public string enable_pay_channels { get; set; }
        public string disable_pay_channels { get; set; }
        public string store_id { get; set; }
    }
}