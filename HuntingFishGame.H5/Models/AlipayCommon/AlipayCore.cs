﻿using Aop.Api;
using Aop.Api.Request;
using Aop.Api.Response;
using BSF.SystemConfig;
using HuntingFishGame.ManageDomain.BLL.GoldShopOrder;
using HuntingFishGame.ManageDomain.Model.GoldShopOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HuntingFishGame.H5.Models.AlipayCommon
{
    public class AlipayCore
    {
        public static string GetAlipayData(string orderId)
        {
            IAopClient client = new DefaultAopClient(AlipayConfig.postUrl, AlipayConfig.appid, AlipayConfig.merchant_private_key, AlipayConfig.format, AlipayConfig.version, AlipayConfig.sign_type, AlipayConfig.alipay_public_key, "UTF-8", false);
            AlipayTradeWapPayRequest request = new AlipayTradeWapPayRequest();
            request.SetNotifyUrl(BSFConfig.H5HostUrl + "/Alipay/AlipayNotify");
            request.SetReturnUrl(BSFConfig.H5HostUrl + "/GoldShopOrder/PaymentSuccess");///GoldShopGoods/index
            request.BizContent = GetBizContent(AlipayConfig.timeout_express, orderId, AlipayConfig.method);
            AlipayTradeWapPayResponse response = client.pageExecute(request);
            string responseData = response.Body.Substring(0, response.Body.IndexOf("<script>"));
            BSF.Log.CommLog.Write(BSFConfig.H5HostUrl + "/Alipay/AlipayNotify");
            return responseData;
        }

        public static string GetBizContent(string timeout_express, string orderId, string method)
        {
            GoldShopOrderModel orderModel = GoldShopOrderBLL.GetInfo(BSFConfig.MainConnectString, orderId);

            string OrderNumber = "alipay" + DateTime.Now.ToString("yyyyMMddHHmmss");
            BizContentModel model = new BizContentModel();
            model.body = orderModel.text;
            model.subject = orderModel.text;
            model.out_trade_no = orderModel.orderid;
            model.timeout_express = timeout_express;
            model.total_amount = orderModel.price;
            model.product_code = method;
            BSF.Log.CommLog.Write(Newtonsoft.Json.JsonConvert.SerializeObject(model));
            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
        }
    }
}