﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HuntingFishGame.H5.Models.WeChatCommon
{
    public class PayUtil
    {
        /// <summary>
        /// 在线支付请求
        /// </summary>
        /// <param name="orderMoney">交易金额(单位:分)</param>                
        /// <param name="payMethod">支付方式(0JsAPI支付，1App支付)</param>
        /// <param name="openId">微信授权用户Id</param>
        /// <param name="traderOrderID">商户交易订单号</param>
        /// <param name="proName">产品名称</param>
        /// <param name="proDesc">产品描述</param>        
        /// <param name="paramStr">附加参数（用于处理支付结果验证）</param>
        /// <param name="notify_url">商户提供的商户前台系统异步支付回调地址</param>          
        public static string PayRequst(int orderMoney, int payMethod, string openId, string traderOrderID, string proName, string proDesc, string paramStr, string notify_url)
        {
            string resultStr = "";

            if (payMethod == 0)
            {
                //JsAPI支付                
                resultStr = WxPayUtil.PayJsApiRequst(orderMoney, openId, traderOrderID, proName, proDesc, paramStr, notify_url);
            }
            else
            {
                //App支付
            }
            return resultStr;
        }
    }
}