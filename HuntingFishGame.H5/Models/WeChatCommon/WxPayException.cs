﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HuntingFishGame.H5.Models.WeChatCommon
{
    public class WxPayException : Exception 
    {
        public WxPayException(string msg)
            : base(msg)
        {

        }
    }
}