﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace HuntingFishGame.H5.Models.WeChatCommon
{
    public class ReqResultNotify
    {
        public ReqResultNotify()
        {
        }

        public void ProcessNotify(HttpRequestBase request)
        {
            WxPayData notifyData = GetNotifyData(request);

            //检查支付结果中transaction_id是否存在
            if (!notifyData.IsSet("transaction_id"))
            {
                //若transaction_id不存在，则立即返回结果给微信支付后台
                WxPayData res = new WxPayData();
                res.SetValue("return_code", "FAIL");
                res.SetValue("return_msg", "支付结果中微信订单号不存在");
                //page.Response.Write(res.ToXml());
                //page.Response.End();
            }

            string transaction_id = notifyData.GetValue("transaction_id").ToString();

            //查询订单，判断订单真实性
            if (!QueryOrder(transaction_id))
            {
                //若订单查询失败，则立即返回结果给微信支付后台
                WxPayData res = new WxPayData();
                res.SetValue("return_code", "FAIL");
                res.SetValue("return_msg", "订单查询失败");
                //page.Response.Write(res.ToXml());
                //page.Response.End();
            }
            //查询订单成功
            else
            {
                WxPayData res = new WxPayData();
                res.SetValue("return_code", "SUCCESS");
                res.SetValue("return_msg", "OK");
                //page.Response.Write(res.ToXml());
                //page.Response.End();
            }
        }

        //查询订单
        private bool QueryOrder(string transaction_id)
        {
            WxPayData req = new WxPayData();
            req.SetValue("transaction_id", transaction_id);
            WxPayData res = WxPayApi.OrderQuery(req);
            if (res.GetValue("return_code").ToString() == "SUCCESS" &&
                res.GetValue("result_code").ToString() == "SUCCESS")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 接收从微信支付后台发送过来的数据并验证签名
        /// </summary>
        /// <returns>微信支付后台返回的数据</returns>
        private WxPayData GetNotifyData(HttpRequestBase request)
        {
            //接收从微信后台POST过来的数据
            System.IO.Stream s = request.InputStream;//page.Request.InputStream;
            int count = 0;
            byte[] buffer = new byte[1024];
            StringBuilder builder = new StringBuilder();
            while ((count = s.Read(buffer, 0, 1024)) > 0)
            {
                builder.Append(Encoding.UTF8.GetString(buffer, 0, count));
            }
            s.Flush();
            s.Close();
            s.Dispose();

            //转换数据格式并验证签名
            WxPayData data = new WxPayData();
            try
            {
                data.FromXml(builder.ToString());
            }
            catch (WxPayException ex)
            {
                //若签名错误，则立即返回结果给微信支付后台
                WxPayData res = new WxPayData();
                res.SetValue("return_code", "FAIL");
                res.SetValue("return_msg", ex.Message);
                //page.Response.Write(res.ToXml());
                //page.Response.End();
            }

            return data;
        }

        /// <summary>
        /// 获取支付返回结果
        /// </summary>
        /// <param name="message">返回结果信息</param>
        /// <returns></returns>
        public WxPayData GetPayResult(HttpRequestBase request, out string message)
        {
            message = "";
            WxPayData notifyData = GetNotifyData(request);


            //检查支付结果中transaction_id是否存在
            if (!notifyData.IsSet("transaction_id"))
            {
                //若transaction_id不存在，则立即返回结果给微信支付后台
                WxPayData res = new WxPayData();
                message = "支付结果中微信订单号不存在";
                res.SetValue("return_code", "FAIL");
                res.SetValue("return_msg", "支付结果中微信订单号不存在");
                //page.Response.Write(res.ToXml());
                //page.Response.End();
            }

            if (notifyData.GetValue("return_code").ToString() == "SUCCESS" &&
                notifyData.GetValue("result_code").ToString() == "SUCCESS")
            {
                return notifyData;
            }
            else
            {
                #region 查询订单业务
                string transaction_id = notifyData.GetValue("transaction_id").ToString();

                WxPayData req = new WxPayData();
                req.SetValue("transaction_id", transaction_id);
                WxPayData res = WxPayApi.OrderQuery(req);
                if (res.GetValue("return_code").ToString() == "SUCCESS" &&
                    res.GetValue("result_code").ToString() == "SUCCESS")
                {
                    return res;
                }
                else
                {
                    return null;
                }

                #endregion
            }
        }
    }
}