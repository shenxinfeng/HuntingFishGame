﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace HuntingFishGame.H5.Models
{
    public class PaymentCommon
    {
        public static SortedDictionary<string, string> GetRequestPost(Controller controller)
        {
            int i = 0;
            SortedDictionary<string, string> sArray = new SortedDictionary<string, string>();
            NameValueCollection coll = null;
            //Load Form variables into NameValueCollection variable.
            coll = controller.Request.Form;

            // Get names of all forms into a string array.
            String[] requestItem = coll.AllKeys;

            for (i = 0; i < requestItem.Length; i++)
            {
                sArray.Add(requestItem[i], controller.Request.Form[requestItem[i]]);
            }

            return sArray;
        }

        /// <summary>
        /// 获取Request.Form或者Request.QueryString的值
        /// </summary>
        /// <param name="controller"></param>
        /// <returns></returns>
        public static SortedDictionary<string, string> GetRequestParam(Controller controller)
        {
            int i = 0;
            SortedDictionary<string, string> sArray = new SortedDictionary<string, string>();
            NameValueCollection coll = null;
            //Load Form variables into NameValueCollection variable.
            coll = controller.Request.Form;
            if (coll.Count == 0)
            {
                coll = null;
                coll = controller.Request.QueryString;
            }

            // Get names of all forms into a string array.
            String[] requestItem = coll.AllKeys;

            for (i = 0; i < requestItem.Length; i++)
            {
                sArray.Add(requestItem[i], controller.Request[requestItem[i]]);
            }

            return sArray;
        }

        public static Dictionary<string, string> GetParam(string querystr, char[] splitmain, char[] splitsub)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            string[] arrparam = querystr.Split(splitmain, StringSplitOptions.RemoveEmptyEntries);
            foreach (string param in arrparam)
            {
                string[] paramkeyval = param.Split(splitsub);
                dict.Add(paramkeyval[0], paramkeyval[1]);
            }
            return dict;
        }

        public static string ConnDic(Dictionary<string, string> para)
        {
            string str = "";
            foreach (KeyValuePair<string, string> kvp in para)
            {
                if (str != "")
                    str += "&";
                str += kvp.Key + "=" + kvp.Value;
            }
            return str;
        }

        private static string[] hexDigits = { "0", "1", "2", "3", "4", "5",
			"6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };
        public static string byteArrayToHexString(byte[] b)
        {
            StringBuilder resultSb = new StringBuilder();
            for (int i = 0; i < b.Length; i++)
                resultSb.Append(byteToHexString(b[i]));

            return resultSb.ToString();
        }

        private static String byteToHexString(byte b)
        {
            int n = b;
            if (n < 0)
                n += 256;
            int d1 = n / 16;
            int d2 = n % 16;
            return hexDigits[d1] + hexDigits[d2];
        }


        /** 获取大写的MD5签名结果 */
        public static string GetMD5(string encypStr, string charset)
        {
            string retStr;
            MD5CryptoServiceProvider m5 = new MD5CryptoServiceProvider();

            //创建md5对象
            byte[] inputBye;
            byte[] outputBye;

            //使用GB2312编码方式把字符串转化为字节数组．
            try
            {
                inputBye = Encoding.GetEncoding(charset).GetBytes(encypStr);
            }
            catch (Exception ex)
            {
                inputBye = Encoding.GetEncoding("GB2312").GetBytes(encypStr);
            }
            outputBye = m5.ComputeHash(inputBye);

            retStr = byteArrayToHexString(outputBye);
            return retStr;
        }


        public static string GetSha1(string encypStr, string charset)
        {
            //string strIN = getstrIN(strIN);
            byte[] aaa = System.Text.Encoding.GetEncoding(charset).GetBytes(encypStr);
            SHA1 sha1 = SHA1.Create();
            byte[] byteText = sha1.ComputeHash(aaa);
            string retStr = byteArrayToHexString(byteText);
            return retStr;
        }


        public static string UrlUperIncode(string value, Encoding enc)
        {
            Encoding.GetEncoding("gb2312");
            string result = System.Web.HttpUtility.UrlEncode(value, enc);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                sb.Append(result[i]);
                if (result[i] == '%')
                {
                    i++;
                    char a = result[i];
                    if (a >= 97 && a <= 122)
                    {
                        a = (char)(a - 32);
                    }
                    sb.Append(a);


                    i++;
                    a = result[i];
                    if (a >= 97 && a <= 122)
                    {
                        a = (char)(a - 32);
                    }
                    sb.Append(a);
                }
            }
            return sb.ToString();
        }
    }
}