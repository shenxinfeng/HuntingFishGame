﻿/*----下载页-----*/
function isWeChat() {
    var o = navigator.userAgent.toLowerCase();
    return "micromessenger" == o.match(/MicroMessenger/i);
}
function share() {
	$(".share").removeClass('active2 hide').addClass('active');
	$(".bg").removeClass('hide');
}
//shebei
var browser = {
    versions: function() {
        var u = navigator.userAgent
          , app = navigator.appVersion;
        return {
            trident: u.indexOf("Trident") > -1,
            presto: u.indexOf("Presto") > -1,
            webKit: u.indexOf("AppleWebKit") > -1,
            gecko: u.indexOf("Gecko") > -1 && u.indexOf("KHTML") == -1,
            mobile: !!u.match(/AppleWebKit.*Mobile.*/) || !!u.match(/AppleWebKit/),
            ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
            mac: u.indexOf("Mac") > -1,
            android: u.indexOf("Android") > -1 || u.indexOf("Linux") > -1,
            iPhone: u.indexOf("iPhone") > -1,
            iPad: u.indexOf("iPad") > -1,
            webApp: u.indexOf("Safari") == -1,
			browser: u.indexOf("Browser") > -1,
        }
    }(),
    language: (navigator.browserLanguage || navigator.language).toLowerCase()
};
var thisCommonJs = {
    is_weixn: function () {
        var ua = navigator.userAgent.toLowerCase();
        if (ua.match(/MicroMessenger/i) == 'micromessenger') {
            return true;
        } else if (ua.match(/weibo/i) == 'weibo') {
            return true;
        } else {
            return false;
        }
    },
    openShowWarning: function () {
        $(".bg").removeClass('hide');
        $(".downright").removeClass('hide');
    }
};


function loadapp(parameters) {
    var isios = navigator.userAgent.toLowerCase().match('iPad')
                  || navigator.userAgent.toLowerCase().match('iphone')
                  || navigator.userAgent.toLowerCase().match('iPod');
    var isandroid = navigator.userAgent.toLowerCase().match('android');
    var isdesktop = !isios && !isandroid;
    if (isios) {
        if (thisCommonJs.is_weixn()) {
            $(".bg").removeClass('hide');
            $(".downright").removeClass('hide');
            $(".downright").html("<img src=\'/content/images/wx_ios.png\'>");
        } else {
            checkapp('shy');//呼出APP
			//location.href = "itms-services://?action=download-manifest&url=https://www.dianyadian.com/down/dianyadian_2.plist";
        }
    } else {
        if (thisCommonJs.is_weixn()) {
            $(".bg").removeClass('hide');
            $(".downright").removeClass('hide');
        } else {
           checkapp(parameters);//呼出APP
		    //location.href = "http://www.dianyadian.com/home/CustomerDown";
        }
    }
}

//repeat load
function onloadDown(){
	var isios = navigator.userAgent.toLowerCase().match('iPad')
                  || navigator.userAgent.toLowerCase().match('iphone')
                  || navigator.userAgent.toLowerCase().match('iPod');
    var isandroid = navigator.userAgent.toLowerCase().match('android');

	if(isios){
		//location.href = "itms-services://?action=download-manifest&url=https://www.dianyadian.com/down/dianyadian_2.plist";
		location.href = "http://www.dianyadian.com/home/CustomerDown";
	}else{
		loadapp();
		// location.href = "http://www.dianyadian.com/home/CustomerDown";
	}
}


//通用下载页面 
function GetDownUrl(dlType){
  var downurl='';
  switch(dlType)
  {
       //case 'dyd':downurl='http://www.dianyadian.com/home/CustomerDown';break;
	   case 'shy':
	   if(GetPhoneType('ios'))
	   {
		   downurl='https://itunes.apple.com/us/app/deep-in-the-sea/id1196123465?l=zh&ls=1&mt=8';
	     //downurl='itms-services://?action=download-manifest&url=https://www.dianyadian.com/down/shy.plist';
	   }
	   else
	   { 
	    downurl='http://down.lydgame.com/app/lieyudao.apk';
	   }
	   break;
  }
  return downurl;
}

//获取移动端类型
function GetPhoneType(phonetype)
{
   switch(phonetype)
   {
    case 'ios':
	 return navigator.userAgent.toLowerCase().match('iPad')
                      || navigator.userAgent.toLowerCase().match('iphone')
                      || navigator.userAgent.toLowerCase().match('iPod');
		break;
	case 'android':		
	case 'desktop':return navigator.userAgent.toLowerCase().match('android');break;
   }
}

//呼出APP
function checkapp(appType) {
    switch(appType)
    {  
        case 'shy':location.href=GetDownUrl(appType);break;
        default:mobileAppInstall.open("dianyadian://", GetDownUrl('dyd'));break;
    }
}
$(document).ready(function() {
   /* var pam=$('#pagetype').val();
    if(pam=='undefined')
        pam='';
    var isios = GetPhoneType('ios');
    var isandroid = GetPhoneType('android');
    var isdesktop =  GetPhoneType('desktop');
    if (!thisCommonJs.is_weixn()) {
        if (isios) {
            checkapp(pam);//呼出APP
        } else {
            checkapp(pam);//呼出APP
        }
    }*/
	

    $(".bg").click(function(){
        $(this).addClass("hide");
        $(".downright").addClass("hide");
    });
    //console.dir($("img[name='imageqrcode']"));
    if ($("img[name='imageqrcode']").length > 0) {//if ($("#imageqrcode").length > 0 ) {
        getqrcodecrossdomain($("img[name='imageqrcode']"));//加载商家二维码
    }
});


var mobileAppInstall = (function () {
    var ua = navigator.userAgent,
            loadIframe,
            win = window;

    function getIntentIframe() {
        if (!loadIframe) {
            var iframe = document.createElement("iframe");
            iframe.style.cssText = "display:none;width:0px;height:0px;";
            document.body.appendChild(iframe);
            loadIframe = iframe;
        }
        return loadIframe;
    }

    function getChromeIntent(url) {
        // 根据自己的产品修改吧
        return "intent://t.qq.com/#Intent;scheme=" + url + ";package=com.tencent.WBlog;end";
    }
    var appInstall = {
        isChrome: ua.match(/Chrome\/([\d.]+)/) || ua.match(/CriOS\/([\d.]+)/),
        isAndroid: ua.match(/(Android);?[\s\/]+([\d.]+)?/),
        timeout: 500,
        /**
         * 尝试跳转appurl,如果跳转失败，进入h5url
         * @param {Object} appurl 应用地址
         * @param {Object} h5url  http地址
         */
        open: function (appurl, h5url) {
            var t = Date.now();
            appInstall.openApp(appurl);
            setTimeout(function () {
                if (Date.now() - t < appInstall.timeout+100) {
                    h5url && appInstall.openH5(h5url);
                }
            }, appInstall.timeout)
        },
        openApp: function (appurl) {
            if (appInstall.isChrome) {
                if (appInstall.isAndroid) {
                    win.location.href = getChromeIntent(appurl);
                } else {
                    win.location.href = appurl;
                }
            } else {
                getIntentIframe().src = appurl;
            }
        },
        openH5: function (h5url) {
            win.location.href = h5url;
        }
    }

    return appInstall;
})();

