﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace HuntingFishGame.H5.Controllers
{
    public class MobileDownloadController : Controller
    {
        /// <summary>
        /// 下载次数
        /// </summary>
        private int downCount = 0;
        // GET: MobileDownload
        public ActionResult Index()
        {
            Write();
            ViewData["downCount"] = downCount;
            return View();
        }

        public string Read(string path)
        {
            string value = string.Empty;
            StreamReader sr = new StreamReader(path, Encoding.Default);
            String line;
            while ((line = sr.ReadLine()) != null)
            {
                value = line.ToString();
            }
            sr.Close();
            return value;
        }
        public void Write()
        {
            string url = Server.MapPath("~/Config/DownNum.txt");
            string readValue = Read(url);
            if (!string.IsNullOrEmpty(readValue))
            {
                downCount = int.Parse(readValue) + 1;
                FileStream fs = new FileStream(url, FileMode.Create);
                //获得字节数组
                byte[] data = System.Text.Encoding.Default.GetBytes(downCount.ToString());
                //开始写入
                fs.Write(data, 0, data.Length);
                //清空缓冲区、关闭流
                fs.Flush();
                fs.Close();
            }
        }

    }
}