﻿using HuntingFishGame.H5.Models;
using HuntingFishGame.ManageDomain.BLL.GoldShopGoods;
using HuntingFishGame.ManageDomain.Model.GoldShopGoods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BSF.SystemConfig;
using Bronze.Components.OAuth;
using HuntingFishGame.ManageDomain.BLL.User;
using HuntingFishGame.ManageDomain.Model.User;

namespace HuntingFishGame.H5.Controllers
{
    public class GoldShopGoodsController : Controller
    {
        //
        // GET: /GoldShopGoods/

        public ActionResult Index(string token)
        {
            //根据token获取在线用户信息
            OnlineUserModel tempUser = OnlineUserBLL.GetInfoByToken(BSFConfig.MainConnectString, token);
            BSF.Log.CommLog.Write(BSFConfig.MainConnectString);
            BSF.Log.CommLog.Write(token);
            if (tempUser != null)
            {
                ViewBag.openid = tempUser.UserId;
            }
            else 
            {
                ViewBag.openid = "";
            }
            List<GoldShopGoodsModel> list = GoldShopGoodsBLL.GetList(BSFConfig.MainConnectString, true);
            return View(list);
        }
    }
}
