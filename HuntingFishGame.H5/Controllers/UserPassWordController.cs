﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Bronze.Components;
using Bronze.Components.Security;
using BSF.Enums;
using BSF.SystemConfig;
using HuntingFishGame.H5.Models;
using HuntingFishGame.ManageDomain.BLL.User;
using HuntingFishGame.ManageDomain.Model.User;
using Newtonsoft.Json;

namespace HuntingFishGame.H5.Controllers
{
    public class UserPassWordController : Controller
    {
        //
        // GET: /UserPassWord/

        public ActionResult Index(string token = "")
        {
            BSF.Log.CommLog.Write("UserPassWord/Index->token：" + token);
            //根据token获取在线用户信息
            OnlineUserModel tempUser = OnlineUserBLL.GetInfoByToken(BSFConfig.MainConnectString, token);
            string userInfo = tempUser == null? "" : JsonConvert.SerializeObject(tempUser);
            BSF.Log.CommLog.Write("UserPassWord/Index->tempUser：" + userInfo);
            if (tempUser != null)
            {
                int count = 0;
                var userList = WxCodeBLL.GetList(BSFConfig.GameConnectString, tempUser.UserId, 1, 20, out count);
                return View(userList);
            }
            else
            {
                return View(new List<WxCodeModel>());
            }     
        }

        /// <summary>
        /// 修改登录密码
        /// </summary>
        [HttpPost]
        public ActionResult UpdateLoginPwd(UpdateLoginParamModel model)
        {
            //if (!ModelState.IsValid)
            //    return Json(new { code = (int)BSFAPICode.NormalError ,msg = "请检查您录入的信息是否正确" });
            //TODO 1.根据账号获取用户信息,2.验证旧密码是否正确,3.修改新密码
            bool state = UserPasswordBLL.UpdateLoginPwd(BSFConfig.GameConnectString, model.UserId, model.OldPwd, model.NewPwd);
            if (state)
            {
                return Json(new { code = (int)BSFAPICode.Success, msg = "修改成功", ReturnUrl = "" });
            }
            else
            {
                return Json(new { code = (int)BSFAPICode.NormalError, msg = "修改失败", ReturnUrl = "" });
            }
        }

        /// <summary>
        /// 修改银行密码
        /// </summary>
        [HttpPost]
        public ActionResult UpdateBankSubmit(UpdateLoginParamModel model)
        {
            //if (!ModelState.IsValid)
            //    return Json(new { code = (int)BSFAPICode.NormalError ,msg = "请检查您录入的信息是否正确" });
            //TODO 1.根据账号获取用户信息,2.验证旧密码是否正确,3.修改新密码
            bool state = UserPasswordBLL.UpdateBankPwd(BSFConfig.GameConnectString, model.UserId, model.OldPwd, model.NewPwd);
            if (state)
            {
                return Json(new { code = (int)BSFAPICode.Success, msg = "修改成功", ReturnUrl = "" });
            }
            else
            {
                return Json(new { code = (int)BSFAPICode.NormalError, msg = "修改失败", ReturnUrl = "" });
            }
        }
        
    }
}
