﻿using Aop.Api.Util;
using BSF.SystemConfig;
using HuntingFishGame.H5.Models;
using HuntingFishGame.H5.Models.AlipayCommon;
using HuntingFishGame.H5.Models.WeChatCommon;
using HuntingFishGame.ManageDomain.BLL.GoldShopOrder;
using HuntingFishGame.ManageDomain.Model.GoldShopOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuntingFishGame.H5.Controllers
{
    public class AlipayController : Controller
    {
        public ActionResult OrderPayRequest(string orderId, string toUrl)
        {
            ViewBag.orderId = orderId;
            //JSAPI支付预处理
            return View();
        }

        public JsonResult GetAlipayData(string orderId)
        {
            string alipayData = AlipayCore.GetAlipayData(orderId);
            return Json(new { alipayData = alipayData });
        }

        public string AlipayNotify()
        {
            BSF.Log.CommLog.Write("CallBack" + DateTime.Now);

            SortedDictionary<string, string> para = null;
            SortedDictionary<string, string> para2 = null;
            try
            {
                para = PaymentCommon.GetRequestPost(this);
                para2 = PaymentCommon.GetRequestPost(this);
                //para2 = PaymentCommon.GetRequestPost(this);
                BSF.Log.CommLog.Write(Newtonsoft.Json.JsonConvert.SerializeObject(para));
                Notify notify = new Notify();
                string signContent = AlipaySignature.GetSignContent(para);
                //这个是新版本的验证签名
                BSF.Log.CommLog.Write("check111:" );
                //这个是旧版本的方法
                if (AlipaySignature.RSACheckV1(para, AlipayConfig.alipay_public_key, AlipayConfig.input_charset, AlipayConfig.sign_type, false))//notify.Verify(para2, Request.Form["notify_id"], Request.Form["sign"]))
                {
                    BSF.Log.CommLog.Write("1");
                    if (para["trade_status"] == "TRADE_SUCCESS" || para["trade_status"] == "TRADE_FINISHED")
                    {
                        BSF.Log.CommLog.Write("2");
                        string tid = (para["out_trade_no"]).ToString();
                        bool isSend = GoldShopOrderBLL.GetPayStatus(BSFConfig.MainConnectString, tid);//是否已推送公众号提醒
                        BSF.Log.CommLog.Write("3");
                        if (!isSend)
                        {
                            BSF.Log.CommLog.Write("4");
                            GoldShopOrderModel payModel = new GoldShopOrderModel()
                            {
                                orderid = tid.ToString(),
                                payment_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                payment_number = para["buyer_id"].ToString(),
                                payment_orderid = para["trade_no"].ToString(),
                                payment_status = "1",
                                payment_type = "1"
                            };

                            bool respay = GoldShopOrderBLL.Update(BSFConfig.MainConnectString, payModel);
                            BSF.Log.CommLog.Write("5：" + respay);
                            if (!respay)
                            {
                                //业务数据处理失败
                                return "failed";
                            }
                            else
                            {
                                //公众号消息推送(只推送已关注公众号的用户)
                                //XXF.Log.CommLog.Write("商品名称字符集合：" + spmcstr);
                                if (!string.IsNullOrEmpty(""))
                                {
                                    //推送成功消息
                                }
                            }
                        }
                        return "success";
                    }
                    else
                    {
                        return "failed";
                    }
                    //正常，写数据
                }
                else
                {
                    throw new Exception("失败！");
                }
            }
            catch (Exception ex)
            {
                BSF.Log.ErrorLog.Write(ex.Message, ex);
                Console.WriteLine(ex.Message);
                return "failed";
                //throw new Exception("服务器出错" + ex.Message);
            }
        }
    }
}