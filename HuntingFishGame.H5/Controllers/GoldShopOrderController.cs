﻿using HuntingFishGame.H5.Models;
using HuntingFishGame.ManageDomain.BLL.GoldShopGoods;
using HuntingFishGame.ManageDomain.Model.GoldShopGoods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BSF.SystemConfig;
using HuntingFishGame.ManageDomain.Model.GoldShopOrder;
using HuntingFishGame.ManageDomain.BLL.GoldShopOrder;

namespace HuntingFishGame.H5.Controllers
{
    public class GoldShopOrderController : Controller
    {
        public ActionResult CreateOrder(long id, string openid)
        {
            GoldShopGoodsModel goodModel = GoldShopGoodsBLL.GetInfo(BSFConfig.MainConnectString, id);

            DateTime nowDate = DateTime.Now;
            if (Convert.ToDateTime(goodModel.starttime) <= nowDate && Convert.ToDateTime(goodModel.endtime) >= nowDate)
            {
                Random ran = new Random();
                int RandKey = ran.Next(100, 999);
                GoldShopOrderModel model = new GoldShopOrderModel();
                model.gid = goodModel.id.ToString();
                model.gift_gold = goodModel.giftgold;
                model.create_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                model.gold = goodModel.gold;
                model.openid = openid;
                model.orderid = DateTime.Now.ToString("yyyyMMdd") + model.openid.GetHashCode().ToString() + RandKey;
                model.payment_status = "0";
                model.price = goodModel.price;
                model.text = goodModel.text;
                model.userid = "";
                bool check = GoldShopOrderBLL.Add(BSFConfig.MainConnectString, model);
                if (check)
                {
                    return Json(new { code = 1, msg = "success", orderId = model.orderid });
                }
                else
                {
                    return Json(new { code = -1, msg = "生成订单失败！" });
                }
            }
            else
            {
                return Json(new { code = -1, msg = "购买商品已过期！" });
            }
        }

        public ActionResult PaymentPage(string orderId, string openid)
        {
            ViewBag.openid = openid;
            GoldShopOrderModel orderModel = GoldShopOrderBLL.GetInfo(BSFConfig.MainConnectString, orderId);
            return View(orderModel);
        }

        public ActionResult PaymentError()
        {
            return View();
        }

        public ActionResult PaymentSuccess()
        {
            return View();
        }
    }
}
