﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntingFishGame.Domain.Model.User
{
    public class UserModel
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 登录密码
        /// </summary>
        public string Pass { get; set; }
        /// <summary>
        /// 银行密码
        /// </summary>
        public string TwoPassWord { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// token
        /// </summary>
        public string Token { get; set; }

        public static UserModel CreateModel(DataRow dr)
        {
            return new UserModel()
            {
                UserId = Convert.ToInt64(dr["UserId"]),
                UserName = dr["UserName"].ToString(),
                Pass = dr["Pass"].ToString(),
                TwoPassWord = dr["TwoPassWord"].ToString(),
                NickName = dr["NickName"].ToString(),
                Token = dr["Token"].ToString()

            };
        }
    }
}
