﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntingFishGame.Domain.Model.WeChat
{
    public class WechatFollowModel
    {
        public long Id { get; set; }

        [DisplayName("第三方app唯一标识")]//, Display(Order = 10)
        public string AppId { get; set; }

        [DisplayName("第三方用户唯一标识")]
        public string OpenId { get; set; }

        [DisplayName("用户授权的作用域")]
        public string OauthScope { get; set; }

        [DisplayName("是否授权")]
        public bool IsAuth { get; set; }

        [DisplayName("是否关注")]
        public bool IsSubscribe { get; set; }

        [DisplayName("关注时间")]
        public DateTime SubscribeTime { get; set; }

        [DisplayName("取消关注时间")]
        public DateTime UnSubscribeTime { get; set; }

        [DisplayName("性别")]
        public int Sex { get; set; }

        [DisplayName("微信昵称")]
        public string NickName { get; set; }

        [DisplayName("省")]
        public string Province { get; set; }

        [DisplayName("市")]
        public string City { get; set; }

        [DisplayName("区")]
        public string Country { get; set; }

        [DisplayName("头像")]
        public string HeadImgUrl { get; set; }

        [DisplayName("绑定id")]
        public string UnionId { get; set; }

        [DisplayName("用户特权信息json 数组")]
        public string Privilege { get; set; }

        [DisplayName("创建时间")]
        public DateTime CreateTime { get; set; }

        [DisplayName("修改时间")]
        public DateTime UpdateTime { get; set; }

        [DisplayName("修改次数")]
        public int Updatecs { get; set; }

        [DisplayName("是否删除")]
        public bool IsDeleted { get; set; }

        public static WechatFollowModel CreateModel(DataRow dr)
        {
            return new WechatFollowModel()
            {
                Id = Convert.ToInt64(dr["ID"]),
                AppId = dr["AppId"].ToString(),
                OpenId = dr["OpenId"].ToString(),
                OauthScope = dr["OauthScope"].ToString(),
                IsAuth = Convert.ToBoolean(dr["IsAuth"]),
                IsSubscribe = Convert.ToBoolean(dr["IsSubscribe"]),
                SubscribeTime = Convert.ToDateTime(dr["SubscribeTime"]),
                UnSubscribeTime = Convert.ToDateTime(dr["UnSubscribeTime"]),
                Sex = Convert.ToInt32(dr["Sex"]),
                NickName = dr["NickName"].ToString(),
                Province = dr["Province"].ToString(),
                City = dr["City"].ToString(),
                Country = dr["Country"].ToString(),
                HeadImgUrl = dr["HeadImgUrl"].ToString(),
                UnionId = dr["UnionId"].ToString(),
                Privilege = dr["Privilege"].ToString(),
                CreateTime = Convert.ToDateTime(dr["CreateTime"]),
                UpdateTime = Convert.ToDateTime(dr["UpdateTime"]),
                Updatecs = Convert.ToInt32(dr["Updatecs"]),
                IsDeleted = Convert.ToBoolean(dr["IsDeleted"])
            };
        }
    }
}
