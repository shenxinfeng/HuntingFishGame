﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HuntingFishGame.Domain.Model.WeChat
{
    /// <summary>
    /// scancode_waitmsg：扫码推事件且弹出“消息接收中”提示框<br />
    /// 用户点击按钮后，微信客户端将调起扫一扫工具，完成扫码操作后，将扫码的结果传给开发者，同时收起扫一扫工具，然后弹出“消息接收中”提示框，随后可能会收到开发者下发的消息。<br />
    /// Scancode_waitmsgButton-->SingleButton-->BaseButton
    /// </summary>
    public class Scancode_waitmsgButton : SingleButton
    {
        public override string type
        {
            get { return "scancode_waitmsg"; }
        }
        /// <summary>
        /// scancode_push类型必须.菜单KEY值，用于消息接口推送，不超过128字节
        /// </summary>
        public string key { get; set; }
    }
}
