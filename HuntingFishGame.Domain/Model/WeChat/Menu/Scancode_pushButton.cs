﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HuntingFishGame.Domain.Model.WeChat
{
    /// <summary>
    /// scancode_push：扫码推事件<br />
    /// 用户点击按钮后，微信客户端将调起扫一扫工具，完成扫码操作后显示扫描结果（如果是URL，将进入URL），且会将扫码的结果传给开发者，开发者可以下发消息。<br />
    /// Scancode_pushButton-->SingleButton-->BaseButton
    /// </summary>
    public class Scancode_pushButton:SingleButton
    {
        public override string type
        {
            get { return "scancode_push"; }
        }
        /// <summary>
        /// scancode_push类型必须.菜单KEY值，用于消息接口推送，不超过128字节
        /// </summary>
        public string key { get; set; }
    }
}
