﻿
namespace HuntingFishGame.Domain.Model.WeChat
{
    /// <summary>
    /// view：跳转URL<br />
    /// 用户点击view类型按钮后，微信客户端将会打开开发者在按钮中填写的网页URL，可与网页授权获取用户基本信息接口结合，获得用户基本信息。<br />
    /// ViewButton-->SingleButton-->BaseButton
    /// </summary>
    public class ViewButton:SingleButton
    {
        public override string type
        {
            get { return "view"; }
        }
        /// <summary>
        /// view类型必须.网页链接，用户点击菜单可打开链接，不超过256字节
        /// </summary>
        public string url { get; set; }
    }
}
