﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HuntingFishGame.Domain.Model.WeChat
{
    /// <summary>
    /// pic_weixin:弹出微信相册发图器<br />
    /// 用户点击按钮后，微信客户端将调起微信相册，完成选择操作后，将选择的相片发送给开发者的服务器，并推送事件给开发者，同时收起相册，随后可能会收到开发者下发的消息。<br />
    /// Pic_weixinButton-->SingleButton-->BaseButton
    /// </summary>
    public class Pic_weixinButton : SingleButton
    {
        public override string type
        {
            get { return "pic_weixin"; }
        }
        /// <summary>
        /// scancode_push类型必须.菜单KEY值，用于消息接口推送，不超过128字节
        /// </summary>
        public string key { get; set; }
    }
}
