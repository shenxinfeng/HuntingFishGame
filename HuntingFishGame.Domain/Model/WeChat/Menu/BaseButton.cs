﻿using System.Collections.Generic;

namespace HuntingFishGame.Domain.Model.WeChat
{
    public abstract class BaseButton
    {
        /// <summary>
        /// 菜单标题，不超过16个字节，子菜单不超过40个字节
        /// </summary>
        public string name { get; set; }

        public List<SingleButton> sub_button { get; set; }
    }
}
