﻿using System.Collections.Generic;

namespace HuntingFishGame.Domain.Model.WeChat
{
    /// <summary>
    /// 二级菜单按钮数组，个数应为1~5个 
    /// </summary>
    public class MultiButton : BaseButton
    {
        public static MultiButton _model = new MultiButton();
        public List<SingleButton> sub_button = new List<SingleButton>();

        public void AddClickButton(ClickButton clickBtn)
        {
            sub_button.Add(clickBtn);
        }

        public void AddViewButton(ViewButton viewBtn)
        {
            sub_button.Add(viewBtn);
        }

        public void AddScancode_pushButton(Scancode_pushButton scancode_pushBtn)
        {
            sub_button.Add(scancode_pushBtn);
        }

        public void AddScancode_waitmsgButton(Scancode_waitmsgButton scancode_waitmsgBtn)
        {
            sub_button.Add(scancode_waitmsgBtn);
        }

        public void AddPic_sysphotoButton(Pic_sysphotoButton pic_sysphotoBtn)
        {
            sub_button.Add(pic_sysphotoBtn);
        }

        public void AddPic_photo_or_albumButton(Pic_photo_or_albumButton pic_photo_or_albumBtn)
        {
            sub_button.Add(pic_photo_or_albumBtn);
        }

        public void AddPic_weixinButton(Pic_weixinButton pic_weixinBtn)
        {
            sub_button.Add(pic_weixinBtn);
        }

        public void AddLocation_selectButton(Location_selectButton location_selectBtn)
        {
            sub_button.Add(location_selectBtn);
        }

    }
}
