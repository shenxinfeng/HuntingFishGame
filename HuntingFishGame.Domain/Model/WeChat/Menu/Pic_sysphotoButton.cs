﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HuntingFishGame.Domain.Model.WeChat
{
    /// <summary>
    /// pic_sysphoto:弹出系统拍照发图<br />
    /// 用户点击按钮后，微信客户端将调起系统相机，完成拍照操作后，会将拍摄的相片发送给开发者，并推送事件给开发者，同时收起系统相机，随后可能会收到开发者下发的消息。<br />
    /// Pic_sysphotoButton-->SingleButton-->BaseButton
    /// </summary>
    public class Pic_sysphotoButton : SingleButton
    {
        public override string type
        {
            get { return "pic_sysphoto"; }
        }
        /// <summary>
        /// scancode_push类型必须.菜单KEY值，用于消息接口推送，不超过128字节
        /// </summary>
        public string key { get; set; }
    }
}
