﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HuntingFishGame.Domain.Model.WeChat
{
    /// <summary>
    /// pic_photo_or_album:弹出拍照或者相册发图<br />
    /// 用户点击按钮后，微信客户端将弹出选择器供用户选择“拍照”或者“从手机相册选择”。用户选择后即走其他两种流程。<br />
    /// Pic_photo_or_albumButton-->SingleButton-->BaseButton
    /// </summary>
    public class Pic_photo_or_albumButton : SingleButton
    {
        public override string type
        {
            get { return "pic_photo_or_album"; }
        }
        /// <summary>
        /// scancode_push类型必须.菜单KEY值，用于消息接口推送，不超过128字节
        /// </summary>
        public string key { get; set; }
    }
}
