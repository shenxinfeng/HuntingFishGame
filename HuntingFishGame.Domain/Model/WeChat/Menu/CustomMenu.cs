﻿using System.Collections.Generic;
using BSF.Tool;

namespace HuntingFishGame.Domain.Model.WeChat
{
    /// <summary>
    /// 一级菜单按钮数组，个数应为1~3个
    /// </summary>
    public class CustomMenu
    {
        public static CustomMenu _menumodel = new CustomMenu();
        public List<BaseButton> button = new List<BaseButton>();

        //public void AddMulitButton(MultiButton multiBtn)
        //{
        //    button.Add(multiBtn);
        //}

        public void AddClickButton(ClickButton clickBtn)
        {
            button.Add(clickBtn);
        }

        public void AddViewButton(ViewButton viewBtn)
        {
            button.Add(viewBtn);
        }

        public void AddScancode_pushButton(Scancode_pushButton scancode_pushBtn)
        {
            button.Add(scancode_pushBtn);
        }

        public void AddScancode_waitmsgButton(Scancode_waitmsgButton scancode_waitmsgBtn)
        {
            button.Add(scancode_waitmsgBtn);
        }

        public void AddPic_sysphotoButton(Pic_sysphotoButton pic_sysphotoBtn)
        {
            button.Add(pic_sysphotoBtn);
        }

        public void AddPic_photo_or_albumButton(Pic_photo_or_albumButton pic_photo_or_albumBtn)
        {
            button.Add(pic_photo_or_albumBtn);
        }

        public void AddPic_weixinButton(Pic_weixinButton pic_weixinBtn)
        {
            button.Add(pic_weixinBtn);
        }

        public void AddLocation_selectButton(Location_selectButton location_selectBtn)
        {
            button.Add(location_selectBtn);
        }

        public virtual string GetJSON()
        {
            return JsonHelper.ToJson(this);
        }
    }
}
