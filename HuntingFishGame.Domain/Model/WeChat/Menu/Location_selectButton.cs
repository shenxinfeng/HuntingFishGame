﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HuntingFishGame.Domain.Model.WeChat
{
    /// <summary>
    /// location_select:弹出地理位置选择器<br />
    /// 用户点击按钮后，微信客户端将调起地理位置选择工具，完成选择操作后，将选择的地理位置发送给开发者的服务器，同时收起位置选择工具，随后可能会收到开发者下发的消息。<br />
    /// Location_selectButton-->SingleButton-->BaseButton
    /// </summary>
    public class Location_selectButton : SingleButton
    {
        public override string type
        {
            get { return "location_select"; }
        }
        /// <summary>
        /// scancode_push类型必须.菜单KEY值，用于消息接口推送，不超过128字节
        /// </summary>
        public string key { get; set; }
    }
}
