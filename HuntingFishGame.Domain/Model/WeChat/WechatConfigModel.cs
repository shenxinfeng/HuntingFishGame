﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntingFishGame.Domain.Model.WeChat
{
    public class WeChatConfigModel
    {
        public long ID { get; set; }

        [DisplayName("官方订阅号Id")]
        public Guid OfficialAccount { get; set; }

        [DisplayName("微信类型(枚举:WeixinType,1-服务号,2-订阅号)")]
        public int WechatType { get; set; }

        [DisplayName("参数键")]
        public string ParamKey { get; set; }

        [DisplayName("参数值")]
        public string ParamValue { get; set; }

        [DisplayName("过期时间")]
        public DateTime ExpirationTime { get; set; }

        public static WeChatConfigModel CreateModel(DataRow dr)
        {
            return new WeChatConfigModel()
            {
                ID = Convert.ToInt64(dr["ID"]),
                OfficialAccount = Guid.Parse(dr["OfficialAccount"].ToString()),
                WechatType = Convert.ToInt32(dr["WechatType"]),
                ParamKey = dr["ParamKey"].ToString(),
                ParamValue = dr["ParamValue"].ToString(),
                ExpirationTime = Convert.ToDateTime(dr["ExpirationTime"])
            };
        }
    }
}
