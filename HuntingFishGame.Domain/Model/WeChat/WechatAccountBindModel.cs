﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSF.Db;

namespace HuntingFishGame.Domain.Model.WeChat
{
    /// <summary>
    /// 实体类tb_WechatAccountBind
    /// </summary>
    public class WechatAccountBindModel
    {
        [DisplayName("序号")]
        public long Id { get; set; }
        [DisplayName("批次号")]
        public string BatchNumber { get; set; }
        [DisplayName("序号")]
        public string OrderNumber { get; set; }
        [DisplayName("账号")]
        public string AccountNumber { get; set; }
        [DisplayName("修改的密码")]
        public string UpdatePassword { get; set; }
        [DisplayName("微信的openId")]
        public string OpenId { get; set; }
        [DisplayName("是否选择该账号")]
        public bool IsChecked { get; set; }
        [DisplayName("创建时间")]
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 状态，枚举：BSFAPICode
        /// </summary>
        [DisplayName("状态")]
        public int Status { get; set; }
        [DisplayName("更新时间")]
        public DateTime UpdateTime { get; set; }


        public static WechatAccountBindModel CreateModel(DataRow dr)
        {
            return new WechatAccountBindModel()
            {
                Id = LibConvert.ObjToInt64(dr["Id"]),
                BatchNumber = dr["BatchNumber"].ToString(),
                OrderNumber = dr["OrderNumber"].ToString(),
                AccountNumber = dr["AccountNumber"].ToString(),
                UpdatePassword = dr["UpdatePassword"].ToString(),
                OpenId = dr["OpenId"].ToString(),
                IsChecked = LibConvert.ObjToBool(dr["IsChecked"]),
                Status = LibConvert.ObjToInt(dr["Status"]),
                CreateTime = LibConvert.StrToDateTime(dr["CreateTime"].ToString()),
                UpdateTime = LibConvert.StrToDateTime(dr["UpdateTime"].ToString())
            };
        }
    }
}
