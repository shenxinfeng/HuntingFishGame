﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSF.Tool;

namespace HuntingFishGame.Domain.Model.WeChat
{
    /// <summary>
    /// 创建二维码ticket
    /// </summary>
    public class QRCodeTicket
    {
        public string ticket { get; set; }
        public int expire_seconds { get; set; }
        public string imgurl { get; set; }

        public ReturnCodeModel error { get; set; }

        public virtual string GetJSON()
        {
            return JsonHelper.ToJson(this);
        }
    }
}
