﻿using System.IO;
using BSF.Tool;

namespace HuntingFishGame.Domain.Model.WeChat
{
    public class DownloadFile
    {
        public Stream Stream { get; set; }
        /// <summary>
        ///  image/jpeg等
        /// </summary>
        public string ContentType { get; set; }
        /// <summary>
        /// 下载多媒体文件的相对路径
        /// </summary>
        public string filepath { get; set; }

        public ReturnCodeModel error { get; set; }

        public virtual string GetJSON()
        {
            return JsonHelper.ToJson(this);
        }
    }
}
