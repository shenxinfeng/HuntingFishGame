﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSF.Tool;

namespace HuntingFishGame.Domain.Model.WeChat
{
    public class FilterMess
    {
        /// <summary>
        /// 群发到的分组的group_id
        /// </summary>
        public string group_id { get; set; }

        /// <summary>
        /// 群发的消息类型，图文消息为mpnews，文本消息为text，语音为voice，音乐为music，图片为image，视频为video
        /// </summary>
        public string msgtype { get; set; }

        /// <summary>
        /// 用于群发的消息的media_id 
        /// </summary>
        public string media_id { get; set; }

        /// <summary>
        /// 视频消息的描述,还有视频消息的标题，视频缩略图的媒体ID  
        /// </summary>
        public string description { get; set; }

        #region 上传图文消息素材 图文消息，一个图文消息支持1到10条图文
        //返回数据示例（正确时的JSON返回结果）：
        //{
        //    "type":"news",
        //    "media_id":"CsEf3ldqkAYJAU6EJeIkStVDSvffUJ54vqbThMgplD-VJXXof6ctX5fI6-aYyUiQ",
        //    "created_at":1391857799
        //}

        /// <summary>
        /// 视频缩略图的媒体ID，图文消息缩略图的media_id，可以在基础支持-上传多媒体文件接口中获得 
        /// </summary>
        public string thumb_media_id { get; set; }

        /// <summary>
        /// 图文消息的作者 
        /// </summary>
        public string author { get; set; }

        /// <summary>
        /// 在图文消息页面点击“阅读原文”后的页面
        /// </summary>
        public string content_source_url { get; set; }

        /// <summary>
        /// 视频消息的标题,图文消息的标题 
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// 图文消息页面的内容，支持HTML标签 
        /// </summary>
        public string content { get; set; }

        /// <summary>
        /// 图文消息的描述 
        /// </summary>
        public string digest { get; set; }

        /// <summary>
        /// 是否显示封面，1为显示，0为不显示 
        /// </summary>
        public string show_cover_pic { get; set; }
        #endregion

        /// <summary>
        /// 用户id(用于根据OpenID列表群发)，一串OpenID列表，OpenID最少个，最多10000个 
        /// </summary>
        public List<string> openid { get; set; }

        public virtual string GetJSON()
        {
            return JsonHelper.ToJson(this);
        }
    }
}
