﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HuntingFishGame.Domain.Model.WeChat
{
    public class ReplyNewsArticle
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string PicUrl { get; set; }
        public string Url { get; set; }

        /// <summary>
        /// 用户发的消息内容
        /// </summary>
        public string xxnr { get; set; }
    }
}
