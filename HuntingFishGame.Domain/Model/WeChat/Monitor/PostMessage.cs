﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HuntingFishGame.Domain.Model.WeChat
{
    public class PostMessage
    {
        /// <summary>
        /// 接收到的消息
        /// </summary>
        public PostMessage()
        {
            Content = "";
            Format = "";
            PicUrl = "";
            MediaId = "";
            Scale = "";
            Label = "";
            Event = "";
            EventKey = "";
        }
        public string ToUserName { get; set; }  //开发者微信号 
        public string FromUserName { get; set; }    //发送方帐号（一个OpenID）
        public int CreateTime { get; set; }    //消息创建时间 （整型） 
        public string MsgType { get; set; } //消息类型

        //接收普通消息，发送被动响应消息（恢复消息），发送客服消息
        public string Content { get; set; } //文本内容,回复文本消息
        public string Format { get; set; }  //语音格式
        public string PicUrl { get; set; }  //图片路径
        public string MediaId { get; set; } //语音内容&&图片id&&视频id,回复的图片id&&语音id&&视频id
        public string ThumbMediaId { get; set; } //视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。 
        public decimal Location_X { get; set; }  //位置x
        public decimal Location_Y { get; set; }  //位置y
        public string Scale { get; set; }   //位置缩放比率
        public string Label { get; set; }   //位置标签
        public string Title { get; set; } //链接消息
        public string Description { get; set; } //链接消息
        public string Url { get; set; } //链接消息

        //Recognition	 接收语音识别结果，UTF8编码
        /// <summary>
        /// 语音识别结果，UTF8编码(开通语音识别功能后才会有)
        /// </summary>
        public string Recognition { get; set; }

        //接收事件推送
        public string Event { get; set; }   //事件类型
        public string EventKey { get; set; }    //事件关键字
        public string Ticket { get; set; }    //二维码的ticket，可用来换取二维码图片
        public string Latitude { get; set; }    //地理位置纬度
        public string Longitude { get; set; }    //地理位置经度
        public string Precision { get; set; }    //地理位置精度
        
        //自定义菜单事件推送


        public string MsgId { get; set; } //public long MsgId { get; set; }   //消息id，64位整型 (事件不包含该信息)
    }
}
