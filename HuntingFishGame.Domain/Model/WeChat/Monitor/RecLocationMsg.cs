﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HuntingFishGame.Domain.Model.WeChat
{
    /// <summary>
    /// 上报地理位置事件，在PostMessage的属性中直接取值。
    /// </summary>
    public class EventLocationMsg
    {
        /// <summary>
        /// 开发者微信号
        /// </summary>
        public string ToUserName { get; set; }
        /// <summary>
        /// 发送方帐号（一个OpenID）
        /// </summary>
        public string FromUserName { get; set; }
        /// <summary>
        /// 消息创建时间 （整型），为时间戳。
        /// </summary>
        public long CreateTime { get; set; }
        /// <summary>
        /// 消息类型，event
        /// </summary>
        //public override string MsgType
        //{
        //    get { return "Event"; }
        //}
        /// <summary>
        /// 事件类型，LOCATION
        /// </summary>
        //public override string Event
        //{
        //    get { return "location"; }
        //}
        /// <summary>
        /// 地理位置纬度
        /// </summary>
        public double Latitude { get; set; }
        /// <summary>
        /// 地理位置经度
        /// </summary>
        public double Longitude { get; set; }
        /// <summary>
        /// 地理位置精度
        /// </summary>
        public double Precision { get; set; }
    }
}
