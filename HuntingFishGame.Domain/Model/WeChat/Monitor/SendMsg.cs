﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSF.Tool;

namespace HuntingFishGame.Domain.Model.WeChat
{
    /// <summary>
    /// 主动发消息。
    /// 当用户主动发消息给公众号的时候（包括发送信息、点击自定义菜单click事件、订阅事件、扫描二维码事件、支付成功事件、用户维权），
    /// 微信将会把消息数据推送给开发者，
    /// 开发者在一段时间内（目前修改为48小时）可以调用客服消息接口，
    /// 通过POST一个JSON数据包来发送消息给普通用户，在48小时内不限制发送次数。
    /// 此接口主要用于客服等有人工消息处理环节的功能，
    /// 方便开发者为用户提供更加优质的服务。
    /// </summary>
    public class SendMsg
    {
        public string touser { get; set; }
        public string msgtype { get; set; }

        public string text { get; set; }//文本消息
        public string media_id { get; set; }//发送的图片的媒体ID,语音的媒体ID,视频的媒体ID
        public string thumb_media_id { get; set; }//视频缩略图的媒体ID&&音乐
        public string musicurl { get; set; }//音乐链接
        public string hqmusicurl { get; set; }//高品质音乐链接，wifi环境优先使用该链接播放音乐
        public string title { get; set; }//视频标题&&音乐&&图文
        public string description { get; set; }//视频描述&&音乐&&图文
        public string url { get; set; }//图文消息点击后跳转的链接 
        public string picurl { get; set; }//图文消息的图片链接，支持JPG、PNG格式，较好的效果为大图640*320，小图80*80 

        public virtual string GetJSON()
        {
            return JsonHelper.ToJson(this);
        }
    }
}
