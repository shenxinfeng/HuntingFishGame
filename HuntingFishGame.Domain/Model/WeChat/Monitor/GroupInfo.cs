﻿using System.Collections.Generic;
using BSF.Tool;

namespace HuntingFishGame.Domain.Model.WeChat
{
    public class GroupInfo
    {
        /// <summary>
        /// 分组id，由微信分配
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 分组名字，UTF8编码
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 分组内用户数量
        /// </summary>
        public int count { get; set; }

        public ReturnCodeModel error { get; set; }

        public virtual string GetJSON()
        {
            return JsonHelper.ToJson(this);
        }
    }

    public class Groups : List<GroupInfo>
    {
        public ReturnCodeModel error { get; set; }

        public virtual string GetJSON()
        {
            return JsonHelper.ToJson(this);
        }
    }

    public class GroupID
    {
        public int id { get; set; }
        public ReturnCodeModel error { get; set; }

        public virtual string GetJSON()
        {
            return JsonHelper.ToJson(this);
        }
    }
}
