﻿using System.Collections.Generic;
using HuntingFishGame.Domain.BLL.Base;
using HuntingFishGame.Domain.DAL.User;
using HuntingFishGame.Domain.Model;

namespace HuntingFishGame.Domain.BLL.User
{
    public class WxCodeBLL : BaseBLL
    {
        private static WxCodeDal wxCodeDal = new WxCodeDal();

        public static List<WxCodeModel> GetList(string connection, string openId, int pageIndex, int pageSize, out int count)
        {
            int _count = 0;
            List<WxCodeModel> list = DbConnAction<List<WxCodeModel>>(connection, (c) =>
            {
                return wxCodeDal.SelectList(c, openId, pageIndex, pageSize, out _count);
            });
            count = _count;
            return list;
        }
    }
}
