﻿using HuntingFishGame.Domain.BLL.Base;
using HuntingFishGame.Domain.DAL.User;
using HuntingFishGame.Domain.Model;
using HuntingFishGame.Domain.Model.User;

namespace HuntingFishGame.Domain.BLL.User
{
    public class UserBLL : BaseBLL
    {
        private static readonly UserDal userDal = new UserDal();

        public static UserModel GetInfo(string connection, string userName)
        {
            return DbConnAction<UserModel>(connection, (c) =>
            {
                return userDal.GetInfo(c, userName);
            });
        }

    }
}
