﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntingFishGame.Domain.BLL.Base;
using HuntingFishGame.Domain.DAL.User;

namespace HuntingFishGame.Domain.BLL.User
{
    /// <summary>
    /// 用户修改密码,解锁相关
    /// </summary>
    public class UserPasswordBLL : BaseBLL
    {
        private static UserPasswordDal userPasswordDal = new UserPasswordDal();

        public static bool UpdateLoginPwd(string connection, long userId, string oldPwd, string newPwd)
        {
            return DbConnAction<bool>(connection, (c) =>
            {
                return userPasswordDal.UpdateLoginPwd(c, userId, oldPwd, newPwd);
            });
        }
        public static bool UpdateBankPwd(string connection, long userId, string oldPwd, string newPwd)
        {
            return DbConnAction<bool>(connection, (c) =>
            {
                return userPasswordDal.UpdateBankPwd(c, userId, oldPwd, newPwd);
            });
        }
        public static int UnBindPc(string connection, string userName, string pwd)
        {
            return DbConnAction<int>(connection, (c) =>
            {
                return userPasswordDal.UnBindPc(c, userName, pwd);
            });
        }
    }
}
