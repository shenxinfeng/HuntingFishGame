﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntingFishGame.Domain.BLL.Base;
using HuntingFishGame.Domain.DAL.WeChat;
using HuntingFishGame.Domain.Model.WeChat;

namespace HuntingFishGame.Domain.BLL.WeChat
{
    /// <summary>
    /// 关注用户
    /// </summary>
    public class WeChatFollowBLL : BaseBLL
    {
        private static readonly WechatFollowDal wechatFollowDal = new WechatFollowDal();

        public static List<WechatFollowModel> GetList(string connection, string openId, out int count, int pageIndex = 1, int pageSize = 10)
        {
            int _count = 0;
            List<WechatFollowModel> list = DbConnAction<List<WechatFollowModel>>(connection, (c) =>
            {
                return wechatFollowDal.GetList(c, openId, out _count, pageIndex, pageSize);
            });
            count = _count;
            return list;
        }

        public static bool Exists(string connection, long id)
        {
            return DbConnAction<bool>(connection, (c) =>
            {
                return wechatFollowDal.Exists(c, id);
            });
        }

        public static WechatFollowModel GetInfo(string connection, long id)
        {
            return DbConnAction<WechatFollowModel>(connection, (c) =>
            {
                return wechatFollowDal.GetInfo(c, id);
            });
        }

        public static WechatFollowModel GetModelDetail(string connection, string openId)
        {
            return DbConnAction<WechatFollowModel>(connection, (c) =>
            {
                return wechatFollowDal.GetModelDetail(c, openId);
            });
        }

        public static long Add(string connection, WechatFollowModel model)
        {
            return DbConnAction(connection, (c) =>
            {
                return wechatFollowDal.Add(c, model);
            });
        }

        public static bool Update(string connection, WechatFollowModel model)
        {
            return DbConnAction(connection, (c) =>
            {
                return wechatFollowDal.Update(c, model);
            });
        }

        public static bool Delete(string connection, long ID)
        {
            return DbConnAction(connection, (c) =>
            {
                return wechatFollowDal.Delete(c, ID);
            });
        }

    }
}
