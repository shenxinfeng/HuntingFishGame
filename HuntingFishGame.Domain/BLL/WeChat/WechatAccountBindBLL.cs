﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSF.Db;
using BSF.Enums;
using HuntingFishGame.Domain.BLL.Base;
using HuntingFishGame.Domain.DAL.WeChat;
using HuntingFishGame.Domain.Model.WeChat;

namespace HuntingFishGame.Domain.BLL.WeChat
{
    /// <summary>
    /// 绑定账号修改密码表
    /// </summary>
    public class WechatAccountBindBLL : BaseBLL
    {
        private static readonly WechatAccountBindDal wechatAccountBindDal = new WechatAccountBindDal();

        public static List<WechatAccountBindModel> GetList(string connection, string batchNumber, out int count, int pageIndex = 1, int pageSize = 10)
        {
            int _count = 0;
            List<WechatAccountBindModel> list = DbConnAction<List<WechatAccountBindModel>>(connection, (c) =>
            {
                return wechatAccountBindDal.GetList(c, batchNumber, out _count, pageIndex, pageSize);
            });
            count = _count;
            return list;
        }

        public static bool Exists(string connection, long id)
        {
            return DbConnAction<bool>(connection, (c) =>
            {
                return wechatAccountBindDal.Exists(c, id);
            });
        }

        public static WechatAccountBindModel GetInfo(string connection, long id)
        {
            return DbConnAction<WechatAccountBindModel>(connection, (c) =>
            {
                return wechatAccountBindDal.GetInfo(c, id);
            });
        }

        public static WechatAccountBindModel GetLastAccountBind(string connection, string openId, string orderNumber)
        {
            return DbConnAction<WechatAccountBindModel>(connection, (c) =>
            {
                return wechatAccountBindDal.GetLastAccountBind(c, openId, orderNumber);
            });
        }

        public static long Add(string connection, WechatAccountBindModel model)
        {
            return DbConnAction(connection, (c) =>
            {
                return wechatAccountBindDal.Add(c, model);
            });
        }

        public static bool Update(string connection, WechatAccountBindModel model)
        {
            return DbConnAction(connection, (c) =>
            {
                return wechatAccountBindDal.Update(c, model);
            });
        }

        /// <summary>
        /// 更新状态
        /// </summary>
        public static bool UpdateStatus(string connection, long id, BSFAPICode status, string statusMemo = "")
        {
            return DbConnAction(connection, (c) =>
            {
                return wechatAccountBindDal.UpdateStatus(c, id, status);
            });
        }

        public static bool Delete(string connection, long id)
        {
            return DbConnAction(connection, (c) =>
            {
                return wechatAccountBindDal.Delete(c, id);
            });
        }

    }
}
