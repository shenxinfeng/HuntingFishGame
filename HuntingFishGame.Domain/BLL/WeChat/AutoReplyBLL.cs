﻿using System;
using System.Collections.Generic;
using HuntingFishGame.Domain.BLL.Base;
using HuntingFishGame.Domain.DAL.WeChat;
using HuntingFishGame.Domain.Model.WeChat;

namespace HuntingFishGame.Domain.BLL.WeChat
{
    /// <summary>
    /// 自动回复
    /// </summary>
    public class AutoReplyBLL : BaseBLL
    {
        private static readonly AutoReplyDal autoReplyDal = new AutoReplyDal();

        public static int TotalItems(string connection, string condition, string query)
        {
            int rows = DbConnAction<int>(connection, (c) =>
            {
                return autoReplyDal.TotalItems(c, condition, query);
            });
            return rows;
        }

        public static List<AutoReplyModel> GetList(string connection, bool? sfdj, DateTime ?cstime, DateTime? cetime, string condition, string query, out int count, int pageIndex = 1, int pageSize = 10)
        {
            int _count = 0;
            List<AutoReplyModel> list = DbConnAction<List<AutoReplyModel>>(connection, (c) =>
            {
                return autoReplyDal.GetList(c, sfdj, cstime, cetime, condition, query, ref _count, pageIndex, pageSize);
            });
            count = _count;
            return list;
        }

        public static bool Exists(string connection, long id)
        {
            return DbConnAction<bool>(connection, (c) =>
            {
                return autoReplyDal.Exists(c, id);
            });
        }

        public static bool Exists(string connection, string keyword, long withoutId = 0)
        {
            return DbConnAction<bool>(connection, (c) =>
            {
                return autoReplyDal.Exists(c, keyword, withoutId);
            });
        }

        public static AutoReplyModel GetInfo(string connection, long id)
        {
            return DbConnAction<AutoReplyModel>(connection, (c) =>
            {
                return autoReplyDal.GetInfo(c, id);
            });
        }

        public static AutoReplyModel GetModelDetail(string connection, string field, string fieldval)
        {
            return DbConnAction<AutoReplyModel>(connection, (c) =>
            {
                return autoReplyDal.DetailsByField(c, field, fieldval);
            });
        }

        public static bool Add(string connection, AutoReplyModel model)
        {
            return DbConnAction(connection, (c) =>
            {
                return autoReplyDal.Add(c, model);
            });
        }

        public static bool Update(string connection, AutoReplyModel model)
        {
            return DbConnAction(connection, (c) =>
            {
                return autoReplyDal.Update(c, model);
            });
        }

        public static bool UpdateState(string connection, long ID, bool sfdj)
        {
            return DbConnAction(connection, (c) =>
            {
                return autoReplyDal.UpdateState(c, ID, sfdj);
            });
        }

        public static bool Delete(string connection, long ID)
        {
            return DbConnAction(connection, (c) =>
            {
                return autoReplyDal.Delete(c, ID);
            });
        }

    }
}
