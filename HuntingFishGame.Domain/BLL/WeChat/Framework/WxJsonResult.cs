﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntingFishGame.Domain.BLL.WeChat
{
    /// <summary>
    /// JSON返回结果（用于菜单接口等）
    /// 基础：errcode , errmsg
    /// </summary>
    public class WxJsonResult
    {
        public ReturnCodeEnum errcode { get; set; }
        public string errmsg { get; set; }

        //public ReturnCodeEnum ReturnCodeEnum
        //{
        //    get
        //    {
        //        try
        //        {
        //            return (ReturnCodeEnum) errorcode;
        //        }
        //        catch
        //        {
        //            return ReturnCodeEnum.系统繁忙;//如果有“其他错误”的话可以指向其他错误
        //        }
        //    }
        //}
    }
}
