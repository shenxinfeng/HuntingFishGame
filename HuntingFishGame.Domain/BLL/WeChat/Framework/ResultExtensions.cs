﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntingFishGame.Domain.BLL.WeChat
{
    public static class ResultExtensions
    {
        // 日志
        //private static Logger logger = LogManager.GetCurrentClassLogger();

        public static bool CheckIsSuccess(this WxJsonResult result)
        {
            if (result == null)
            {
                //logger.Trace("跟踪A：result is null");
            }
            else
            {
                //logger.Trace("跟踪A：" + JsonConvert.SerializeObject(result));
            }
            return result.errcode == ReturnCodeEnum.请求成功;
        }

        public static bool CheckIsSuccess(this OAuthAccessTokenResult result)
        {
            //"errcode":null,"errmsg":null,"
            if (result == null)
            {
                //logger.Trace("跟踪B：result is null");
            }
            else
            {
                //logger.Trace("跟踪B：" + JsonConvert.SerializeObject(result));
            }
            return result.errcode == null || result.errcode == "0" || result.errcode.Equals(ReturnCodeEnum.请求成功.ToString());
        }

    }
}
