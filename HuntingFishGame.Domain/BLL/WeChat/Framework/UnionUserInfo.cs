﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntingFishGame.Domain.BLL.WeChat
{
    /// <summary>
    /// 获取用户基本信息（包括UnionID机制）
    /// </summary>
    public class UnionUserInfo : UserInfo
    {

        public string unionid { get; set; }
        public string remark { get; set; }
        public int groupid { get; set; }

    }
}
