﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntingFishGame.Domain.BLL.WeChat
{
    public class WeChatResponseModel<T>
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        public T data { get; set; }
    }

    public class GetTemplateIdResponse 
    {
        public string template_id { get; set; }
    }

    public class SendMessageResponse
    {
        public string msgid { get; set; }
    }
}
