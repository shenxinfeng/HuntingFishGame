﻿using BSF.Tool;

namespace HuntingFishGame.Domain.BLL.WeChat
{
    public class WeChatSendMessageHelper
    {
        private static string setIndustryUrl = "https://api.weixin.qq.com/cgi-bin/template/api_set_industry?access_token={0}";
        private static string getIndustryUrl = "https://api.weixin.qq.com/cgi-bin/template/get_industry?access_token={0}";
        private static string getTemplateUrl = "https://api.weixin.qq.com/cgi-bin/template/api_add_template?access_token={0}";
        private static string getTemplateListUrl = "https://api.weixin.qq.com/cgi-bin/template/get_all_private_template?access_token={0}";
        private static string delTemplateUrl = "https://api.weixin.qq.com/cgi-bin/template/del_private_template?access_token={0}";
        private static string sendMessageUrl = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={0}";

        /// <summary>设置所属行业</summary>
        /// <param name="accessToken"></param>
        /// <param name="industry_id1"></param>
        /// <param name="industry_id2"></param>
        public static void SetIndustry(string accessToken, string industry_id1, string industry_id2)
        {
            string postData = Newtonsoft.Json.JsonConvert.SerializeObject(new
            {
                industry_id1 = industry_id1,
                industry_id2 = industry_id2
            });
            string url = string.Format(setIndustryUrl, accessToken);
            PostAndGetHelper.HttpPost(url, postData);
        }

        /// <summary>获取所属行业</summary>
        /// <param name="accessToken"></param>
        public static void GetIndustry(string accessToken)
        {
            string url = string.Format(getIndustryUrl, accessToken);
            PostAndGetHelper.HttpGet(url, "");
        }

        /// <summary>获取发送信息用的模板编号</summary>
        /// <param name="accessToken"></param>
        /// <param name="templateIdShort">模板库编号</param>
        /// <returns></returns>
        public static WeChatResponseModel<GetTemplateIdResponse> GetTemplate(string accessToken, string templateIdShort)
        {
            string postData = Newtonsoft.Json.JsonConvert.SerializeObject(new
            {
                template_id_short = templateIdShort
            });
            string url = string.Format(getTemplateUrl, accessToken);
            string response = PostAndGetHelper.HttpPost(url, postData);
            return Newtonsoft.Json.JsonConvert.DeserializeObject<WeChatResponseModel<GetTemplateIdResponse>>(response);
        }

        /// <summary>获取模板集合（无法调试,返回结果格式未知）</summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static object GetAllTemplateList(string accessToken)
        {
            string url = string.Format(getTemplateListUrl, accessToken);
            string response = PostAndGetHelper.HttpGet(url, "");
            return response;
        }

        /// <summary>删除模板</summary>
        /// <param name="accessToken"></param>
        /// <param name="template_id">公众帐号下模板消息ID</param>
        /// <returns></returns>
        public static object DelTemplate(string accessToken, string template_id)
        {
            string postData = Newtonsoft.Json.JsonConvert.SerializeObject(new
            {
                template_id = template_id
            });
            string url = string.Format(delTemplateUrl, accessToken);
            string response = PostAndGetHelper.HttpPost(url, postData);
            return response;
        }

        /// <summary>发送信息</summary>
        /// <param name="accessToken"></param>
        /// <param name="openId"></param>
        /// <param name="templateId"></param>
        /// <param name="data"></param>
        /// <param name="url"></param>
        /// <param name="topcolor"></param>
        /// <returns></returns>
        public static WeChatResponseModel<SendMessageResponse> SendTemplateMessage(string accessToken, string openId, string templateId, object data, string url, string topcolor = "#173177")
        {
            string postUrl = string.Format(sendMessageUrl, accessToken);
            string dataStr = Newtonsoft.Json.JsonConvert.SerializeObject(new
            {
                touser = openId,//用户OPENID
                template_id = templateId,//模板编号
                topcolor = topcolor,//颜色
                url = url,//跳转地址
                data = data//内容（替换模板中.DATA结尾的参数,参考测试方法）
            });
            string response = PostAndGetHelper.HttpPost(postUrl, dataStr.ToString());
            return Newtonsoft.Json.JsonConvert.DeserializeObject<WeChatResponseModel<SendMessageResponse>>(response);
        }

    }
}
