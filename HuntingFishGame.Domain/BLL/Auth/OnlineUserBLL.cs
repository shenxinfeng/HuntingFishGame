﻿using System;
using HuntingFishGame.Domain.BLL.Base;
using HuntingFishGame.Domain.DAL;
using HuntingFishGame.Domain.Model;

namespace HuntingFishGame.Domain.BLL
{
    /// <summary>
    /// 用户登陆
    /// </summary>
    public class OnlineUserBLL : BaseBLL
    {
        private static readonly OnlineUserDal onlineUserDal = new OnlineUserDal();

        //public static List<ProgramMainModel> GetList(string connection, long Mid, string keyword, string CStime, string CEtime, int pageindex, int pagesize, out int count)
        //{
        //    int _count = 0;
        //    List<ProgramMainModel> list = DbConnAction<List<ProgramMainModel>>(connection, (c) =>
        //    {
        //        return programMainDal.GetList(c, Mid, keyword, CStime, CEtime, pagesize, pageindex, out _count);
        //    });
        //    count = _count;
        //    return list;
        //}

        public static bool Exists(string connection, long id)
        {
            return DbConnAction<bool>(connection, (c) =>
            {
                return onlineUserDal.Exists(c, id);
            });
        }

        public static OnlineUserModel GetInfo(string connection, long id)
        {
            return DbConnAction<OnlineUserModel>(connection, (c) =>
            {
                return onlineUserDal.GetInfo(c, id);
            });
        }

        public static OnlineUserModel GetInfoByUserId(string connection, string userId)
        {
            return DbConnAction<OnlineUserModel>(connection, (c) =>
            {
                return onlineUserDal.GetInfoByUserId(c, userId);
            });
        }

        public static OnlineUserModel GetInfoByToken(string connection, string token)
        {
            return DbConnAction<OnlineUserModel>(connection, (c) =>
            {
                return onlineUserDal.GetInfoByToken(c, token);
            });
        }

        public static long Add(string connection, OnlineUserModel model)
        {
            return DbConnAction(connection, (c) =>
            {
                return onlineUserDal.Add(c, model);
            });
        }

        public static bool Update(string connection, OnlineUserModel model)
        {
            return DbConnAction(connection, (c) =>
            {
                return onlineUserDal.Update(c, model);
            });
        }

        /// <summary>
        /// 更新过期时间
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="userId"></param>
        /// <param name="expirationTime"></param>
        /// <param name="isOnline"></param>
        /// <returns></returns>
        public static bool UpdateExpirationTime(string connection, string userId, DateTime expirationTime, bool isOnline = false)
        {
            return DbConnAction(connection, (c) =>
            {
                return onlineUserDal.UpdateExpirationTime(c, userId, expirationTime, isOnline);
            });
        }

        /// <summary>
        /// 更新是否在线,如果在线，则更新最后请求时间
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="userId"></param>
        /// <param name="isOnline"></param>
        /// <returns></returns>
        public static bool UpdateIsOnline(string connection, string userId, bool isOnline = false)
        {
            return DbConnAction(connection, (c) =>
            {
                return onlineUserDal.UpdateIsOnline(c, userId, isOnline);
            });
        }

        public static bool Delete(string connection, long ID)
        {
            return DbConnAction(connection, (c) =>
            {
                return onlineUserDal.Delete(c, ID);
            });
        }

    }
}
