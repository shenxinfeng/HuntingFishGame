﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using BSF.Db;
using HuntingFishGame.Domain.BLL.WeChat;
using HuntingFishGame.Domain.Model.WeChat;

namespace HuntingFishGame.Domain.DAL.WeChat
{
    /// <summary>
    /// 数据访问类:WeChatConfig
	/// </summary>
    public partial class WeChatConfigDal
	{
		/// <summary>
		/// 是否存在该记录
		/// </summary>
        public bool Exists(DbConn pubConn, long ID)
        {
            return SqlHelper.Visit(ps =>
            {
                ps.Add("ID", ID);
                string strSql = "select count(1) from sys_WechatConfig where ID=@ID";
                return LibConvert.ObjToInt(pubConn.ExecuteScalar(strSql.ToString(), ps.ToParameters())) > 0;
            });
        }

		/// <summary>
		/// 增加一条数据
		/// </summary>
        public long Add(DbConn pubConn, WeChatConfigModel model)
        {
            return SqlHelper.Visit(ps =>
            {
                string strSql = @"insert into sys_WechatConfig(OfficialAccount,WechatType,ParamKey,ParamValue,ExpirationTime)
                                    values (@OfficialAccount,@WechatType,@ParamKey,@ParamValue,@ExpirationTime);select @@IDENTITY";
                ps.Add("@OfficialAccount", model.OfficialAccount);
                ps.Add("@WechatType", model.WechatType);
                ps.Add("@ParamKey", model.ParamKey);
                ps.Add("@ParamValue", model.ParamValue);
                ps.Add("@ExpirationTime", model.ExpirationTime);
                string id = pubConn.ExecuteScalar(strSql, ps.ToParameters()).ToString();
                return LibConvert.StrToInt64(id);
            });
        }

		/// <summary>
		/// 更新一条数据
		/// </summary>
        public bool Update(DbConn pubConn, WeChatConfigModel model)
        {
            return SqlHelper.Visit(ps =>
            {
                string strSql = "update sys_WechatConfig set ParamValue=@ParamValue,ExpirationTime=@ExpirationTime where ID=@ID";
                ps.Add("@ParamValue", model.ParamValue);
                ps.Add("@ExpirationTime", model.ExpirationTime);
                ps.Add("@ID", model.ID);
                return pubConn.ExecuteSql(strSql.ToString(), ps.ToParameters()) > 0;
            });
        }

		/// <summary>
		/// 删除一条数据
		/// </summary>
        public bool Delete(DbConn pubConn, long ID)
        {
            return SqlHelper.Visit(ps =>
            {
                string strSql = "delete from sys_WechatConfig where ID=@ID";
                ps.Add("@ID", ID);
                return pubConn.ExecuteSql(strSql.ToString(), ps.ToParameters()) > 0;
            });
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pubConn"></param>
        /// <param name="type"></param>
        /// <param name="count"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public List<WeChatConfigModel> GetList(DbConn pubConn, WeixinType? type, out int count, int pageIndex = 1, int pageSize = 10)
        {
            int _count = 0;
            List<WeChatConfigModel> model = SqlHelper.Visit<List<WeChatConfigModel>>(ps =>
            {
                string sqlwhere = "";
                StringBuilder sql = new StringBuilder();
                sql.Append("select ROW_NUMBER() over(order by T.ID desc) as rownum,T.* from sys_WechatConfig T where 1=1 ");
                if (type.HasValue)
                {
                    ps.Add("WechatType", (int)type);
                    sqlwhere += " and ( T.WechatType =@WechatType )";
                }
                _count = Convert.ToInt32(pubConn.ExecuteScalar("select count(1) from sys_WechatConfig T where 1=1 " + sqlwhere, ps.ToParameters()));
                string sqlSel = "select * from (" + sql + sqlwhere + ") A where rownum between " + ((pageIndex - 1) * pageSize + 1) + " and " + pageSize * pageIndex;
                DataTable table = pubConn.SqlToDataTable(sqlSel, ps.ToParameters());
                List<WeChatConfigModel> list = new List<WeChatConfigModel>();
                foreach (DataRow dr in table.Rows)
                {
                    list.Add(WeChatConfigModel.CreateModel(dr));
                }
                return list;
            });
            count = _count;
            return model;
        }

        /// <summary>获取详情</summary>
        /// <param name="pubConn"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public WeChatConfigModel GetInfo(DbConn pubConn, long id)
        {
            return SqlHelper.Visit(ps =>
            {
                ps.Add("ID", id);
                string sql = "select * from sys_WechatConfig where ID=@ID";
                DataTable table = pubConn.SqlToDataTable(sql, ps.ToParameters());
                if (table != null && table.Rows.Count > 0)
                {
                    return WeChatConfigModel.CreateModel(table.Rows[0]);
                }
                else
                {
                    return null;
                }
            });
        }

        /// <summary>
        /// 获取详情
        /// </summary>
        /// <param name="pubConn"></param>
        /// <param name="paramKey"></param>
        /// <param name="type"></param>
        /// <param name="officialAccount"></param>
        /// <returns></returns>
        public WeChatConfigModel GetModelByParamKey(DbConn pubConn, string paramKey, WeixinType? type = null, Guid? officialAccount = null)
        {
            return SqlHelper.Visit(ps =>
            {
                ps.Add("ParamKey", paramKey);
                string sql = "select top 1 * from sys_WechatConfig where ParamKey=@ParamKey ";
                if (type.HasValue)
                {
                    sql += "and WechatType=@WechatType";
                    ps.Add("WechatType", (int)type);
                }
                if (officialAccount.HasValue)
                {
                    sql += "and OfficialAccount=@OfficialAccount";
                    ps.Add("OfficialAccount", officialAccount);
                }
                DataTable table = pubConn.SqlToDataTable(sql, ps.ToParameters());
                if (table != null && table.Rows.Count > 0)
                {
                    return WeChatConfigModel.CreateModel(table.Rows[0]);
                }
                else
                {
                    return null;
                }
            });
        }
	}
}
