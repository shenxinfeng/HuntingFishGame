﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSF.Db;
using HuntingFishGame.Domain.Model;
using HuntingFishGame.Domain.Model.User;

namespace HuntingFishGame.Domain.DAL.User
{
    public class UserDal
    {

        /// <summary>获取详情</summary>
        /// <param name="pubConn"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public UserModel GetInfo(DbConn pubConn, string userName)
        {
            return SqlHelper.Visit(ps =>
            {
                ps.Add("UserName", userName);
                string sql = "SELECT * FROM TUsers where UserName=@UserName";
                DataTable table = pubConn.SqlToDataTable(sql, ps.ToParameters());
                if (table != null && table.Rows.Count > 0)
                {
                    return UserModel.CreateModel(table.Rows[0]);
                }
                else
                {
                    return null;
                }
            });
        }

    }
}
