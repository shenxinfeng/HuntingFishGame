﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSF.Db;
using BSF.Tool;

namespace HuntingFishGame.Domain.DAL.User
{
    public class UserPasswordDal
    {
        //[SP_ChangeBankPassword] 修改银行密码
        //[SP_UpdateUserPassword] 修改用户的密码
        //[SP_UserBindMobile]绑定微信或解绑的存储过程
        //表是TWx_Code 微信绑定表

        //public static readonly AutoReplyDal Instance = new AutoReplyDal();

        /// <summary>
        /// 修改登录密码
        /// </summary>
        public bool UpdateLoginPwd(DbConn pubConn, long userId, string oldPwd, string newPwd)
        {
            return SqlHelper.Visit(ps =>
            {
                int result = 0;
                ps.Add("UserID", userId);
                ps.Add("UserOldPassword", MD5Helper.Encrypt32MD5(oldPwd));
                ps.Add("UserNewPassword", MD5Helper.Encrypt32MD5(newPwd));
                string strSql = "declare @result varchar(50);exec @result=SP_UpdateUserPasswordByWeiXin @UserID,@UserOldPassword,@UserNewPassword;select @result;";
                DataTable table = pubConn.SqlToDataTable(strSql.ToString(), ps.ToParameters());
                if (table != null && table.Rows.Count > 0)
                {
                    result = LibConvert.ObjToInt(table.Rows[0][0]);
                }
                //string strSql = "SP_UpdateUserPasswordByWeiXin";//Exec SP_UpdateUserPasswordByWeiXin @UserID,@UserOldPassword,@UserNewPassword;->ExecuteScalar，能执行成功，返回的是null
                //int result = LibConvert.ObjToInt(pubConn.ExecuteProcedure(strSql.ToString(), ps.ToParameters()));
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            });
        }

        /// <summary>
        /// 修改银行密码
        /// </summary>
        public bool UpdateBankPwd(DbConn pubConn, long userId, string oldPwd, string newPwd)
        {
            return SqlHelper.Visit(ps =>
            {
                int result = 0;
                ps.Add("UserID", userId);
                ps.Add("MD5PassOld", MD5Helper.Encrypt32MD5(oldPwd));
                ps.Add("MD5PassNew", MD5Helper.Encrypt32MD5(newPwd));
                //string strSql = "SP_ChangeBankPasswd";
                //int result = LibConvert.ObjToInt(pubConn.ExecuteProcedure(strSql.ToString(), ps.ToParameters()));
                string strSql = "declare @result varchar(50);exec @result=SP_ChangeBankPasswd @UserID,@MD5PassOld,@MD5PassNew;select @result;";//这个存储过程是失败了返回1，成功了返回0
                DataTable table = pubConn.SqlToDataTable(strSql.ToString(), ps.ToParameters());
                if (table != null && table.Rows.Count > 0)
                {
                    result = LibConvert.ObjToInt(table.Rows[0][0]);
                }
                if (result == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            });
        }
        /// <summary>
        /// 解锁
        /// </summary>
        /// <param name="pubConn"></param>
        /// <param name="userName"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        /// <remarks>
        /// 如果密码不对，返回6
        /// 如果用户未锁定机器，返回4
        /// 成功，返回3
        /// </remarks>
        public int UnBindPc(DbConn pubConn, string userName, string pwd)
        {
            return SqlHelper.Visit(ps =>
            {
                int result = 0;
                ps.Add("UserName", userName);
                ps.Add("PWD", MD5Helper.Encrypt32MD5(pwd));
                //string strSql = "SP_UserUnlockMathineByWeiXin";
                //int result = LibConvert.ObjToInt(pubConn.ExecuteProcedure(strSql.ToString(), ps.ToParameters()));
                string strSql = "declare @result varchar(50);exec @result=SP_UserUnlockMathineByWeiXin @UserName,@PWD;select @result;";
                DataTable table = pubConn.SqlToDataTable(strSql.ToString(), ps.ToParameters());
                if (table != null && table.Rows.Count > 0)
                {
                    result = LibConvert.ObjToInt(table.Rows[0][0]);
                }
                return result;
            });
        }
    }
}
