﻿using HuntingFishGame.ManageDomain.Model.GoldShopGoods;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSF.Db;
using BSF.SystemConfig;

namespace HuntingFishGame.ManageDomain.DAL.GoldShopGoods
{
    public class GoldShopGoodsDAL
    {
        public List<GoldShopGoodsModel> SelectList(DbConn pubConn, bool fromH5)
        {
            return SqlHelper.Visit(ps =>
            {
                //using (DbConn pubConnS = DbConn.CreateConn(BSFConfig.GameConnectString))
                //{
                //    pubConnS.Open();
                //    ps.Add("@UserID", "13617312");
                //    ps.Add("@UserOldPassword", "edc12a04de0aa8f3c4cf53fa60c3406f");
                //    ps.Add("@UserNewPassword", "59a2dbc136f9e72d88136af72bf6a045");
                //    //object o = pubConnS.ExecuteProcedure("SP_UpdateUserPasswordByWeiXin", ps.ToParameters());
                //    //DataTable o1 = pubConnS.SqlToDataTable("exec SP_UpdateUserPasswordByWeiXin @UserID,@UserOldPassword,@UserNewPassword", ps.ToParameters());
                //    //object o3 = pubConnS.SqlToDataTable("exec SP_UpdateUserPasswordByWeiXin @UserID,@UserOldPassword,@UserNewPassword", ps.ToParameters());
                //    object o4 = pubConnS.SqlToDataTable("declare @result varchar(50) exec @result=SP_UpdateUserPasswordByWeiXin @UserID,@UserOldPassword,@UserNewPassword;select @result", ps.ToParameters());
                //    string a = "";
                //}
                //ps = new SimpleProcedureParameter();
                //ps.Add("@I", "222");
                //object o2 = pubConn.ExecuteProcedure("Pro_StockCheck", ps.ToParameters());
                string sqlWhere = " where 1=1 ";
                if (fromH5)
                {
                    sqlWhere += " and f_starttime<=@f_timetemp and f_endtime>=@f_timetemp";
                    ps.Add("f_timetemp", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                }
                string sql = @"select f_id,f_text,f_gold,f_giftgold,f_price,f_createtime,f_updatetime,f_starttime,f_endtime from tb_goldshop_goods" + sqlWhere;
                DataTable table = pubConn.SqlToDataTable(sql, ps.ToParameters());
                List<GoldShopGoodsModel> list = new List<GoldShopGoodsModel>();
                foreach (DataRow dr in table.Rows)
                {
                    list.Add(GoldShopGoodsModel.CreateModel(dr));
                }
                return list;
            });
        }

        /// <summary>
		/// 增加一条数据
		/// </summary>
        public bool Add(DbConn pubConn, GoldShopGoodsModel model)
        {
            return SqlHelper.Visit(ps =>
            {
                string strSql = @"insert into tb_goldshop_goods(f_text,f_gold,f_giftgold,f_price,f_createtime,f_updatetime,f_starttime,f_endtime)
values (@f_text,@f_gold,@f_giftgold,@f_price,@f_createtime,@f_updatetime,@f_starttime,@f_endtime);select @@IDENTITY";
                ps.Add("@f_text", model.text);
                ps.Add("@f_gold", model.gold);
                ps.Add("@f_giftgold", model.giftgold);
                ps.Add("@f_price", model.price);
                ps.Add("@f_createtime", DateTime.Now);
                ps.Add("@f_updatetime", DateTime.Now);
                ps.Add("@f_starttime", model.starttime);
                ps.Add("@f_endtime", model.endtime);
                return pubConn.ExecuteSql(strSql, ps.ToParameters()) > 0;
            });
        }

        /// <summary>
		/// 是否存在该记录
		/// </summary>
        public bool Exists(DbConn pubConn, long id)
        {
            return SqlHelper.Visit(ps =>
            {
                ps.Add("f_id", id);
                string strSql = "select count(1) from tb_goldshop_goods where f_id=@f_id";
                return LibConvert.ObjToInt(pubConn.ExecuteScalar(strSql.ToString(), ps.ToParameters())) > 0;
            });
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DbConn pubConn, GoldShopGoodsModel model)
        {
            return SqlHelper.Visit(ps =>
            {
                string strSql = "update tb_goldshop_goods set f_text=@f_text,f_gold=@f_gold,f_giftgold=@f_giftgold,f_price=@f_price,f_updatetime=@f_updatetime,f_starttime=@f_starttime,f_endtime=@f_endtime where f_id=@f_id";
                ps.Add("@f_id", model.id);
                ps.Add("@f_text", model.text);
                ps.Add("@f_gold", model.gold);
                ps.Add("@f_giftgold", model.giftgold);
                ps.Add("@f_price", model.price);
                ps.Add("@f_updatetime", DateTime.Now);
                ps.Add("@f_starttime", model.starttime);
                ps.Add("@f_endtime", model.endtime);
                return pubConn.ExecuteSql(strSql.ToString(), ps.ToParameters()) > 0;
            });
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(DbConn pubConn, long id)
        {
            return SqlHelper.Visit(ps =>
            {
                string strSql = "delete from tb_goldshop_goods where f_id=@f_id";
                ps.Add("@f_id", id);
                return pubConn.ExecuteSql(strSql.ToString(), ps.ToParameters()) > 0;
            });
        }

        /// <summary>获取详情</summary>
        /// <param name="pubConn"></param>
        /// <param name="Mid"></param>
        /// <returns></returns>
        public GoldShopGoodsModel GetInfo(DbConn pubConn, long id)
        {
            return SqlHelper.Visit(ps =>
            {
                ps.Add("f_id", id);
                string sql = "select * from tb_goldshop_goods where f_id=@f_id";
                DataTable table = pubConn.SqlToDataTable(sql, ps.ToParameters());
                if (table != null && table.Rows.Count > 0)
                {
                    return GoldShopGoodsModel.CreateModel(table.Rows[0]);
                }
                else
                {
                    return new GoldShopGoodsModel();
                }
            });
        }
    }
}
