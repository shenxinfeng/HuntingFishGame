﻿using BSF.Db;
using HuntingFishGame.ManageDomain.Model.GoldShopOrder;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntingFishGame.ManageDomain.DAL.GoldShopOrder
{
    public class GoldShopOrderDal
    {
        public List<GoldShopOrderModel> SelectList(DbConn pubConn, int pageIndex, int pageSize, out int count)
        {
            try
            {
                int _count = 0;
                var models = SqlHelper.Visit(ps =>
                {
                    string strWhere = " where 1=1 ";
                    string totalsql = string.Concat("select count(*) from tb_goldshop_order", strWhere);

                    _count = (int)pubConn.ExecuteScalar(totalsql, ps.ToParameters());

                    Int32 startIndex = (pageIndex - 1) * pageSize + 1;
                    Int32 endIndex = pageIndex * pageSize;
                    string order = "desc";
                    string sql = String.Concat(
                        " SELECT *  FROM ",
                        " (SELECT row_number() over(order by f_create_time ", order, ") as rownum, ",
                        "* FROM tb_goldshop_order",
                        strWhere, ")",
                        " as TEMP ",
                        " WHERE rownum between ", startIndex, " and ", endIndex
                        );
                    DataTable table = pubConn.SqlToDataTable(sql, ps.ToParameters());
                    List<GoldShopOrderModel> modelList = new List<GoldShopOrderModel>();
                    foreach (DataRow dr in table.Rows)
                    {
                        modelList.Add(GoldShopOrderModel.CreateModel(dr));
                    }
                    return modelList;
                });
                count = _count;
                return models;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(DbConn pubConn, GoldShopOrderModel model)
        {
            return SqlHelper.Visit(ps =>
            {
                string strSql = @"insert into tb_goldshop_order(f_orderid,f_userid,f_gid,f_text,f_price,f_gold,f_gift_gold,f_payment_status,f_create_time,f_openid)
values (@f_orderid,@f_userid,@f_gid,@f_text,@f_price,@f_gold,@f_gift_gold,@f_payment_status,@f_create_time,@f_openid);select @@IDENTITY";
                ps.Add("@f_orderid", model.orderid);
                ps.Add("@f_userid", model.userid);
                ps.Add("@f_gid", model.gid);
                ps.Add("@f_text", model.text);
                ps.Add("@f_price", model.price);
                ps.Add("@f_gold", model.gold);
                ps.Add("@f_gift_gold", model.gift_gold);
                ps.Add("@f_payment_type", model.payment_type);
                ps.Add("@f_payment_status", model.payment_status);
                ps.Add("@f_create_time", DateTime.Now);
                ps.Add("@f_openid", model.openid);
                return pubConn.ExecuteSql(strSql, ps.ToParameters()) > 0;
            });
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(DbConn pubConn, long id)
        {
            return SqlHelper.Visit(ps =>
            {
                ps.Add("f_id", id);
                string strSql = "select count(1) from tb_goldshop_order where f_id=@f_id";
                return LibConvert.ObjToInt(pubConn.ExecuteScalar(strSql.ToString(), ps.ToParameters())) > 0;
            });
        }

        /// <summary>
        /// 更新一条数据（支付）
        /// </summary>
        public bool Update(DbConn pubConn, GoldShopOrderModel model)
        {
            return SqlHelper.Visit(ps =>
            {
                string strSql = @"update tb_goldshop_order set f_payment_time=@f_payment_time,f_payment_number=@f_payment_number,
f_payment_orderid=@f_payment_orderid,f_payment_status=@f_payment_status,f_payment_type=@f_payment_type where f_orderid=@f_orderid";//f_text=@f_text,
                ps.Add("@f_orderid", model.orderid);
                ps.Add("@f_payment_time", DateTime.Now);
                ps.Add("@f_payment_number", model.payment_number);
                ps.Add("@f_payment_orderid", model.payment_orderid);
                ps.Add("@f_payment_status", model.payment_status);
                ps.Add("@f_payment_type", model.payment_type);
                return pubConn.ExecuteSql(strSql.ToString(), ps.ToParameters()) > 0;
            });
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(DbConn pubConn, long id)
        {
            return SqlHelper.Visit(ps =>
            {
                string strSql = "delete from tb_goldshop_order where f_id=@f_id";
                ps.Add("@f_id", id);
                return pubConn.ExecuteSql(strSql.ToString(), ps.ToParameters()) > 0;
            });
        }

        /// <summary>获取详情</summary>
        /// <param name="pubConn"></param>
        /// <param name="Mid"></param>
        /// <returns></returns>
        public GoldShopOrderModel GetInfo(DbConn pubConn, string orderid)
        {
            return SqlHelper.Visit(ps =>
            {
                ps.Add("f_orderid", orderid);
                string sql = "select * from tb_goldshop_order where f_orderid=@f_orderid";
                DataTable table = pubConn.SqlToDataTable(sql, ps.ToParameters());
                if (table != null && table.Rows.Count > 0)
                {
                    return GoldShopOrderModel.CreateModel(table.Rows[0]);
                }
                else
                {
                    return new GoldShopOrderModel();
                }
            });
        }

        /// <summary>判断是否支付</summary>
        /// <param name="pubConn"></param>
        /// <param name="orderid"></param>
        /// <returns></returns>
        public bool GetPayStatus(DbConn pubConn, string orderid)
        {
            return SqlHelper.Visit(ps =>
            {
                ps.Add("orderid", orderid);
                DataTable table = pubConn.SqlToDataTable("select f_payment_status from tb_goldshop_order where f_orderid=@orderid", ps.ToParameters());
                return Convert.ToInt32(table.Rows[0]["f_payment_status"]) > 0;
            });
        }
    }
}
