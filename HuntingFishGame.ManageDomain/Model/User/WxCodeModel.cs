﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSF.Db;

namespace HuntingFishGame.ManageDomain.Model.User
{
    public class WxCodeModel
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// 微信的openId
        /// </summary>
        public string OpenId { get; set; }
        /// <summary>
        /// 验证码
        /// </summary>
        public int ChkCode { get; set; }
        /// <summary>
        /// 发送时间
        /// </summary>
        public DateTime CodeTime { get; set; }
        /// <summary>
        /// 验证码类型
        /// </summary>
        public int CodeType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CodeRight { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ReMain { get; set; }


        public static WxCodeModel CreateModel(DataRow dr)
        {
            return new WxCodeModel()
            {
                UserId = LibConvert.ObjToInt(dr["userid"]),
                NickName = dr["nickName"].ToString(),
                OpenId = dr["opid"].ToString(),
                ChkCode = LibConvert.ObjToInt(dr["chkCode"]),
                CodeTime = LibConvert.ObjToDateTime(dr["codeTime"]),
                CodeType = LibConvert.ObjToInt(dr["codeType"]),
                CodeRight = LibConvert.ObjToInt(dr["codeRight"]),
                ReMain = dr["remain"].ToString()
            };
        }
    }
}
