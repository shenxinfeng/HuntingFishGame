﻿using System;
using System.Data;


namespace HuntingFishGame.ManageDomain.Model.User
{
    public class OnlineUserModel
    {
        public long ID { get; set; }
        public string SessionId { get; set; }
        public string Token { get; set; }
        /// <summary>
        /// 这里存的是微信的openId
        /// </summary>
        public string UserId { get; set; }
        public string RealName { get; set; }
        public string ClientIP { get; set; }
        public DateTime FirstRequestTime { get; set; }
        public DateTime LastRequestTime { get; set; }
        /// <summary>
        /// 开始上线时间
        /// </summary>
        public DateTime StartOnlineTime { get; set; }
        public string RefreshToken { get; set; }
        public DateTime ExpirationTime { get; set; }
        public bool IsOnline { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
        public int LoginType { get; set; }

        public static OnlineUserModel CreateModel(DataRow dr)
        {
            return new OnlineUserModel()
            {
                ID = Convert.ToInt64(dr["ID"]),
                SessionId = dr["SessionId"].ToString(),
                Token = dr["Token"].ToString(),
                UserId = dr["UserId"].ToString(),
                RealName = dr["RealName"].ToString(),
                ClientIP = dr["ClientIP"].ToString(),
                FirstRequestTime = Convert.ToDateTime(dr["FirstRequestTime"]),
                LastRequestTime = Convert.ToDateTime(dr["LastRequestTime"]),
                StartOnlineTime = Convert.ToDateTime(dr["StartOnlineTime"]),
                RefreshToken = dr["RefreshToken"].ToString(),
                ExpirationTime = Convert.ToDateTime(dr["ExpirationTime"]),
                IsOnline = Convert.ToBoolean(dr["IsOnline"]),
                Longitude = Convert.ToDecimal(dr["Longitude"]),
                Latitude = Convert.ToDecimal(dr["Latitude"]),
                LoginType = Convert.ToInt32(dr["LoginType"])

            };
        }
    }
}
