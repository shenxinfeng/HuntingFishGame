﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntingFishGame.ManageDomain.Model.GoldShopGoods
{
    public class GoldShopGoodsModel
    {
        /****** Script for SelectTopNRows command from SSMS  ******/
        public long id { get; set; }
        public string text { get; set; }
        public string gold { get; set; }
        public string giftgold { get; set; }
        public string price { get; set; }
        public string createtime { get; set; }
        public string updatetime { get; set; }
        public string starttime { get; set; }
        public string endtime { get; set; }

        public static GoldShopGoodsModel CreateModel(DataRow dr)
        {
            return new GoldShopGoodsModel()
            {
                id = Convert.ToInt64(dr["f_id"]),
                text = dr["f_text"].ToString(),
                gold = dr["f_gold"].ToString(),
                giftgold = dr["f_giftgold"].ToString(),
                price = dr["f_price"].ToString(),
                createtime = dr["f_createtime"].ToString(),
                updatetime = dr["f_updatetime"].ToString(),
                starttime = Convert.ToDateTime(dr["f_starttime"]).ToString("yyyy-MM-dd HH:mm:ss"),
                endtime = Convert.ToDateTime(dr["f_endtime"]).ToString("yyyy-MM-dd HH:mm:ss"),
            };
        }
    }
}
