﻿using System;
using System.ComponentModel;
using System.Data;
using BSF.Db;

namespace HuntingFishGame.ManageDomain.Model.WeChat
{
    /// <summary>
    /// 实体类tb_AutoReplyModel
    /// </summary>
    public class AutoReplyModel
    {
        [DisplayName("序号")]
        public int id { get; set; }
        [DisplayName("关键词")]
        public string keyword { get; set; }
        [DisplayName("自动回复内容")]
        public string content { get; set; }
        [DisplayName("备注")]
        public string remark { get; set; }
        [DisplayName("是否冻结：0-未冻结，1-已冻结")]
        public bool sfdj { get; set; }
        [DisplayName("创建人编码")]
        public string jdrbm { get; set; }
        [DisplayName("创建人名称")]
        public string jdrmc { get; set; }
        [DisplayName("创建时间")]
        public DateTime createtime { get; set; }


        public static AutoReplyModel CreateModel(DataRow dr)
        {
            return new AutoReplyModel()
            {
                id = LibConvert.ObjToInt(dr["f_id"]),
                keyword = dr["f_keyword"].ToString(),
                content = dr["f_content"].ToString(),
                remark = dr["f_remark"].ToString(),
                sfdj = LibConvert.ObjToBool(dr["f_sfdj"]),
                jdrbm = dr["f_jdrbm"].ToString(),
                jdrmc = dr["f_jdrmc"].ToString(),
                createtime = LibConvert.StrToDateTime(dr["f_createtime"].ToString())

            };
        }
    }

}


