﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntingFishGame.ManageDomain.Model.GoldShopOrder
{
    public class GoldShopOrderModel
    {
        public string id { get; set; }
        public string orderid { get; set; }
        public string userid { get; set; }
        public string gid { get; set; }
        public string text { get; set; }
        public string price { get; set; }
        public string gold { get; set; }
        public string gift_gold { get; set; }
        public string payment_type { get; set; }
        public string payment_status { get; set; }
        public string create_time { get; set; }
        public string payment_time { get; set; }
        public string payment_number { get; set; }
        public string payment_orderid { get; set; }
        public string openid { get; set; }

        public static GoldShopOrderModel CreateModel(DataRow dr)
        {
            return new GoldShopOrderModel()
            {
                id = dr["f_id"].ToString(),
                orderid = dr["f_orderid"].ToString(),
                userid = dr["f_userid"].ToString(),
                gid = dr["f_gid"].ToString(),
                text = dr["f_text"].ToString(),
                price = dr["f_price"].ToString(),
                gold = dr["f_gold"].ToString(),
                gift_gold = dr["f_gift_gold"].ToString(),
                payment_type = dr["f_payment_type"].ToString(),
                payment_status = dr["f_payment_status"].ToString(),
                create_time = dr["f_create_time"].ToString(),
                payment_number = dr["f_payment_number"].ToString(),
                payment_orderid = dr["f_payment_orderid"].ToString(),
                openid = dr["f_openid"].ToString()
            };
        }
    }
}