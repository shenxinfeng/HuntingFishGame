﻿using HuntingFishGame.ManageDomain.BLL.Base;
using HuntingFishGame.ManageDomain.DAL.GoldShopGoods;
using HuntingFishGame.ManageDomain.Model.GoldShopGoods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntingFishGame.ManageDomain.BLL.GoldShopGoods
{
    public class GoldShopGoodsBLL : BaseBLL
    {
        private static GoldShopGoodsDAL goldShopGoodsDal = new GoldShopGoodsDAL();

        public static List<GoldShopGoodsModel> GetList(string connection, bool fromH5)
        {
            List<GoldShopGoodsModel> list = DbConnAction<List<GoldShopGoodsModel>>(connection, (c) =>
            {
                return goldShopGoodsDal.SelectList(c, fromH5);
            });
            return list;
        }

        public static GoldShopGoodsModel GetInfo(string connection, long id)
        {
            return DbConnAction<GoldShopGoodsModel>(connection, (c) =>
            {
                return goldShopGoodsDal.GetInfo(c, id);
            });
        }

        public static bool Add(string connection, GoldShopGoodsModel model)
        {
            return DbConnAction(connection, (c) =>
            {
                return goldShopGoodsDal.Add(c, model);
            });
        }

        public static bool Update(string connection, GoldShopGoodsModel model)
        {
            return DbConnAction(connection, (c) =>
            {
                return goldShopGoodsDal.Update(c, model);
            });
        }

        public static bool Delete(string connection, long ID)
        {
            return DbConnAction(connection, (c) =>
            {
                return goldShopGoodsDal.Delete(c, ID);
            });
        }
    }
}
