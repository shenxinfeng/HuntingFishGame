﻿using System;
using BSF.Db;

namespace HuntingFishGame.ManageDomain.BLL.Base
{
    public class BaseBLL
    {
        protected static T DbConnAction<T>(string connection, Func<DbConn, T> action)
        {
            using (DbConn pubConn = DbConn.CreateConn(connection))
            {
                pubConn.Open();
                return action.Invoke(pubConn);
            }
        }

        protected static void DbConnAction(string connection, Action<DbConn> action)
        {
            using (DbConn pubConn = DbConn.CreateConn(connection))
            {
                pubConn.Open();
                action.Invoke(pubConn);
            }
        }
    }
}
