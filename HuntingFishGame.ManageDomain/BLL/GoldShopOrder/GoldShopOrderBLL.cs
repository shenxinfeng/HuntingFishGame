﻿using HuntingFishGame.ManageDomain.BLL.Base;
using HuntingFishGame.ManageDomain.DAL.GoldShopOrder;
using HuntingFishGame.ManageDomain.Model.GoldShopOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntingFishGame.ManageDomain.BLL.GoldShopOrder
{
    public class GoldShopOrderBLL : BaseBLL
    {
        private static GoldShopOrderDal goldShopOrderDal = new GoldShopOrderDal();

        public static List<GoldShopOrderModel> GetList(string connection, int pageIndex, int pageSize, out int count)
        {
            int _count = 0;
            List<GoldShopOrderModel> list = DbConnAction<List<GoldShopOrderModel>>(connection, (c) =>
            {
                return goldShopOrderDal.SelectList(c, pageIndex, pageSize, out _count);
            });
            count = _count;
            return list;
        }

        public static GoldShopOrderModel GetInfo(string connection, string orderid)
        {
            return DbConnAction<GoldShopOrderModel>(connection, (c) =>
            {
                return goldShopOrderDal.GetInfo(c, orderid);
            });
        }

        public static bool Add(string connection, GoldShopOrderModel model)
        {
            return DbConnAction(connection, (c) =>
            {
                return goldShopOrderDal.Add(c, model);
            });
        }

        public static bool Update(string connection, GoldShopOrderModel model)
        {
            return DbConnAction(connection, (c) =>
            {
                return goldShopOrderDal.Update(c, model);
            });
        }

        public static bool Delete(string connection, long ID)
        {
            return DbConnAction(connection, (c) =>
            {
                return goldShopOrderDal.Delete(c, ID);
            });
        }

        public static bool GetPayStatus(string connection, string orderid)
        {
            return DbConnAction(connection, (c) =>
            {
                return goldShopOrderDal.GetPayStatus(c, orderid);
            });
        }
    }
}
