﻿using System.Collections.Generic;
using HuntingFishGame.ManageDomain.BLL.Base;
using HuntingFishGame.ManageDomain.DAL.User;
using HuntingFishGame.ManageDomain.Model.GoldShopOrder;

namespace HuntingFishGame.ManageDomain.BLL.User
{
    /// <summary>
    /// 用户修改密码,解锁相关
    /// </summary>
    public class UserPasswordBLL : BaseBLL
    {
        private static UserPasswordDal userPasswordDal = new UserPasswordDal();

        public static bool UpdateLoginPwd(string connection, long userId, string oldPwd, string newPwd)
        {
            return DbConnAction<bool>(connection, (c) =>
            {
                return userPasswordDal.UpdateLoginPwd(c, userId, oldPwd, newPwd);
            });
        }
        public static bool UpdateBankPwd(string connection, long userId, string oldPwd, string newPwd)
        {
            return DbConnAction<bool>(connection, (c) =>
            {
                return userPasswordDal.UpdateBankPwd(c, userId, oldPwd, newPwd);
            });
        }
        public static bool UnBindPc(string connection, string userName, string pwd)
        {
            return DbConnAction<bool>(connection, (c) =>
            {
                return userPasswordDal.UnBindPc(c, userName, pwd);
            });
        }
    }
}
