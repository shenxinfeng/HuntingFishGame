﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuntingFishGame.ManageDomain.BLL.Base;
using HuntingFishGame.ManageDomain.DAL.User;
using HuntingFishGame.ManageDomain.Model.GoldShopOrder;
using HuntingFishGame.ManageDomain.Model.User;

namespace HuntingFishGame.ManageDomain.BLL.User
{
    public class WxCodeBLL : BaseBLL
    {
        private static WxCodeDal wxCodeDal = new WxCodeDal();

        public static List<WxCodeModel> GetList(string connection, string openId, int pageIndex, int pageSize, out int count)
        {
            int _count = 0;
            List<WxCodeModel> list = DbConnAction<List<WxCodeModel>>(connection, (c) =>
            {
                return wxCodeDal.SelectList(c, openId, pageIndex, pageSize, out _count);
            });
            count = _count;
            return list;
        }
    }
}
