﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntingFishGame.ManageDomain.BLL.WeChat
{

    public class WechatJSTicket
    {

        public string jsapi_ticket { get; set; }

        public double expire_time { get; set; }
    }

    public class WechatAccessToken
    {

        public string access_token { get; set; }

        public double expires_in { get; set; }
    }

    /// <summary>
    /// 获取凭证返回json参数
    /// </summary>
    public class AppAccessToken
    {
        //{"code":1,"msg":"成功","data":{"Access_token":"b85MsWpcbctycKbVJXoDMz32VXLWK7ajkCOm7qnD1KG40Fw2yE2W8cD5R70NZ4CLK1t7NBLSv-CN50KLPmKd1IPF9FmQxlSSvZOKDTF5GVcWPSbAHAHAD"},"counttype":null,"total":0,"couponsnum":0,"servertime":1446435081116}
        public int code { get; set; }
        public WechatTokenJSTicket data { get; set; }
        public string counttype { get; set; }
        public int total { get; set; }
        public int couponsnum { get; set; }
        public long servertime { get; set; }
    }
    public class WechatTokenJSTicket
    {
        public string Access_token { get; set; }

        public string jsapi_ticket { get; set; }

        public DateTime ExpirationTime { get; set; }
    }

    public class WechatTicket
    {
        /// <summary>
        /// 错误码
        /// </summary>
        public int errcode { get; set; }
        /// <summary>
        /// 错误信息
        /// </summary>
        public string errmsg { get; set; }
        /// <summary>
        /// ticket
        /// </summary>
        public string ticket { get; set; }
        /// <summary>
        /// 有效期为7200秒
        /// </summary>
        public int expires_in { get; set; }
    }
}
