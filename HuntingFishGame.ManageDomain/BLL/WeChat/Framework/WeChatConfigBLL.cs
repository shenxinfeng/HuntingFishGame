﻿using System;
using System.Collections.Generic;
using HuntingFishGame.ManageDomain.BLL.Base;
using HuntingFishGame.ManageDomain.DAL.WeChat;
using HuntingFishGame.ManageDomain.Model.WeChat;

namespace HuntingFishGame.ManageDomain.BLL.WeChat
{
    /// <summary>
    /// 微信配置
    /// </summary>
    public class WeChatConfigBLL : BaseBLL
    {
        private static readonly WeChatConfigDal weChatConfigDal = new WeChatConfigDal();

        public static List<WeChatConfigModel> GetList(string connection, WeixinType? type, out int count, int pageIndex = 1, int pageSize = 10)
        {
            int _count = 0;
            List<WeChatConfigModel> list = DbConnAction<List<WeChatConfigModel>>(connection, (c) =>
            {
                return weChatConfigDal.GetList(c, type, out _count, pageIndex, pageSize);
            });
            count = _count;
            return list;
        }

        public static bool Exists(string connection, long id)
        {
            return DbConnAction<bool>(connection, (c) =>
            {
                return weChatConfigDal.Exists(c, id);
            });
        }

        public static WeChatConfigModel GetInfo(string connection, long id)
        {
            return DbConnAction<WeChatConfigModel>(connection, (c) =>
            {
                return weChatConfigDal.GetInfo(c, id);
            });
        }

        public static WeChatConfigModel GetModelByParamKey(string connection, string paramKey, WeixinType? type = null, Guid? officialAccount = null)
        {
            return DbConnAction<WeChatConfigModel>(connection, (c) =>
            {
                return weChatConfigDal.GetModelByParamKey(c, paramKey, type, officialAccount);
            });
        }

        public static long Add(string connection, WeChatConfigModel model)
        {
            return DbConnAction(connection, (c) =>
            {
                return weChatConfigDal.Add(c, model);
            });
        }

        public static bool Update(string connection, WeChatConfigModel model)
        {
            return DbConnAction(connection, (c) =>
            {
                return weChatConfigDal.Update(c, model);
            });
        }

        public static bool Delete(string connection, long ID)
        {
            return DbConnAction(connection, (c) =>
            {
                return weChatConfigDal.Delete(c, ID);
            });
        }

    }
}
