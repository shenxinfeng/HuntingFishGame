﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntingFishGame.ManageDomain.BLL.WeChat
{
    /// <summary>
    /// 微信配置信息
    /// </summary>
    public class WeChatSystemConfigModel
    {
        public string AppID { get; set; }
        public string AppSecret { get; set; }
        public string WeChatCallBackHost { get; set; }
        public WeixinType WeixinType { get; set; }
    }
}
