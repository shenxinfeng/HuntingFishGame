﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuntingFishGame.ManageDomain.BLL.WeChat
{
    /// <summary>
    /// 公众号枚举
    /// </summary>
    public enum WeixinType
    {
        /// <summary>
        /// 认证服务号
        /// </summary>
        CertServiceAccount = 1,
        /// <summary>
        /// 订阅号
        /// </summary>
        SubscribeAccount = 2
    }
    public enum OAuthScope
    {
        /// <summary>
        /// 不弹出授权页面，直接跳转，只能获取用户openid
        /// </summary>
        snsapi_base = 1,
        /// <summary>
        /// 弹出授权页面，可通过openid拿到昵称、性别、所在地。并且，即使在未关注的情况下，只要用户授权，也能获取其信息
        /// </summary>
        snsapi_userinfo = 2,
        /// <summary>
        /// web jssdk 授权域
        /// </summary>
        snsapi_login = 3
    }

    /// <summary>
    /// 上传媒体文件类型
    /// </summary>
    public enum UploadMediaFileType
    {
        /// <summary>
        /// 图片: 128K，支持JPG格式
        /// </summary>
        image = 0,
        /// <summary>
        /// 语音：256K，播放长度不超过60s，支持AMR\MP3格式
        /// </summary>
        voice = 1,
        /// <summary>
        /// 视频：1MB，支持MP4格式
        /// </summary>
        video = 2,
        /// <summary>
        /// thumb：64KB，支持JPG格式
        /// </summary>
        thumb = 3
    }


    /// <summary>
    /// 返回码（JSON）
    /// 参考：http://mp.weixin.qq.com/wiki/17/fa4e1434e57290788bde25603fa2fcbd.html
    /// </summary>
    public enum ReturnCodeEnum
    {
        系统繁忙 = -1,
        请求成功 = 0,
        验证失败 = 40001,
        不合法的凭证类型 = 40002,
        不合法的OpenID = 40003,
        不合法的媒体文件类型 = 40004,
        不合法的文件类型 = 40005,
        不合法的文件大小 = 40006,
        不合法的媒体文件id = 40007,
        不合法的消息类型 = 40008,
        不合法的图片文件大小 = 40009,
        不合法的语音文件大小 = 40010,
        不合法的视频文件大小 = 40011,
        不合法的缩略图文件大小 = 40012,
        不合法的APPID = 40013,
        //不合法的access_token      =             40014,
        不合法的access_token = 40014,
        不合法的菜单类型 = 40015,
        //不合法的按钮个数             =          40016,
        //不合法的按钮个数              =         40017,
        不合法的按钮个数1 = 40016,
        不合法的按钮个数2 = 40017,
        不合法的按钮名字长度 = 40018,
        不合法的按钮KEY长度 = 40019,
        不合法的按钮URL长度 = 40020,
        不合法的菜单版本号 = 40021,
        不合法的子菜单级数 = 40022,
        不合法的子菜单按钮个数 = 40023,
        不合法的子菜单按钮类型 = 40024,
        不合法的子菜单按钮名字长度 = 40025,
        不合法的子菜单按钮KEY长度 = 40026,
        不合法的子菜单按钮URL长度 = 40027,
        不合法的自定义菜单使用用户 = 40028,
        缺少access_token参数 = 41001,
        缺少appid参数 = 41002,
        缺少refresh_token参数 = 41003,
        缺少secret参数 = 41004,
        缺少多媒体文件数据 = 41005,
        缺少media_id参数 = 41006,
        缺少子菜单数据 = 41007,
        access_token超时 = 42001,
        需要GET请求 = 43001,
        需要POST请求 = 43002,
        需要HTTPS请求 = 43003,
        多媒体文件为空 = 44001,
        POST的数据包为空 = 44002,
        图文消息内容为空 = 44003,
        多媒体文件大小超过限制 = 45001,
        消息内容超过限制 = 45002,
        标题字段超过限制 = 45003,
        描述字段超过限制 = 45004,
        链接字段超过限制 = 45005,
        图片链接字段超过限制 = 45006,
        语音播放时间超过限制 = 45007,
        图文消息超过限制 = 45008,
        接口调用超过限制 = 45009,
        创建菜单个数超过限制 = 45010,
        不存在媒体数据 = 46001,
        不存在的菜单版本 = 46002,
        不存在的菜单数据 = 46003,
        解析JSON_XML内容错误 = 47001,
        api功能未授权 = 48001,


        用户未授权该api = 50001,
        用户受限 = 50002, //用户受限_可能是违规后接口被封禁

        参数错误 = 61451, //(invalid parameter)
        无效客服账号 = 61452, //(invalid kf_account)
        客服帐号已存在 = 61453, //(kf_account exsited)
        客服帐号名长度超过限制 = 61454,  // (仅允许10个英文字符，不包括@及@后的公众号的微信号)(invalid kf_acount length)
        客服帐号名包含非法字符 = 61455,//(仅允许英文+数字)(illegal character in kf_account)
        客服帐号个数超过限制 = 61456,//(10个客服账号)(kf_account count exceeded)
        无效头像文件类型 = 61457,//(invalid file type)
        系统错误 = 61450,//(system error)
        日期格式错误 = 61500,
        日期范围错误 = 61501,

        POST数据参数不合法 = 9001001,
        远端服务不可用 = 9001002,
        Ticket不合法 = 9001003,
        获取摇周边用户信息失败 = 9001004,
        获取商户信息失败 = 9001005,
        获取OpenID失败 = 9001006,
        上传文件缺失 = 9001007,
        上传素材的文件类型不合法 = 9001008,
        上传素材的文件尺寸不合法 = 9001009,
        上传失败 = 9001010,
        帐号不合法 = 9001020,
        已有设备激活率低于低不能新增设备 = 9001021, //已有设备激活率低于50%，不能新增设备
        设备申请数不合法 = 9001022, //，必须为大于0的数字
        已存在审核中的设备ID申请 = 9001023,
        一次查询设备ID数量不能超过50 = 9001024,
        设备ID不合法 = 9001025,
        页面ID不合法 = 9001026,
        页面参数不合法 = 9001027,
        一次删除页面ID数量不能超过10 = 9001028,
        页面已应用在设备中无法删除 = 9001029, //页面已应用在设备中，请先解除应用关系再删除
        一次查询页面ID数量不能超过50 = 9001030,
        时间区间不合法 = 9001031,
        保存设备与页面的绑定关系参数错误 = 9001032,
        门店ID不合法 = 9001033,
        设备备注信息过长 = 9001034,
        设备申请参数不合法 = 9001035,
        查询起始值begin不合法 = 9001036


    }

    /// <summary>
    /// 
    /// </summary>
    public enum LangType
    {
        /// <summary>
        /// 中文简体
        /// </summary>
        zh_CN,

        /// <summary>
        /// 中文繁体
        /// </summary>
        zh_TW,

        /// <summary>
        /// 英文
        /// </summary>
        en
    }

}
