﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using BSF.SystemConfig;

namespace HuntingFishGame.Manage
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //注册Area
            AreaRegistration.RegisterAllAreas();
            //注册过滤器
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            //注册路由
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            BSFConfig.MainConnectString = ConfigurationManager.ConnectionStrings["MainConnectString"].ConnectionString;
        }
    }
}
