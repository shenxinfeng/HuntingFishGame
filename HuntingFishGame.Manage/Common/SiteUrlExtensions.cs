﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BSF.SystemConfig;

namespace HuntingFishGame.Manage
{
    public static class SiteUrlExtensions
    {

        public static string MainSiteRootUrl = BSFConfig.Get("MainSiteRootUrl", string.Empty);

        /// <summary>
        /// 获取主站点的完整的Url
        /// </summary>
        /// <param name="relativeUrl"></param>
        /// <returns></returns>
        public static string MainSiteFullUrl(this string relativeUrl)
        {
            return MainSiteRootUrl + relativeUrl;
            //if (UrlUtils.IsAbsolutePath(relativeUrl))
            //    return relativeUrl;

            //string fullUrl = string.Empty;

            //if (MainSiteRootUrl.IsNull())
            //{
            //    return UrlUtils.ResolveServerUrl(relativeUrl);
            //}

            //return MainSiteRootUrl.UrlCombine(relativeUrl);

        }
    }
}