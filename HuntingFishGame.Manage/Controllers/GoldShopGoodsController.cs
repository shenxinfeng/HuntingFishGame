﻿using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using HuntingFishGame.ManageDomain.BLL;
using HuntingFishGame.ManageDomain.Model.GoldShopGoods;
using HuntingFishGame.ManageDomain.BLL.GoldShopGoods;
using System.Collections.Generic;
using BSF.SystemConfig;

namespace Dyd.BaseService.ConfigManager.Web.Controllers
{
    public class GoldShopGoodsController : Controller
    {
        //
        // GET: /ProgramMain/
        public ActionResult Index()
        {
            List<GoldShopGoodsModel> pageList = null;
            int count = 0;
            List<GoldShopGoodsModel> List = GoldShopGoodsBLL.GetList(BSFConfig.MainConnectString, false);
            return View(List);
        }

        public ActionResult AddOrEdit(long id = 0)
        {
            if (id == 0)
            {
                return View(new GoldShopGoodsModel());
            }
            else
            {
                GoldShopGoodsModel model = GoldShopGoodsBLL.GetInfo(BSFConfig.MainConnectString, id);
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult AddOrEdit(GoldShopGoodsModel model)
        {
            bool state = model.id == 0 ? GoldShopGoodsBLL.Add(BSFConfig.MainConnectString, model) : GoldShopGoodsBLL.Update(BSFConfig.MainConnectString, model);
            return RedirectToAction("index");
        }

        public JsonResult Delete(long id)
        {
            bool state = GoldShopGoodsBLL.Delete(BSFConfig.MainConnectString, id);
            return Json(new { code = 1, Data = state });
        }
    }
}
