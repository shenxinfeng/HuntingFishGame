﻿using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Dyd.BaseService.ConfigManager.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="username">用户名</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(string username, string password, string returnUrl)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["Admin"].Contains(";" + username + "," + password + ";"))
            {
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(username, false, (int)FormsAuthentication.Timeout.TotalMinutes);
                string enticket = FormsAuthentication.Encrypt(ticket);
                HttpCookie auth = new HttpCookie(FormsAuthentication.FormsCookieName, enticket);
                Response.AppendCookie(auth);
                if (!string.IsNullOrEmpty(returnUrl))
                {
                    return Redirect(returnUrl);
                }
                else
                {
                    return RedirectToAction("Index", "AutoReply");
                    //return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                ModelState.AddModelError("", "登陆出错,请咨询管理员。");
                return View();
            }
        }

        /// <summary>
        /// 登出
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }
    }
}
