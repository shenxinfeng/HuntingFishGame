﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuntingFishGame.Manage.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Success(string title)
        {
            if (string.IsNullOrWhiteSpace(title))
            {
                title = "成功页.";
            }
            ViewBag.Title = title;

            return View();
        }

        public ActionResult Error(string title)
        {
            if (string.IsNullOrWhiteSpace(title))
            {
                title = "失败页.";
            }
            ViewBag.Title = title;

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}