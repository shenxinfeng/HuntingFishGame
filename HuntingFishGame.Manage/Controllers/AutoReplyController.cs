﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using BSF.Db;
using BSF.Enums;
using BSF.SystemConfig;
using HuntingFishGame.ManageDomain.BLL.WeChat;
using HuntingFishGame.ManageDomain.Model.WeChat;
using Webdiyer.WebControls.Mvc;

namespace HuntingFishGame.Manage.Controllers
{
    public class AutoReplyController : Controller
    {
        //
        // GET: /ProgramMain/
        public ActionResult Index(string keyword, bool? sfdj, string CStime, string CEtime, int pageIndex = 1, int pageSize = 10)
        {
            ViewBag.sfdj = sfdj;
            ViewBag.keyword = keyword;
            ViewBag.CStime = CStime;
            ViewBag.CEtime = CEtime;
            ViewBag.pageIndex = pageIndex;
            ViewBag.pageSize = pageSize;

            PagedList<AutoReplyModel> pageList = null;
            int count = 0;
            DateTime? stime = null;
            if (!string.IsNullOrWhiteSpace(CStime))
            {
                stime = LibConvert.ObjToDateTime(CStime);
            }
            DateTime? etime = null;
            if (!string.IsNullOrWhiteSpace(CEtime))
            {
                etime = LibConvert.ObjToDateTime(CEtime);
            }
            List<AutoReplyModel> list = AutoReplyBLL.GetList(BSFConfig.MainConnectString, sfdj, stime, etime, keyword, "", out count, pageIndex, pageSize);
            pageList = new PagedList<AutoReplyModel>(list, pageIndex, pageSize, count);

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("true", "是");
            dict.Add("false", "否");
            ViewBag.StateList = dict;
            return View(pageList);
        }

        public ActionResult AddOrEdit(long id = 0, bool? sfdj = null)
        {
            ViewBag.sfdj = sfdj;
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("true", "是");
            dict.Add("false", "否");
            ViewBag.StateList = dict;
            if (id == 0)
            {
                return View(new AutoReplyModel());
            }
            else
            {
                AutoReplyModel model = AutoReplyBLL.GetInfo(BSFConfig.MainConnectString, id);
                return View(model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddOrEdit(AutoReplyModel model)
        {
            //return Json(new { code = (int)BSFAPICode.NormalError, msg = "已存在重复的关键词" });
            if (model.id == 0)
            {
                //判断是否有重复的关键词
                if (AutoReplyBLL.Exists(BSFConfig.MainConnectString, model.keyword))
                {
                    return Json(new { code = (int)BSFAPICode.NormalError, msg = "已存在重复的关键词" });
                    //return Content("已存在重复的关键词");
                }
            }
            else
            {
                //判断是否有重复的关键词
                if (AutoReplyBLL.Exists(BSFConfig.MainConnectString, model.keyword, model.id))
                {
                    return Json(new { code = (int)BSFAPICode.NormalError, msg = "已存在重复的关键词" });
                }
            }
            model.jdrbm = "";
            model.jdrmc = "";
            model.createtime = DateTime.Now;
            bool state = model.id == 0 ? AutoReplyBLL.Add(BSFConfig.MainConnectString, model) : AutoReplyBLL.Update(BSFConfig.MainConnectString, model);
            return Json(new { code = (int)BSFAPICode.Success, msg = "成功", ReturnUrl = SiteUrls.Instance().AutoReplyListUrl() });
            //return RedirectToAction("index");
        }

        public JsonResult UpdateState(long id, bool sfdj)
        {
            bool result = AutoReplyBLL.UpdateState(BSFConfig.MainConnectString, id, sfdj);
            return Json(new { code = 1, Data = result });
        }

        public JsonResult Delete(long id)
        {
            bool result = AutoReplyBLL.Delete(BSFConfig.MainConnectString, id);
            return Json(new { code = 1, Data = result });
        }
    }
}