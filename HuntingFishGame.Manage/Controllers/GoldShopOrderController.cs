﻿using System.Web.Mvc;
using System.Collections.Generic;
using BSF.SystemConfig;
using HuntingFishGame.ManageDomain.Model.GoldShopOrder;
using HuntingFishGame.ManageDomain.BLL.GoldShopOrder;
using Webdiyer.WebControls.Mvc;

namespace Dyd.BaseService.ConfigManager.Web.Controllers
{
    public class GoldShopOrderController : Controller
    {
        //
        // GET: /ProgramMain/
        public ActionResult Index(int pageIndex = 0, int pageSize = 0)
        {
            PagedList<GoldShopOrderModel> pageList = null;
            int count = 0;
            List<GoldShopOrderModel> list = GoldShopOrderBLL.GetList(BSFConfig.MainConnectString, pageIndex, pageSize, out count);
            pageList = new PagedList<GoldShopOrderModel>(list, pageIndex, pageSize, count);
            return View(pageList);
        }
    }
}
