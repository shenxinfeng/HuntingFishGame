﻿using BSF.Framework;
using BSF.SystemConfig;
using HuntingFishGame.CustomerApi;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HuntingFishGame.CustomerApiTest.API
{
    [TestClass]
    public class BaseTest
    {
        public BaseTest()
        {
            //AppBootstrap.Instance.Start();

            BSFConfig.MainConnectString = Configs.MainConnectString;
            BSFConfig.GameConnectString = Configs.GameConnectString;

        }
    }
}
