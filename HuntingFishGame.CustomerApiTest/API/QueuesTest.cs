﻿using System;
using System.Collections.Generic;
using System.Linq;
using BSF.Log;
using BSF.Queues;
using HuntingFishGame.Domain.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace HuntingFishGame.CustomerApiTest.API
{
    [TestClass]
    public class QueuesTest : BaseTest
    {
        private static IActionBlock<OnlineUserModel> block;
        /// <summary>
        /// 单线程/多线程切换，默认日志发送线程数：1
        /// </summary>
        private readonly int LoggingTaskNum = 1;
        /// <summary>
        /// 阻塞队列的最大长度,默认日志队列最大长度：50000，超过长度后丢掉
        /// </summary>
        private readonly int LoggingQueueLength = 50000;

        /// <summary>
        /// 默认日志打包大小：300,满足条数后执行插入操作
        /// </summary>
        private readonly int LoggingBufferSize = 300;

        /// <summary>
        /// 默认发送阻塞时间。单位:毫秒。5000,即5秒
        /// </summary>
        private readonly int LoggingBlockElapsed = 5000;

        [TestMethod]
        public void QueuesTest_001()
        {
            //创建队列实例
            if (LoggingTaskNum == 1)
            {
                block = new TimerActionBlock<OnlineUserModel>((buffer) =>
                {
                    Send(buffer);
                }, LoggingQueueLength, LoggingBufferSize, LoggingBlockElapsed);
            }
            else
            {
                block = new ThreadedTimerActionBlock<OnlineUserModel>(LoggingTaskNum, (buffer) =>
                {
                    Send(buffer);
                }, LoggingQueueLength, LoggingBufferSize, LoggingBlockElapsed);
            }

            OnlineUserModel log = new OnlineUserModel()
            {
                UserId = "1",
                RealName = "shenxf"
            };
            block.Enqueue(log);

            ////清空队列
            //block.Flush();
        }
        //
        private static int Send(IList<OnlineUserModel> logEntities)
        {
            if (logEntities == null || logEntities.Count <= 0) { return 0; }
            //TODO 执行插入操作
            int rows = 0;
            if (logEntities.Any())
            {
                foreach (var item in logEntities)
                {
                    CommLog.Write(string.Format("用户({0}):{1}", item.UserId, JsonConvert.SerializeObject(item)));
                    rows++;
                }
            }
            return rows;
        }
    }
}
