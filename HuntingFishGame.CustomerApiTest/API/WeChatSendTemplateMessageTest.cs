﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSF.SystemConfig;
using HuntingFishGame.CustomerApi;
using HuntingFishGame.Domain.BLL.WeChat;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace HuntingFishGame.CustomerApiTest.API
{
    [TestClass]
    public class WeChatSendTemplateMessageTest //: BaseTest
    {
        //TODO BaseTest “System.Configuration.ConfigurationErrorsException”类型的异常在 System.Configuration.dll 中发生，但未在用户代码中进行处理
        //其他信息: 无法解析属性“type”的值。错误为: 无法解析类型“CacheManager.SystemRuntimeCaching.MemoryCacheHandle`1, CacheManager.SystemRuntimeCaching”。请验证拼写是否正确或者是否提供了完整的类型名。

        #region 测试代码()
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// 测试模板：	{{result.DATA}}领奖金额:{{withdrawMoney.DATA}}领奖 时间:{{withdrawTime.DATA}}银行信息:{{cardInfo.DATA}}到账时间: {{arrivedTime.DATA}}{{remark.DATA}}
        /// </remarks>
        [TestMethod]
        public void TestSendMessage()
        {
            string token = WeixinConfig.BaseAccessToken(WeixinType.CertServiceAccount).ParamValue;//改成自己的获取ACCESS_TOKEN的方法

            var data = new
            {
                //使用TemplateDataItem简单创建数据。
                result = new
                {
                    //使用new 方式，构建数据，包括value, color两个固定属性。
                    value = "伍华聪",
                    color = "#173177"
                },
                withdrawMoney = new
                {
                    //使用new 方式，构建数据，包括value, color两个固定属性。
                    value = "伍华聪\n",
                    color = "#173177"
                },
                withdrawTime = new
                {
                    //使用new 方式，构建数据，包括value, color两个固定属性。
                    value = "伍华聪\n",
                    color = "#173177"
                },
                cardInfo = new
                {
                    //使用new 方式，构建数据，包括value, color两个固定属性。
                    value = "伍华聪\n",
                    color = "#173177"
                },
                arrivedTime = new
                {
                    //使用new 方式，构建数据，包括value, color两个固定属性。
                    value = "18620292076\n",
                    color = "#173177"
                },
                remark = new
                {
                    //使用new 方式，构建数据，包括value, color两个固定属性。
                    value = "2016年4月18日\n",
                    color = "#173177"
                }
            };

            string url = "http://www.iqidi.com";
            string topColor = "#173177";
            string dataStr = Newtonsoft.Json.JsonConvert.SerializeObject(data);
            string templateId = "-hoLYFbzjdg4RxXUgr9smwzantzCC75sAx8k1CZ1Ckw";//这个是模拟测试的模板编号。。正式的用方法去微信获取
            var response = WeChatSendMessageHelper.SendTemplateMessage(token, WeixinConfig.OpenId, templateId, data, url, topColor);
        }

        [TestMethod]
        public void TestSendTemplate()
        {
            string token = WeixinConfig.BaseAccessToken(WeixinType.CertServiceAccount).ParamValue;//改成自己的获取ACCESS_TOKEN的方法
            string verifyCode = "123456";
            string openId = WeixinConfig.OpenId;

            var data = new
            {
                //使用TemplateDataItem简单创建数据。
                verifyCode = new
                {
                    //使用new 方式，构建数据，包括value, color两个固定属性。
                    value = verifyCode + "\n",
                    color = "#173177"
                },
                arrivedTime = new
                {
                    //使用new 方式，构建数据，包括value, color两个固定属性。
                    value = DateTime.Now.ToString("yyyy年MM月dd日") + "\n",
                    color = "#173177"
                },
                remark = new
                {
                    //使用new 方式，构建数据，包括value, color两个固定属性。
                    value = "如非本人操作，请尽快修改密码。【鱼乐游戏】\n",
                    color = "#173177"
                }
            };
            string dataStr = Newtonsoft.Json.JsonConvert.SerializeObject(data);
            var template = JsonConvert.DeserializeObject<WeChatTemplateModel>(BSFConfig.WeChatTemplate);
            string templateId = template.TemplateId;//这个是模拟测试的模板编号。。正式的用方法去微信获取
            string url = template.TemplateUrl;
            string topColor = template.TopColor;
            var response = WeChatSendMessageHelper.SendTemplateMessage(token, openId, templateId, data, url, topColor);
        }

        #endregion
    }
}
