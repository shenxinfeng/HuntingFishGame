﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSF.Log;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HuntingFishGame.CustomerApiTest.API
{
    [TestClass]
    public class AopTest : BaseTest
    {
        [TestMethod]
        public void Test_001()
        {
        }
    }

    /// <summary>
    /// 业务层的类和方法，让它继承自上下文绑定类的基类
    /// </summary>
    /// <remarks>
    /// C#当中利用Attribute实现简易AOP
    /// http://blog.csdn.net/xiaogui340/article/details/10607829
    /// 浅谈C#关于AOP编程的学习总结
    /// http://www.cnblogs.com/xuxw/p/3887566.html
    /// 所谓AOP（面向切面编程）这个唬人的名词就是干这件事用的，其实现方式有很多种，比如利用spring等框架，但是在实际项目中并不是想引一个框架进来就能随便引的，很多时候都需要我们自己手写一些机制。
    /// 原来之前自己写的好多代码原理就是基于AOP的，比如MVC的过滤器Filter，它里面的异常捕捉可以通过FilterAttribute,IExceptionFilter去处理，这两个对象的处理机制内部原理应该就是AOP，只不过之前没有这个概念罢了。
    /// 这里想到了MVC当中的Filter，只要在Controller或者Action上打一个特性标签（Attribute），就能在方法执行前后做一些其他事情了。那么我们就来简单模拟一个Filter的实现吧。
    /// </remarks>
    [TestClass]
    [MyInterceptor]
    public class BusinessHandler : ContextBoundObject
    {
        [TestMethod]
        [MyInterceptorMethod]
        public void DoSomething()
        {
            CommLog.Write("执行了方法本身！");
        }
    }
}
