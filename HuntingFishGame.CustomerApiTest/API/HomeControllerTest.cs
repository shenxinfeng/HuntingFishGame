﻿using System.Collections.Generic;
using BSF.Framework;
using HuntingFishGame.CustomerApi.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Linq;
using BSF.SystemConfig;
using BSF.Tool;
using HuntingFishGame.CustomerApi.Controllers.Monitor;
using HuntingFishGame.Domain.BLL.User;
using Newtonsoft.Json;

namespace HuntingFishGame.CustomerApiTest.API
{
    [TestClass]
    public class HomeControllerTest : BaseTest
    {
        private static readonly AccountController Instance = new AccountController();
        private static readonly WeixinController inst = new WeixinController();

        [TestMethod]
        public void Album_List_Test_001()
        {
            //Dictionary<object, string> headers = new Dictionary<object, string>();
            //headers.Add("Authorization", "Bearer " + ApiUrlHelper.Token_252);
            //headers["ContentType"] = "application/x-www-form-urlencoded;charset=utf-8"; ;//传图时不需要
            //Dictionary<string, object> param = new Dictionary<string, object>();
            //param.Add("pageIndex", 0);
            //param.Add("pageSize", 10);
            //param.Add("userId", ApiUrlHelper.Userid_252);
            ////模拟请求
            //string result = WebUrlHelper.HttpPostRequest(ApiUrlHelper.AlbumListTestUri, param, headers);
            //RequestSimulationHelper.checkResponse(ApiUrlHelper.AlbumListTestUri, result);

            ////ValidToken验证
            //ApiUrlHelper.SetAuthCookie();
            //var response = Instance.TestCacheManage("key");
            ////text/plain
            //var resultValue = response.Content.ReadAsStringAsync().Result;
            ////text/json
            //response.Content.ReadAsAsync<object>().ContinueWith(
            //    (readTask) =>
            //    {
            //        Console.WriteLine(readTask.Result);
            //    });
            //// Check that response was successful or throw exception 
            //response.EnsureSuccessStatusCode();
            //if (!response.IsSuccessStatusCode)
            //{
            //    Console.Write(string.Format("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase));
            //}

        }

        [TestMethod]
        public void Test()
        {
            /*
             * 1.格式错误,js#shenxinfeng#123123#wokao
             * 2.密码带有特殊字符,js#shenxinfeng#123_56
             * 3.密码太短,js#shenxinfeng#123
             * 4.账号不存在,js#shenxinfeng#123456
             * 5.账号包含特殊字符,js#shenxinfeng_#123456
             * 6.账号密码都正确,js#shenxinfeng#123456 js#robot193#lyd2017
             * **/
            var configFile = AppBootstrap.Instance.MapPath("~/config/TextMsg.xml");
            //自动回复
            //var configFile = AppBootstrap.Instance.MapPath("~/config/TextMsgAutoReply.xml");
            Stream stream = FileHelper.FileToStream(configFile);
            var ret = inst.Index("", "", "");//, stream
            //https://mp.weixin.qq.com/wiki/17/f298879f8fb29ab98b2f2971d42552fd.html
            //<xml><ToUserName><![CDATA[oyrtlv_b2tRIqF3C2Uvw7Iaw4TmU]]></ToUserName><FromUserName><![CDATA[gh_1228689c8fb7]]></FromUserName><CreateTime>1492563175</CreateTime><MsgType><![CDATA[text]]></MsgType><Content><![CDATA[指令有误，请重新输入]]></Content></xml>
            ////c#文件流读文件 
            //using (FileStream fsRead = new FileStream(configFile, FileMode.Open))
            //{
            //    int fsLen = (int)fsRead.Length;
            //    byte[] heByte = new byte[fsLen];
            //    int r = fsRead.Read(heByte, 0, heByte.Length);
            //    string myStr = System.Text.Encoding.UTF8.GetString(heByte);
            //    Console.WriteLine(myStr);
            //    Console.ReadKey();
            //}
        }

        [TestMethod]
        public void WxCode_List_Test()
        {
            List<string> userAccountList = new List<string>();
            //插入绑定的账号 pm.FromUserName->用户的OpenId,pm.ToUserName->当前公众号Id
            int count = 0;
            var userList = WxCodeBLL.GetList(BSFConfig.GameConnectString, "oyrtlv_b2tRIqF3C2Uvw7Iaw4TmU", 1, 20, out count);
            if (userList != null && userList.Any())
            {
                userAccountList = userList.Select(p => p.UserName).ToList();
            }
            userAccountList.RemoveAll(p => p == "");
            var result = JsonConvert.SerializeObject(userAccountList);
        }

    }
}
