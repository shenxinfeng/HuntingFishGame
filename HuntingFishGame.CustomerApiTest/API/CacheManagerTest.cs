﻿using System;
using CacheManager.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HuntingFishGame.CustomerApiTest.API
{
    [TestClass]
    public class CacheManagerTest
    {

        /// <summary>
        /// CacheManager缓存管理
        /// </summary>
        /// <remarks>
        /// .Net缓存管理框架CacheManager
        /// http://www.tuicool.com/articles/nmyUb2r
        /// 官方主页
        /// http://cachemanager.net/
        /// 源代码
        /// https://github.com/MichaCo/CacheManager
        /// 官方MVC项目的Sample
        /// https://github.com/MichaCo/CacheManager/tree/master/samples/CacheManager.Samples.Mvc
        /// </remarks>
        [TestMethod]
        public void Cache_Test_003()
        {
            //var cache = CacheFactory.Build("getStartedCache", settings =>
            //{
            //    settings.WithSystemRuntimeCacheHandle("handleName");
            //});
            var cache = CacheFactory.Build<int>("myCache", settings =>
            {
                settings
                    .WithSystemRuntimeCacheHandle("inProcessCache") //内存缓存Handle
                    .And
                    .WithRedisConfiguration("redis", config => //Redis缓存配置
                    {
                        config.WithAllowAdmin()
                            .WithDatabase(0)
                            .WithEndpoint("localhost", 6379);
                    })
                    .WithMaxRetries(1000) //尝试次数
                    .WithRetryTimeout(100) //尝试超时时间
                    //.WithRedisBackPlate("redis") //redis使用Back Plate
                    .WithRedisCacheHandle("redis", true); //redis缓存handle
            });
            //cache.Add("keyA", "valueA");
            cache.Put("keyB", 23);
            cache.Update("keyB", v => 42);
            Console.WriteLine(@"KeyA is " + cache.Get("keyA")); // should be valueA
            Console.WriteLine(@"KeyB is " + cache.Get("keyB")); // should be 42
            cache.Remove("keyA");
            Console.WriteLine(@"KeyA removed? " + (cache.Get("keyA") == null).ToString());
            Console.WriteLine(@"We are done...");
            Console.ReadKey();
        }
    }
}
