﻿using System;
using BSF.CacheManager;

namespace HuntingFishGame.CustomerApiTest
{
    /// <summary>
    /// API接口
    /// </summary>
    public static class ApiUrlHelper
    {
        //private static string HOST_NAME = "http://192.168.0.252:8090";
        public static string HOST_NAME = "http://192.168.0.60:8005";
        public static string Token = "16163c9f7c7a476d8ce8379da42413e0";
        public static string OrgId = "471";
        public static Guid UserId = Guid.Parse("E58D2751-FBBE-4C18-BDEE-DA50AEEC17F8");
        public static Guid ClassId = Guid.Parse("81C98980-D638-48EB-9ED8-BFC8D7F2877F");
        public static Guid GroupId = Guid.Parse("81C98980-D638-48EB-9ED8-BFC8D7F2877F");

        public static string Token_252 = "8559f6a3e94c44a7ba88f0df391a9b9b";
        public static Guid UserId_252 = Guid.Parse("2468d967-a754-4535-9ed2-1a2e4909dd75");
        //测试邀请有礼
        public static readonly string InvitationUri = HOST_NAME + "user/account/testInvitation";
        //登陆接口
        public static readonly string LoginUri = HOST_NAME + "/user/account/login";
        //日志->新增接口
        public static readonly string BlogCreateUri = HOST_NAME + "/blog/create";
        //日志->删除接口
        public static readonly string BlogDeleteUri = HOST_NAME + "/blog/delete";
        //图片->测试
        public static readonly string PhotoListTestUri = HOST_NAME + "/photo/list";
        //
        public static readonly string PhotoUploadTestUri = HOST_NAME + "/photo/uploadImg";

        public static readonly string AlbumListTestUri = HOST_NAME + "/album/list";

        #region 选修课
        //上传图片测试
        public static string XxkHourScoreInfoTestUri = HOST_NAME + "/xxk/recordScore/hourScoreInfo";
        //选修课->获取时间接口
        public static string XxkGetClassTimeUri = HOST_NAME + "/xxk/tearcher/getClassTime";
        #endregion
        #region 作业
        public static readonly string TeamAddUri = HOST_NAME + "/homework/team/add";
        public static readonly string TeamEditUri = HOST_NAME + "/homework/team/edit";
        public static readonly string TeamDeleteUri = HOST_NAME + "/homework/team/delete";
        public static readonly string TeamListUri = HOST_NAME + "/homework/team/list";
        
        //获取班级成员列表
        public static readonly string ClassmateListUri = HOST_NAME + "/homework/teamMember/classmateList";
        public static readonly string TeamMemberListUri = HOST_NAME + "/homework/teamMember/list";
        public static readonly string TeamMemberDeleteUri = HOST_NAME + "/homework/teamMember/del";
        #endregion

        /// <summary>
        /// 保存用户信息
        /// </summary>
        /// <returns></returns>
        public static string SetAuthCookie()
        {
            //TODO 缓存Token(测试),key每个开发人员设置不一样
            return CacheHelper.GetOrAdd<string>("TestKey", (key) => Token_252 + "|" + UserId_252, 3 * 60);

            //ExceptionUtils.AssertArgumentNullOrEmpty(Token_252, "token");
            //var comboValue = Token_252 + "|" + UserId_252;
            //bool rememberMe = false;
            ////记住密码，过期时间为7天
            //if (rememberMe)
            //{
            //    //clear any other tickets that are already in the response
            //    //Response.Cookies.Clear();

            //    //设置为7天过期
            //    DateTime expiryDate = DateTime.Now.AddDays(7);

            //    //create a new forms auth ticket
            //    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(2, comboValue, DateTime.Now, expiryDate, true, String.Empty);

            //    //encrypt the ticket
            //    string encryptedTicket = FormsAuthentication.Encrypt(ticket);

            //    //create a new authentication cookie – and set its expiration date
            //    var authenticationCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket)
            //    {
            //        Expires = ticket.Expiration
            //    };

            //    //add the cookie to the response.
            //    HttpContext.Current.Response.Cookies.Add(authenticationCookie);
            //}
            //else
            //{
            //    FormsAuthentication.SetAuthCookie(comboValue, false);
            //}
        }

        public static string SetAuthCookie(string token, Guid userId)
        {
            return CacheHelper.GetOrAdd<string>("TestKey", (key) => token + "|" + userId, 3 * 60);
        }
    }
}
