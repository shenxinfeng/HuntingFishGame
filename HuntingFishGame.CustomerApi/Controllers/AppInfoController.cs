﻿using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BSF.Attributes;
using BSF.Enums;
using BSF.ServicesResult;

namespace HuntingFishGame.OpenApi.Controllers
{
    /// <summary>
    /// 提供APP更新所需要的接口
    /// </summary>
    [RoutePrefix("appinfo")]
    //[ApiDocument(1)]
    public class AppInfoController : ApiController
    {
        /// <summary>
        /// 签名验证接口
        /// </summary>
        /// <author>沈鑫锋</author>
        /// <param name="token">用户token</param>
        /// <param name="sign">签名</param>
        /// <returns></returns>
        [Route("SignCheck"), ValidSignature(isCheck: false), Compress] //gzip压缩
        [MultiParameterSupport] //可传json数据
        [ResponseType(typeof(ApiResult))] //返回类型
        public HttpResponseMessage SignCheck(string token, string sign)
        {
            return this.ApiResult(BSFAPICode.Success, "签名校验成功");
        }

    }
}
