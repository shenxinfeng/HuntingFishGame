﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BSF.BaseService.MonitorPlatform.Model;
using BSF.Db;
using BSF.Enums;
using BSF.Log;
using BSF.ServicesResult;
using BSF.Tool;
using HuntingFishGame.CustomerApi.Models;
using BSF.Extensions;
using BSF.SystemConfig;
using HuntingFishGame.Domain.BLL;
using HuntingFishGame.Domain.Model;
using Newtonsoft.Json;

namespace HuntingFishGame.CustomerApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class BaseApiController : Controller
    {
        /// <summary>
        /// 当前请求
        /// </summary>
        public System.Web.HttpRequest CustomRequest = null;

        /// <summary>
        /// 控制器
        /// </summary>
        public string CustomControllerName = "";

        /// <summary>
        /// 方法
        /// </summary>
        public string CustomActionName = "";

        /// <summary>
        /// 
        /// </summary>
        private static object _lock = new object();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="usertype">用户类型</param>
        /// <param name="action">IOnlineUser->传入参数,VisitCore里面的方法使用,object->输出参数(JsonResult),VisitCore返回的参数</param>
        /// <returns></returns>
        public virtual ActionResult Visit(UserTypeEnum usertype, Func<IOnlineUser, object> action)
        {
            /*仅测试限制一个请求 debugvisitonly*/
            bool isDebug = LibConvert.ObjToBool(BSFConfig.Get("debugvisitonly", "true"));
            if (isDebug)
            {
                lock (_lock)
                {
                    if (RouteData != null)
                    {
                        System.Diagnostics.Debug.WriteLine("controller:" + RouteData.Values["controller"]);
                        System.Diagnostics.Debug.WriteLine("action:" + RouteData.Values["action"]);
                    }
                    return VisitCore(usertype, action);
                }
            }
            return VisitCore(usertype, action);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="usertype">用户类型</param>
        /// <param name="action">IOnlineUser->传入参数,VisitCore里面的方法使用,object->输出参数(JsonResult),VisitCore返回的参数</param>
        /// <returns></returns>
        private ActionResult VisitCore(UserTypeEnum usertype, Func<IOnlineUser, object> action)
        {

            var json = new JsonResult();
            json.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            #region 初始耗时参数绑定

            var _req = GetRequest(); //获取请求
            //TimeWatchLog watch = new TimeWatchLog();
            //var paramBind = new ParamBinds();
            var paramBind = new BindParameter();
            string url = "";
            if (_req != null)
                url = _req.RawUrl.ToString();
            CommonHelper.ParamBinds(_req, ref paramBind);
            paramBind.RequseUrl = url;

            string ccontroller = "";
            string caction = "";
            if (base.ControllerContext != null)
            {
                ccontroller = base.ControllerContext.RouteData.Values["controller"].ToString();
                caction = base.ControllerContext.RouteData.Values["action"].ToString();
            }
            else
            {
                ccontroller = CustomControllerName;
                caction = CustomActionName;
            }

            #endregion

            #region 签名校验

            bool isOpenSecureSignatureVerification = BSFConfig.IsOpenSecureSignatureVerification;
            int intervalTime = BSFConfig.SecureSignatureVerificationIntervalTime;
            if (isOpenSecureSignatureVerification == true)
            {
                //上传图片接口,得到得到订单信息  不进行签名   
                if (caction.CompareTo("UploadEvaluationImage") == 0 || caction.CompareTo("GetOrderForComPay") == 0)
                {

                }
                else
                {
                    string err = "";
                    if (!SignHelper.ValidateSign(_req, intervalTime, out err))
                    {
                        //watch.Write(paramBind.CopyTo<ParamBinds>(new ParamBinds()));
                        json.Data = new ServiceResult {code = (int) BSFAPICode.SignInvalid, msg = err};
                        return json;
                    }
                }
            }

            #endregion

            #region 权限验证

            bool ischecked = false;
            OnlineUserModel item = null;
            if (usertype != UserTypeEnum.none)
            {
                string tokenStr = GetTokenStr(_req);
                item = OnlineUserBLL.GetInfoByToken(BSFConfig.MainConnectString, tokenStr);
                if (item == null)
                {
                    ischecked = false;
                }
            }
            else if (usertype == UserTypeEnum.none)
                ischecked = true;
            if (ischecked == false)
            {
                json.Data = new ServiceResult()
                {
                    code = (int) BSFAPICode.TokenInvalid,
                    msg = "token无效",
                    data = null,
                    total = 0
                };
            }
            else
            {
                IOnlineUser token = null;
                if (usertype != UserTypeEnum.none)
                {
                    token = item.CopyTo<OnlineUser>(new OnlineUser());
                }
                try
                {

                    #region  限流相关代码 用户缓解压力

                    #endregion

                    var r = action.Invoke(token);
                    if (r.GetType() == typeof (ListResult<>)) //如果是列表类型的，需要记录总记录数和列表集合
                    {
                        dynamic r1 = r;
                        json.Data = new ServiceResult() {code = 1, msg = "", data = r1.List, total = r1.Total};
                    }
                    else if (r is ServiceResult) //为了兼容api原先的返回非标准结果方式
                    {
                        json.Data = r as ServiceResult;
                    }
                    else if (r is RedirectResult)
                    {
                        return (ActionResult) r;
                    }
                    else
                    {
                        json.Data = new ServiceResult() {code = 1, msg = "没有找到数据!", data = r, total = 0};
                    }

                    //}
                }
                catch (DydException exp)
                {
                    //异常返回
                    json.Data = new ServiceResult()
                    {
                        code = (int) exp.Code,
                        msg = exp.Message,
                        data = exp.Data,
                        total = 0
                    };
                }
                catch (Exception exp)
                {

                    ErrorLog.Write(url + "调用错误:" + exp.Message, exp);
                    //异常返回
                    json.Data = new ServiceResult() {code = -1, msg = exp.Message, data = null, total = 0};
                }
            }
            //watch.Write(paramBind.CopyTo<ParamBinds>(new ParamBinds()));

            #endregion

            #region 跨域

            int crossdomain = LibConvert.ObjToInt(RequestMethodHelper.RequestParamValue(GetRequest(), "crossdomain"));
            if (crossdomain == 1)
            {
                string jsons = Newtonsoft.Json.JsonConvert.SerializeObject(json.Data);
                json.Data = "";
                var response = System.Web.HttpContext.Current.Response;
                response.ContentType = "text/html";
                response.Charset = "utf-8"; //设置输出流的字符集-中文
                response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8"); //设置输出流的字符集
                //获取回调函数名
                var context = System.Web.HttpContext.Current.Request;
                string callback = context.QueryString["success_jsonpCallback"];
                response.Write(callback + "(" + jsons + ")");
                response.End();
            }

            #endregion

            return json;
        }

        /// <summary>
        /// 获取请求信息
        /// </summary>
        /// <returns></returns>
        public System.Web.HttpRequest GetRequest()
        {
            System.Web.HttpRequest _req;

            if (System.Web.HttpContext.Current != null) //当前请求
                _req = System.Web.HttpContext.Current.Request;
            else
                _req = CustomRequest;

            return _req;
        }

        /// <summary>
        /// 获取token
        /// </summary>
        /// <param name="_req"></param>
        /// <returns></returns>
        public string GetTokenStr(System.Web.HttpRequest _req)
        {
            string tokenStr = string.Empty;
            if (_req != null)
            {
                #region 获取access_token参数
                tokenStr = _req.QueryString["access_token"] as string;
                if (string.IsNullOrWhiteSpace(tokenStr))
                {
                    tokenStr = _req.Params["access_token"] as string;
                }
                #endregion
                #region GetBearerToken
                if (string.IsNullOrWhiteSpace(tokenStr))
                {

                    var header = _req.Headers;
                    List<string> withParam = new List<string>
                    {
                        "authorization"
                    };
                    foreach (var key in header.AllKeys)
                    {
                        if (!withParam.Contains(key.ToLower()))
                            continue;
                        else
                        {
                            var headerText = header.GetValues(key).First().ToString();

                            if (!String.IsNullOrEmpty(headerText))
                            {
                                var chars = headerText.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                                if (chars.Length > 1 && chars[0] == "Bearer")
                                {
                                    tokenStr = chars[1];
                                    break;
                                }
                            }
                        }
                    }
                }
                #endregion
            }
            #region 获取Cookie
            if (string.IsNullOrWhiteSpace(tokenStr))
            {
                string cookieVal = CookieHelper.GetCookieValue("_currentUser");
                if (!string.IsNullOrWhiteSpace(cookieVal))
                {
                    IOnlineUser user = JsonConvert.DeserializeObject<OnlineUser>(cookieVal);
                    if (user != null)
                    {
                        tokenStr = user.Token;
                    }
                }
            }
            #endregion
            return tokenStr;
        }
    }

    /// <summary>
    /// 用户类型枚举
    /// </summary>
    public enum UserTypeEnum
    {
        /// <summary>
        /// 普通用户
        /// </summary>
        customer = 0,
        /// <summary>
        /// 无 不需要验证
        /// </summary>
        none = -1
    }

}