﻿using System.Web.Mvc;
using BSF.Log;

namespace HuntingFishGame.CustomerApi
{
    /// <summary>
    /// 异常持久化类,全局异常处理与异常日志
    /// http://www.cnblogs.com/lori/archive/2012/06/15/2551528.html
    /// </summary>
    public class ExceptionLogAttribute : HandleErrorAttribute
    {
        /// <summary>
        /// 触发异常时调用的方法
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnException(ExceptionContext filterContext)
        {

            //string message = string.Format("消息类型：{0}<br>消息内容：{1}<br>引发异常的方法：{2}<br>引发异常的对象：{3}<br>异常目录：{4}<br>异常文件：{5}<br>"
            //    , filterContext.Exception.GetType().Name
            //    , filterContext.Exception.Message
            //    , filterContext.Exception.TargetSite
            //    , filterContext.Exception.Source
            //    , filterContext.RouteData.GetRequiredString("controller")
            //    , filterContext.RouteData.GetRequiredString("action"));
            string message = string.Format("接口:{0}/{1}", filterContext.RouteData.GetRequiredString("controller"),
                filterContext.RouteData.GetRequiredString("action"));
            ErrorLog.Write(message, filterContext.Exception);//TODO:将 ex 错误对象记录到系统日志模块
            base.OnException(filterContext);
        }
    }
}