﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using BSF.Enums;
using BSF.ServicesResult;
using BSF.SystemConfig;
using BSF.Tool;
using HuntingFishGame.CustomerApi;

namespace HuntingFishGame.OpenApi
{
    /// <summary>
    /// 验证客户端签名
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, Inherited = true)]
    public class ValidSignatureAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// 是否需要签名校验
        /// </summary>
        private bool IsSignInterface { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        public ValidSignatureAttribute(bool isCheck)
        {
            IsSignInterface = isCheck;
        }

        /// <summary>
        /// api controller 访问拦截器
        /// 集成校验签名
        /// </summary>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            #region 初始耗时参数绑定
            ////耗时日志
            //TimeWatchLog watch = new TimeWatchLog();
            var _req = CommonHelper.GetRequest();//获取请求
            //var paramBinds = new ParamBinds();
            //string url = "";
            //if (_req != null)
            //    url = _req.RawUrl;
            //CommonHelper.ParamBinds(_req, ref paramBinds);
            //paramBinds.RequseUrl = url;

            //int dqbm = LibConvert.StrToInt(_req["dqbm"]);
            //int configAreaCode = CommonHelper.GetDqbmForConfig(dqbm);
            //获取控制器
            //var currcontroller = actionContext.ControllerContext.Controller;
            //string caction = actionContext.ControllerContext.RouteData.Route.RouteTemplate;

            #endregion

            #region 签名校验(增加模块)
            //取APP配置中心(按城市配置)
            //string isOpenSecureSignatureVerification = new tb_system_config_dal().GetCache(SnsProcessConfig.IsOpenSecureSignatureVerification, configAreaCode);
            //int intervalTime = LibConvert.ObjToInt(new tb_system_config_dal().GetCache(SnsProcessConfig.SecureSignatureVerificationIntervalTime, configAreaCode));
            //取统一配置中心(统一配置)
            bool isOpenSecureSignatureVerification = BSFConfig.IsOpenSecureSignatureVerification;
            int intervalTime = BSFConfig.SecureSignatureVerificationIntervalTime;
            if (isOpenSecureSignatureVerification == true)//进行签名校验
            {
                //根据版本号判断是否需要签名校验
                bool isSign = !(DeviceHelper.Instance.IsAndroidLess("1.0.4") || DeviceHelper.Instance.IsIOSLess("1.0.1"));
                //上传图片接口  不进行签名  
                //List<string> signExclude = new List<string>() { "uploadimage" };
                if (!isSign || !IsSignInterface)//if (signExclude.Contains(caction.ToLower()))//if (caction.CompareTo("UploadImage") == 0)
                {

                }
                else
                {
                    string err = "";
                    if (!SignHelper.ValidateSign(_req, intervalTime, out err))
                    {
                        //watch.Write(paramBinds);//打印耗时日志
                        ApiResult apiResult = new ApiResult(BSFAPICode.SignErr, err);
                        actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.OK, apiResult);
                        //json.Data = new ServiceResult { code = (int)BSFAPICode.SignInvalid, msg = err };
                        //return json;
                    }
                }
            }
            #endregion

        }

    }
}