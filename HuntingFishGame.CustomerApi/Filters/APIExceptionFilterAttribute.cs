﻿using System.Net.Http;
using System.Web.Http.Filters;

namespace HuntingFishGame.CustomerApi
{
    /// <summary>
    /// ASP.NET Web API之消息[拦截]处理 http://blog.csdn.net/dyllove98/article/details/9771459
    /// </summary>
    public class APIExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            //业务异常
            if (context.Exception is BusinessException)
            {
                context.Response = new HttpResponseMessage {StatusCode = System.Net.HttpStatusCode.ExpectationFailed};
                BusinessException exception = (BusinessException) context.Exception;
                context.Response.Headers.Add("BusinessExceptionCode", exception.Code);
                context.Response.Headers.Add("BusinessExceptionMessage", exception.Message);
            }
            //其它异常
            else
            {
                context.Response = new HttpResponseMessage {StatusCode = System.Net.HttpStatusCode.InternalServerError};
            }
        }
    }
}