using System;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Description;
using Bronze.Components.WebApi;

namespace HuntingFishGame.CustomerApi.Areas.HelpPage
{
    public static class ApiDescriptionExtensions
    {
        /// <summary>
        /// Generates an URI-friendly ID for the <see cref="ApiDescription"/>. E.g. "Get-Values-id_name" instead of "GetValues/{id}?name={name}"
        /// </summary>
        /// <param name="description">The <see cref="ApiDescription"/>.</param>
        /// <returns>The ID as a string.</returns>
        public static string GetFriendlyId(this ApiDescription description)
        {
            string path = description.RelativePath;
            string[] urlParts = path.Split('?');
            string localPath = urlParts[0];
            string queryKeyString = null;
            if (urlParts.Length > 1)
            {
                string query = urlParts[1];
                string[] queryKeys = HttpUtility.ParseQueryString(query).AllKeys;
                queryKeyString = String.Join("_", queryKeys);
            }

            StringBuilder friendlyPath = new StringBuilder();
            friendlyPath.AppendFormat("{0}-{1}",
                description.HttpMethod.Method,
                localPath.Replace("/", "-").Replace("{", String.Empty).Replace("}", String.Empty));
            if (queryKeyString != null)
            {
                friendlyPath.AppendFormat("_{0}", queryKeyString.Replace('.', '-'));
            }
            return friendlyPath.ToString();
        }


        public static string GetRouteName(this ApiDescription description)
        {
            string path = string.Empty;
            if (description.Route != null && description.Route is System.Web.Http.Routing.HttpRoute)
            {
                path = description.Route.RouteTemplate;
            }
            else
            {
                path = description.RelativePath;
            }

            return HelpConfig.SiteVirtualDir + path;
        }

        public static string GetRoutePrefix(this HttpControllerDescriptor descriptor)
        {
            var attr = descriptor.ControllerType.GetCustomAttributes(typeof(RoutePrefixAttribute), false).FirstOrDefault() as RoutePrefixAttribute;
            if (attr != null)
            {
                return attr.Prefix;
            }
            return descriptor.ControllerName;
        }

        public static Type GetParentName(this HttpControllerDescriptor descriptor)
        {
            var attr = descriptor.ControllerType.GetCustomAttributes(typeof(ApiDocumentAttribute), false).FirstOrDefault() as ApiDocumentAttribute;
            if (attr != null && attr.Parent != null)
            {
                return attr.Parent;
            }
            return null;
        }
    }
}