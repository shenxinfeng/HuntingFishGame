﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BSF.SystemConfig;

namespace HuntingFishGame.CustomerApi.Areas.HelpPage
{
    public class HelpConfig
    {
        private static string _siteVirtualDirectory;

        /// <summary>
        /// 部署网站的虚拟目录
        /// </summary>
        public static string SiteVirtualDir
        {
            get
            {
                if (_siteVirtualDirectory == null)
                {
                    _siteVirtualDirectory = BSFConfig.Get("SiteVirtualDir");
                }
                return _siteVirtualDirectory;
            }
        }
    }
}