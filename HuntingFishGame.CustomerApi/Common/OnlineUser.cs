﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HuntingFishGame.CustomerApi
{
    [Serializable]
    public class OnlineUser : IOnlineUser
    {
        public string SessionId { get; set; }
        public string Token { get; set; }
        public string RefreshToken { get; set; }
        public string UserId { get; set; }
        //public string UserName { get; set; }
        public string RealName { get; set; }
        public bool IsOnline { get; set; }
        public DateTime FirstRequestTime { get; set; }
        public DateTime LastRequestTime { get; set; }
        public DateTime ExpirationTime { get; set; }
        public string ClientIP { get; set; }
    }
}