﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace HuntingFishGame.CustomerApi
{
    public class Configs
    {
        /// <summary>
        /// 主库连接
        /// </summary>
        public static readonly string MainConnectString = ConfigurationManager.ConnectionStrings["MainConnectString"].ConnectionString;
        //public static string MainConnectString = System.Configuration.ConfigurationManager.AppSettings["MainConnectString"].ToString();
        /// <summary>
        /// 游戏库连接
        /// </summary>
        public static readonly string GameConnectString = ConfigurationManager.ConnectionStrings["GameConnectString"].ConnectionString;
    }
}