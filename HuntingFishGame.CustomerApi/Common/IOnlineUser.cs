﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HuntingFishGame.CustomerApi
{
    /// <summary>
    /// 在线用户信息
    /// </summary>
    public interface IOnlineUser
    {

        /// <summary>
        /// SessionId
        /// </summary>
        string SessionId { get; set; }

        /// <summary>
        /// Token
        /// </summary>
        string Token { get; set; }

        /// <summary>
        /// 刷新Token
        /// </summary>
        string RefreshToken { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        string UserId { get; set; }

        ///// <summary>
        ///// 用户名
        ///// </summary>
        //string UserName { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        string RealName { get; set; }

        /// <summary>
        /// 是否在线
        /// </summary>
        bool IsOnline { get; set; }

        /// <summary>
        /// 第一次请求时间
        /// </summary>
        DateTime FirstRequestTime { get; set; }

        /// <summary>
        /// 最后请求时间
        /// </summary>
        DateTime LastRequestTime { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        DateTime ExpirationTime { get; set; }

        /// <summary>
        /// 客户端IP
        /// </summary>
        string ClientIP { get; set; }

    }
}