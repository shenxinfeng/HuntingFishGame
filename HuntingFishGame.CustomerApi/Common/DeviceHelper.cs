﻿using System;
using BSF.Db;
using BSF.Enums;

namespace HuntingFishGame.CustomerApi
{

    public class DeviceHelper
    {
        public static DeviceHelper Instance = new DeviceHelper();

        private string versionParam = "version";
        private string devicetypeParam = "devicetype";
        /// <summary>
        /// 获取请求
        /// </summary>
        /// <returns></returns>
        public System.Web.HttpRequest GetRequest()
        {
            System.Web.HttpRequest _req = null;

            if (System.Web.HttpContext.Current != null)//当前请求
                _req = System.Web.HttpContext.Current.Request;

            return _req;
        }

        /// <summary>获取手机设备类型</summary>
        /// <returns></returns>
        public DeviceTypeEnum GetDeviceType()
        {
            if (IsIos())
            {
                return DeviceTypeEnum.IOS;
            }
            else if (IsAndroid())
            {
                return DeviceTypeEnum.Android;
            }
            else
            {
                return DeviceTypeEnum.Default;
            }
        }

        #region Android
        /// <summary>
        /// 判断安卓
        /// </summary>
        /// <returns></returns>
        public bool IsAndroid()
        {
            var _req = GetRequest();
            string phoneTypeDevice = _req[devicetypeParam];//设备类型DeviceTypeEnum
            if (!string.IsNullOrWhiteSpace(phoneTypeDevice) && LibConvert.ObjToInt(phoneTypeDevice).Equals((int)DeviceTypeEnum.Android))
            {
                return true;
            }
            else
            {
                //if (string.IsNullOrWhiteSpace(dydPhoneTypeDevice))
                //{
                //string agent = HttpContext.Current.Request.UserAgent;
                //}
                string agent = _req.UserAgent;
                phoneTypeDevice = agent;
                if (!string.IsNullOrWhiteSpace(phoneTypeDevice) && phoneTypeDevice.Contains("Android"))
                {
                    return true;
                }
                else
                    return false;
            }
        }

        /// <summary>
        /// 判断大于安卓某个版本(ios返回false)
        /// </summary>
        /// <param name="phoneversion"></param>
        /// <returns></returns>
        public bool IsAndroidMore(string phoneversion)
        {
            if (IsAndroid())
            {
                var _req = GetRequest();
                var version = _req[versionParam];
                if (String.Compare(version, phoneversion, StringComparison.OrdinalIgnoreCase) > 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        /// <summary>
        /// 判断小于等于安卓某个版本(ios返回false)
        /// </summary>
        /// <param name="phoneversion"></param>
        /// <returns></returns>
        public bool IsAndroidLess(string phoneversion)
        {
            if (IsAndroid())
            {
                var _req = GetRequest();
                var version = _req[versionParam];
                if (String.Compare(version, phoneversion, StringComparison.OrdinalIgnoreCase) <= 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        /// <summary>
        /// 判断等于安卓某个版本(ios返回false)
        /// </summary>
        /// <param name="phoneversion"></param>
        /// <returns></returns>
        public bool IsAndroidEquals(string phoneversion)
        {
            if (IsAndroid())
            {
                var _req = GetRequest();
                var version = _req[versionParam];
                if (String.Compare(version, phoneversion, StringComparison.OrdinalIgnoreCase) == 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
        #endregion
        #region IOS
        /// <summary>
        /// 判断ios
        /// </summary>
        /// <returns></returns>
        public bool IsIos()
        {
            var _req = GetRequest();
            string phoneTypeDevice = _req[devicetypeParam];//设备类型DeviceTypeEnum
            //string version = _req[versionParam];
            //List<string> PhoneTypeDevice = new List<string>();
            //PhoneTypeDevice.Add("iphone");
            //PhoneTypeDevice.Add("ipad");
            //PhoneTypeDevice.Add("ipod");
            //PhoneTypeDevice.Add("itouch");
            //PhoneTypeDevice.Add("iwatch");
            if (!string.IsNullOrWhiteSpace(phoneTypeDevice) &&
                LibConvert.ObjToInt(phoneTypeDevice).Equals((int)DeviceTypeEnum.IOS))
            {
                return true;
            }
            else
            {
                //request获取user-agent
                if (string.IsNullOrWhiteSpace(phoneTypeDevice))
                {
                    //string agent = HttpContext.Current.Request.UserAgent;
                    string agent = _req.UserAgent;
                    phoneTypeDevice = agent;
                }
                if (!string.IsNullOrWhiteSpace(phoneTypeDevice) && phoneTypeDevice.Contains("OS"))
                {
                    return true;
                }
                else
                    return false;
            }
        }

        /// <summary>
        /// 判断大于等于ios某个版本(Android返回false)
        /// </summary>
        /// <param name="phoneversion"></param>
        /// <returns></returns>
        public bool IsIosMoreAndEquals(string phoneversion)
        {
            if (IsIos())
            {
                var _req = GetRequest();
                var version = _req[versionParam];
                if (String.Compare(version, phoneversion, StringComparison.OrdinalIgnoreCase) >= 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        /// <summary>
        /// 判断大于ios某个版本(Android返回false)
        /// </summary>
        /// <param name="phoneversion"></param>
        /// <returns></returns>
        public bool IsIOSMore(string phoneversion)
        {
            if (IsIos())
            {
                var _req = GetRequest();
                var version = _req[versionParam];
                if (String.Compare(version, phoneversion, StringComparison.OrdinalIgnoreCase) > 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        /// <summary>
        /// 判断小于等于ios某个版本(Android返回false)
        /// </summary>
        /// <param name="phoneversion"></param>
        /// <returns></returns>
        public bool IsIOSLess(string phoneversion)
        {
            if (IsIos())
            {
                var _req = GetRequest();
                var version = _req[versionParam];
                if (String.Compare(version, phoneversion, StringComparison.OrdinalIgnoreCase) <= 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        /// <summary>
        /// 判断等于ios某个版本(Android返回false)
        /// </summary>
        /// <param name="phoneversion"></param>
        /// <returns></returns>
        public bool IsIOSEquals(string phoneversion)
        {
            if (IsIos())
            {
                var _req = GetRequest();
                var version = _req[versionParam];
                if (String.Compare(version, phoneversion, StringComparison.OrdinalIgnoreCase) == 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        /// <summary>
        /// C#通过UserAgent判断智能设备(Android,IOS,PC,Mac)
        /// </summary>
        /// <returns></returns>
        public bool CheckAgent()
        {
            bool flag = false;
            var _req = GetRequest();
            string agent = _req.UserAgent;
            //string agent = HttpContext.Current.Request.UserAgent;
            //判断智能设备Agent中的关键词,MQQBrowser 为 QQ 手机浏览器
            string[] keywords = { "Android", "iPhone", "iPod", "iPad", "Windows Phone", "MQQBrowser" };
            //排除 Windows 桌面系统           
            if (!agent.Contains("Windows NT") || (agent.Contains("Windows NT") && agent.Contains("compatible; MSIE 9.0;")))
            {
                //排除 Window 桌面系统 和 苹果桌面系统                
                if (!agent.Contains("Windows NT") && !agent.Contains("Macintosh"))
                {
                    foreach (string item in keywords)
                    {
                        if (agent.Contains(item))
                        {
                            flag = true;
                            break;
                        }
                    }
                }
                flag = true;
            }
            return flag;
        }
        #endregion

    }
}