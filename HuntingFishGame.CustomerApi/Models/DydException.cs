﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BSF.Enums;

namespace HuntingFishGame.CustomerApi.Models
{
    /// <summary>
    /// dyd错误
    /// </summary>
    public class DydException : Exception
    {
        /// <summary>
        /// 自定义错误码
        /// </summary>
        public BSFAPICode Code = BSFAPICode.NormalError;

        /// <summary>
        /// 消息内容
        /// </summary>
        public string Message = "";

        /// <summary>
        /// 数据
        /// </summary>
        public object Data = "";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="message"></param>
        public DydException(BSFAPICode code, string message)
        {
            this.Code = code;
            this.Message = message;
            this.Data = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="message"></param>
        /// <param name="data"></param>
        public DydException(BSFAPICode code, string message, object data)
        {
            this.Code = code;
            this.Message = message;
            this.Data = data;
        }
    }
}