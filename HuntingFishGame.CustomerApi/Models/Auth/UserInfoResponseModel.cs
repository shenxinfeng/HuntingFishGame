﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HuntingFishGame.CustomerApi
{
    /// <summary>
    /// 登录后返回用户信息
    /// </summary>
    public class UserInfoResponseModel
    {
        public string Token { get; set; }
        public DateTime TokenExpirationTime { get; set; }
    }
}