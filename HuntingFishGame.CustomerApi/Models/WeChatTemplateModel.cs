﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HuntingFishGame.CustomerApi
{
    /// <summary>
    /// 微信模版信息配置
    /// </summary>
    public class WeChatTemplateModel
    {
        /// <summary>
        /// 模版Id
        /// </summary>
        public string TemplateId { get; set; }
        /// <summary>
        /// 模版内容
        /// </summary>
        public string TemplateContent { get; set; }
        /// <summary>
        /// 跳转地址
        /// </summary>
        public string TemplateUrl { get; set; }
        /// <summary>
        /// 默认颜色
        /// </summary>
        public string TopColor { get; set; }
    }
}