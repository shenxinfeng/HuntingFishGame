﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using BSF.SystemConfig;
using BSF.Framework;
using System.Reflection;

namespace HuntingFishGame.CustomerApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //注册Area
            AreaRegistration.RegisterAllAreas();

            //支持传递参数：数值到bool类型值(1 = true )转换
            GlobalConfiguration.Configuration.BindParameter(typeof(bool), new BooleanValidatorModelBinder());
            GlobalConfiguration.Configuration.BindParameter(typeof(bool?), new BooleanValidatorModelBinder());

            //绑定简单参数传递json问题
            GlobalConfiguration.Configuration.ParameterBindingRules.Insert(0, SimplePostVariableParameterBinding.HookupParameterBinding);
            GlobalConfiguration.Configure(WebApiConfig.Register);

            //注册过滤器
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            //注册路由
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            BSFConfig.MainConnectString = Configs.MainConnectString;
            BSFConfig.GameConnectString = Configs.GameConnectString;

            //主要为了解决可以在Action上设置SessionState
            ControllerBuilder.Current.SetControllerFactory(typeof(CustomControllerFactory));

            if (!AppBootstrap.Instance.IsStartCompleted)
            {
                AppBootstrap.Instance.Start(Assembly.GetExecutingAssembly());
            }
            
        }
    }
}
