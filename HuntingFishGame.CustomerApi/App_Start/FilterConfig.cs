﻿using System.Web;
using System.Web.Mvc;
using BSF.Attributes;

namespace HuntingFishGame.CustomerApi
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //在这里定义的是全局的Filter,意味着所有的Controller都启用这里注册的Filter
            filters.Add(new HandleErrorAttribute());
            //明明就是这个过滤器，为什么还是会有异常？ 原来问题在FilterConfig 这个类里面，这个类只是对MVC配置起效。（汗！！！！！！）,我们加过滤器的代码要加入到webapi的配置而非mvc的配置 http://www.cnblogs.com/shi-meng/p/4635571.html
            //filters.Add(new ValidSignatureAttribute());//校验签名,放到了WebApiConfig里面
            //filters.Add(new CrossDoMainAttribute());//跨域
            //ExceptionLogAttribute继承自HandleError，主要作用是将异常信息写入日志系统中
            filters.Add(new ExceptionLogAttribute());
        }
    }
}
