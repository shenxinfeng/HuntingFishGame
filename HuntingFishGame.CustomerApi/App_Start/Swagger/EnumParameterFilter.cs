﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Bronze.Components;
using Swashbuckle.Swagger;

namespace HuntingFishGame.CustomerApi.App_Start.Swagger
{

    /// <summary>
    /// 枚举参数描述
    /// </summary>
    public class EnumParameterFilter : IOperationFilter
    {

        public void Apply(Operation operation, SchemaRegistry schemaRegistry, System.Web.Http.Description.ApiDescription apiDescription)
        {
            if (operation.parameters == null)
                return;

            foreach (var par in operation.parameters)
            {
                var propertyEnums = par.@enum;
                if (propertyEnums != null && propertyEnums.Count > 0)
                {
                    var enumDescriptions = new List<string>();

                    for (int i = 0; i < propertyEnums.Count; i++)
                    {
                        var enumOption = propertyEnums[i];
                        var member = enumOption.GetType().GetMember(enumOption.ToString(), BindingFlags.Public | BindingFlags.Static).SingleOrDefault();
                        var desc = ControllerSummaryFilter.GetEnumDescrition(member);
                        if (desc.IsNull())
                        {
                            desc = Enum.GetName(enumOption.GetType(), enumOption);
                        }

                        enumDescriptions.Add(string.Format("{0} : {1} ", (int)enumOption, desc));
                    }
                    par.description += string.Format(" ({0})", string.Join(", ", enumDescriptions.ToArray()));
                }
            }

           
        }
    }
}



