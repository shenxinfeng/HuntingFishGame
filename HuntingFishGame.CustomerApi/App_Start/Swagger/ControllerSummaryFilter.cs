using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Http.Controllers;
using System.Web.Http.Description;
using System.Xml.XPath;
using HuntingFishGame.CustomerApi.Areas.HelpPage;
using Swashbuckle.Swagger;
using Swashbuckle.Swagger.XmlComments;

namespace HuntingFishGame.CustomerApi.App_Start.Swagger
{
    /// <summary>
    /// 文档结构，HACK 原生Swashbuckle不支持自动提取group的注释信息
    /// </summary>
    public class ControllerSummaryFilter : IDocumentFilter
    {
        private static string MemberXPath = "/doc/members/member[@name='{0}']";
        private static string SummaryTag = "summary";
        private static XPathNavigator _navigator;

        public void Apply(SwaggerDocument swaggerDoc, SchemaRegistry schemaRegistry, IApiExplorer apiExplorer)
        {
            var xmlCommentsPath = SwaggerConfig.GetXmlCommentsPath();
            _navigator=new XPathDocument(xmlCommentsPath).CreateNavigator();
            var tags=new List<Tag>();
            
          
            var controllerDescs= apiExplorer.ApiDescriptions.Select(a => a.ActionDescriptor.ControllerDescriptor).Distinct();

            //获取每一个Controller的描述信息
            foreach (var controllerDesc in controllerDescs)
            {
                var name = controllerDesc.GetRoutePrefix().Replace("/", "--");
                tags.Add(new Tag { name = name, description = GetTagDescrition(controllerDesc) });
            }

            //关键步骤：强制设置swagger doc的metadata信息
            swaggerDoc.tags = tags;
            swaggerDoc.info.description = "### 调用说明 ###\r\n" +
                                          "在通过http请求调用API后，返回结果以 **JSON**格式返回，基本结构如下：\r\n" +
                                          "<pre>" +
                                          @"
{
    ret: 1,
    msg: 'sample string 2',
    data:{
        ...
    }
}
" +

                                          "</pre>\r\n" +
                                          "只有当 ret = 1时表示正确调用返回，其他返回值都是错误代码<br><br><br><br>";


        }

        /// <summary>
        /// 获取Controller的描述信息
        /// </summary>
        /// <param name="apiDesc"></param>
        /// <returns></returns>
        public string GetTagDescrition(HttpControllerDescriptor apiDesc)
        {
            var commentId = XmlCommentsIdHelper.GetCommentIdForType(apiDesc.ControllerType);
            var typeNode = _navigator.SelectSingleNode(string.Format(MemberXPath, commentId));

            string description = string.Empty;
            if (typeNode != null)
            {
                var summaryNode = typeNode.SelectSingleNode(SummaryTag);
                if (summaryNode != null)
                    description = summaryNode.ExtractContent();
            }
            return description;
        }

        /// <summary>
        /// 获取枚举的描述信息
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <returns></returns>
        public static string GetEnumDescrition(MemberInfo propertyInfo)
        {
            var displayAttr = propertyInfo.GetCustomAttribute<DisplayAttribute>();
            if (displayAttr != null)
            {
                return displayAttr.Name;
            }

            if (_navigator==null)
            {
                var xmlCommentsPath = SwaggerConfig.GetServiceXmlCommentsPath();
                _navigator = new XPathDocument(xmlCommentsPath).CreateNavigator();
            }

            var commentId = GetCommentIdForMember(propertyInfo);
            try
            {
                var typeNode = _navigator.SelectSingleNode(string.Format(MemberXPath, commentId));

                string description = string.Empty;
                if (typeNode != null)
                {
                    var summaryNode = typeNode.SelectSingleNode(SummaryTag);
                    if (summaryNode != null)
                        description = summaryNode.ExtractContent();
                }
                return description;
            }
            catch (Exception)
            {
                
            }
            return string.Empty;
        }

        public static string GetCommentIdForMember(MemberInfo propertyInfo)
        {
            var builder = new StringBuilder("F:");
            AppendFullTypeName(propertyInfo.DeclaringType, builder);
            builder.Append(".");
            AppendPropertyName(propertyInfo, builder);

            return builder.ToString();
        }

        private static void AppendFullTypeName(Type type, StringBuilder builder, bool expandGenericArgs = false)
        {
            builder.Append(type.Namespace);
            builder.Append(".");
            AppendTypeName(type, builder, expandGenericArgs);
        }

        private static void AppendTypeName(Type type, StringBuilder builder, bool expandGenericArgs)
        {
            if (type.IsNested)
            {
                AppendTypeName(type.DeclaringType, builder, false);
                builder.Append(".");
            }

            builder.Append(type.Name);

        }

        private static void AppendPropertyName(MemberInfo propertyInfo, StringBuilder builder)
        {
            builder.Append(propertyInfo.Name);
        }

    }
}