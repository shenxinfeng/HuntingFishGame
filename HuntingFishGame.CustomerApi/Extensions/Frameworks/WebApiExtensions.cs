﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using BSF.Enums;
using BSF.ServicesResult;

namespace HuntingFishGame.OpenApi
{
    public static class WebApiExtensions
    {
        public static HttpResponseMessage ApiResult(this ApiController controller, BSFAPICode code,string message)
        {
            var result =new ApiResult(code,message);
            return controller.Request.CreateResponse(HttpStatusCode.OK, result);
        }

        public static HttpResponseMessage ApiResult<T>(this ApiController controller, T returnObject)
        {
            var result = new ApiResult<T>(returnObject);
            return controller.Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}