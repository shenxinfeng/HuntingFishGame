﻿using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.ExceptionHandling;
using BSF.Log;

namespace System.Web.Http.ExceptionHandling
{
    /// <summary>
    /// 捕获异常
    /// </summary>
    public class WebApiGlobalExceptionLogger : ExceptionLogger
    {
        /// <summary>
        /// 记录请求参数，以及调用堆栈
        /// </summary>
        /// <param name="context"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override Task LogAsync(ExceptionLoggerContext context, CancellationToken cancellationToken)
        {
            ErrorLog.Write("全局日志", context.Exception);
            return base.LogAsync(context, cancellationToken);
        }
    }
}