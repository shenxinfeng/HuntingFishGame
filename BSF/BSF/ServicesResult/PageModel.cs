﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSF.ServicesResult
{
    /// <summary>
    /// 分页条件
    /// </summary>
    public class PaggingModel
    {
        /// <summary>
        /// 当前页
        /// </summary>
        public int CurPage { get; set; }

        /// <summary>
        /// 每页条数
        /// </summary>
        public int NumPerPage { get; set; }
    }
}
