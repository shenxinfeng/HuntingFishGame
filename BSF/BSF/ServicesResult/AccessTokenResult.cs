﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSF.ServicesResult
{
    /// <summary>
    /// 
    /// </summary>
    public class AccessTokenInfo
    {
        /// <summary>
        /// 应用的唯一标识，对应于AppKey
        /// </summary>
        public string client_id { get; set; }

        /// <summary>
        ///OpenID是此网站上或应用中唯一对应用户身份的标识，网站或应用可将此ID进行存储，便于用户下次登录时辨识其身份，或将其与用户在网站上或应用中的原有账号进行绑定。 
        /// </summary>
        public string open_userId { get; set; }

        /// <summary>
        /// 该access token的有效期，单位为秒
        /// </summary>
        public int expires_in { get; set; }
    }
}
