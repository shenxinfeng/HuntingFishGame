﻿using System;
using BSF.Api;

namespace BSF.ServicesResult
{
    /// <summary>
    /// api返回值定义
    /// 为了兼容原有的返回值模式
    /// </summary>
    public class ServiceResult
    {
        public int code { get; set; }      //返回代码 1成功 0失败
        public string msg { get; set; }    //错误信息
        public object data { get; set; }   //返回数据
        public int total { get; set; }
        /// <summary>
        /// 服务器时间 （UTCNow - 1970-01-01）
        /// </summary>
        public long servertime { get { return (long)(DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds; } }
        //public long servertime { get { return new TimeProvider().GetTimeStamp(); } }
        public string jsonStr { get; set;} //json字符串
    }

    public class ServiceResult2 : ServiceResult
    {
        public object data2 { get; set; }
    }

    public class ServiceResult3 : ServiceResult2
    {
        public string callbackUrl { get; set; }
    }
}