﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BSF.ServicesResult
{
     /// <summary>
     /// 
     /// </summary>
    public class LoginResult
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public string ErrorMsg { get; set; }

        /// <summary>
        /// 用户id
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// 用户所属机构id
        /// </summary>
        public string OrgId { get; set; }

        /// <summary>
        /// 用户所属机构名称
        /// </summary>
        public string OrgName { get; set; }

        /// <summary>
        /// 用户类型(1为学生，2为学生家长，3为教师)
        /// </summary>
        public int UserType { get; set; }

        /// <summary>
        /// 当前用户登录验证通过后返回的验证凭据
        /// </summary>
        public string access_token { get; set; }

        /// <summary>
        /// 如果access_token快过期或已经过期，可以通过refresh_token的方式来换取新的access_token和refresh_token
        /// </summary>
        public string refresh_token { get; set; }

        /// <summary>
        /// 该access token的有效期，单位为秒
        /// </summary>
        public int expires_in { get; set; }
    }
}
