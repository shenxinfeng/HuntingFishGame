﻿namespace BSF.ServicesResult
{
    /// <summary>
    /// 
    /// </summary>
    public class AppBaseInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string AppKey { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AppName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AppLogo { get; set; }
    }
}
