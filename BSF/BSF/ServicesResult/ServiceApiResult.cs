﻿using System;
using BSF.Enums;

namespace BSF.ServicesResult
{
    /// <summary>
    /// 
    /// </summary>
    public interface IApiResult
    {
        /// <summary>
        /// 执行时间
        /// </summary>
        double ExcuteTime { get; set; }
    }

    /// <summary>
    /// 开放平台Api返回结果定义
    /// </summary>
    public class ApiResult : IApiResult
    {
        /// <summary>
        /// 
        /// </summary>
        public ApiResult()
        {
            this.code = (int)BSFAPICode.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        public ApiResult(object data)
        {
            this.code = (int)BSFAPICode.Success;
            this.data = data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="msg"></param>
        public ApiResult(BSFAPICode code, string msg)
        {
            this.code = (int)code;
            this.msg = msg;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="msg"></param>
        public ApiResult(int ret, string msg)
        {
            this.code = ret;
            this.msg = msg;
        }

        /// <summary>
        /// 设置返回错误代码和错误信息
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public ApiResult Return(int ret, string msg)
        {
            this.code = ret;
            this.msg = msg;
            return this;
        }



        /// <summary>
        /// 设置返回值的消息体
        /// </summary>
        public object data { get; set; }

        /// <summary>
        /// 返回状态，1为正确，其他值表示调用返回错误，具体错误原因参见msg
        /// </summary>
        public int code { get; set; }


        private string _msg = string.Empty;

        /// <summary>
        /// 接口调用时发生错误的描述
        /// </summary>
        public string msg
        {
            get { return _msg; }
            set { _msg = value; }
        }

        public double ExcuteTime { get; set; }
    }


    /// <summary>
    /// 开放平台Api返回结果定义
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ApiResult<T> : IApiResult
    {
        /// <summary>
        /// 
        /// </summary>
        public ApiResult()
        {
            this.code = (int)BSFAPICode.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        public ApiResult(T data)
        {
            this.code = (int)BSFAPICode.Success;
            this.data = data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="msg"></param>
        public ApiResult(BSFAPICode code, string msg)
        {
            this.code = (int)code;
            this.msg = msg;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="msg"></param>
        public ApiResult(int ret, string msg)
        {
            this.code = ret;
            this.msg = msg;
        }

        /// <summary>
        /// 设置返回错误代码和错误信息
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public ApiResult<T> Return(int ret, string msg)
        {
            this.code = ret;
            this.msg = msg;
            return this;
        }

        /// <summary>
        /// 设置返回值的消息体
        /// </summary>
        public T data { get; set; }

        /// <summary>
        /// 返回状态码，1为正确，其他值表示调用返回错误，具体错误原因参见msg
        /// </summary>
        public int code { get; set; }

        /// <summary>
        /// 
        /// </summary>
        private string _msg = string.Empty;

        /// <summary>
        /// 接口调用时发生错误的描述
        /// </summary>
        public string msg
        {
            get { return _msg; }
            set { _msg = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static implicit operator ApiResult<T>(Enum value)
        {
            var result = new ApiResult<T>();
            result.code = Convert.ToInt32(value);
            return result;
        }

        /// <summary>
        /// 指向时间（毫秒）
        /// </summary>
        public double ExcuteTime { get; set; }

        //public static implicit operator ApiResult<T>(int otherType)
        //{
        //    return new SizeType
        //    {
        //        InternalValue = otherType
        //    };
        //}
    }
}
