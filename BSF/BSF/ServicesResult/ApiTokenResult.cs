﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSF.ServicesResult
{
    /// <summary>
    /// 
    /// </summary>
    public class ApiTokenResult
    {
        /// <summary>
        /// 访问api需要的凭据
        /// </summary>
        public string ApiToken { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime ExpireTime { get; set; }
    }
}
