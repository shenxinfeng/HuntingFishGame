﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSF.Tool
{
    public class DirectoryHelper
    {
        public static DirectoryHelper _dirhelper = new DirectoryHelper();

        #region 判断文件夹是否存在
        public static bool Exists(string dirPath)
        {
            return Directory.Exists(dirPath);
        }
        #endregion

        #region 创建文件夹
        public static bool Create(string dirPath)
        {
            try
            {
                Directory.CreateDirectory(dirPath);
            }
            catch
            {
                return false;
            }
            return true;
        }
        #endregion

        #region 创建目录
        /// <summary>
        /// 创建目录
        /// </summary>
        ///  <param name="path"></param>
        public static void CreateDirectory(string Path)
        {
            if (!Directory.Exists(Path))
            {
                DirectoryInfo di = Directory.CreateDirectory(Path);
            }
        }
        #endregion

        #region 获取文件名
        /// <summary>
        /// 获取文件名
        /// </summary>
        /// <returns></returns>
        public static string CreateFileName()
        {
            Random rd = new Random();
            StringBuilder serial = new StringBuilder();
            serial.Append(DateTime.Now.ToString("yyyyMMddHHmmss"));
            serial.Append(rd.Next(100000, 999999).ToString());
            return serial.ToString();
        }
        #endregion

        #region 生成文件路径
        /// <summary>
        /// 生成文件路径
        /// </summary>
        /// <returns></returns>
        public static string CreateFilePath()
        {
            return "/" + DateTime.Now.ToString("yyyyMMdd");
        }

        /// <summary>
        /// 生成文件路径
        /// </summary>
        /// <returns></returns>
        public static string CreateFilePath2()
        {
            return "/" + DateTime.Now.Year.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Day.ToString();
        }
        #endregion

    }
}
