﻿using System.Linq;
using BSF.BaseService;

namespace BSF.Tool
{
    /// <summary>
    /// 
    /// </summary>
    public class ConfigHelper
    {
        public static string Get(string key, string defaultvalue = "")
        {
            try
            {
                if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains(key))
                {
                    return System.Configuration.ConfigurationManager.AppSettings[key];
                }
                else
                {
                    if (BaseServiceContext.ConfigManagerProvider != null)
                    {
                        string value = null;
                        if (BaseServiceContext.ConfigManagerProvider.TryGet<string>(key, out value) == true)
                        {
                            return value;
                        }
                        else
                        {
                            return defaultvalue;
                        }
                    }
                }
            }
            catch
            {
                return defaultvalue;
            }

            return defaultvalue;
        }
    }
}
