﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSF.Tool
{
    public class NumberHelper
    {
        /// <summary>
        /// 数字转中文
        /// </summary>
        /// <param name="number">eg: 22</param>
        /// <returns></returns>
        private static string NumberToChinese(int number)
        {
            string res = string.Empty;
            string str = number.ToString();
            string schar = str.Substring(0, 1);
            switch (schar)
            {
                case "1":
                    res = "一";
                    break;
                case "2":
                    res = "二";
                    break;
                case "3":
                    res = "三";
                    break;
                case "4":
                    res = "四";
                    break;
                case "5":
                    res = "五";
                    break;
                case "6":
                    res = "六";
                    break;
                case "7":
                    res = "七";
                    break;
                case "8":
                    res = "八";
                    break;
                case "9":
                    res = "九";
                    break;
                default:
                    res = "零";
                    break;
            }
            if (str.Length > 1)
            {
                switch (str.Length)
                {
                    case 2:
                    case 6:
                        res += "十";
                        break;
                    case 3:
                    case 7:
                        res += "百";
                        break;
                    case 4:
                        res += "千";
                        break;
                    case 5:
                        res += "万";
                        break;
                    default:
                        res += "";
                        break;
                }
                res += NumberToChinese(int.Parse(str.Substring(1, str.Length - 1)));
            }
            return res;
        }

        /// <summary>
        /// 将value 插入到指定数组的指定的位置
        /// </summary>
        /// <param name="a">指定数组</param>
        /// <param name="value">待插入的元素</param>
        /// <param name="index">插入的位置</param>
        /// <returns>插入后的数组</returns>
        public static T[] InsertNumber<T>(T[] a, T value, int index)
        {
            try
            {
                //转换成List<int>集合
                List<T> list = new List<T>(a);
                //插入
                list.Insert(index, value);
                //从List<int>集合，再转换成数组
                return list.ToArray();
            }
            catch (Exception ex) //捕获由插入位置非法而导致的异常
            {
                throw ex;
            }
        }
    }
}
