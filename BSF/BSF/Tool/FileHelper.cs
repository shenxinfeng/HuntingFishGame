using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.IO;
using System.Net;
using System.Web;
using BSF.SystemConfig;

namespace BSF.Tool
{
    /// <summary>
    /// 文件操作类
    /// </summary>
    public class FileHelper : IDisposable
    {
        private bool _alreadyDispose = false;

        #region 构造函数
        /// <summary>
        /// FileObj
        /// </summary>
        public FileHelper()
        {
            //
            // TODO: 在此处添加构造函数逻辑
            //
        }
        /// <summary>
        /// ~FileObj 释放资源
        /// </summary>
        ~FileHelper()
        {
            Dispose(); ;
        }
        /// <summary>
        /// Dispose
        /// </summary>
        /// <param name="isDisposing"></param>
        protected virtual void Dispose(bool isDisposing)
        {
            if (_alreadyDispose) return;
            //if (isDisposing)
            //{
            //     if (xml != null)
            //     {
            //         xml = null;
            //     }
            //}
            _alreadyDispose = true;
        }
        #endregion

        #region IDisposable 成员
        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region 判断文件是否存在
        public static bool Exists(string filePath)
        {
            return File.Exists(filePath);
        }
        #endregion

        #region 创建文件
        public static bool Create(string filePath)
        {
            try
            {
                File.Create(filePath);
            }
            catch
            {
                return false;
            }
            return true;
        }
        #endregion

        //-------------------------------------------------

        #region 取得文件后缀名
        /****************************************
          * 函数名称：GetPostfixStr
          * 功能说明：取得文件后缀名
          * 参     数：filename:文件名称
          * 调用示列：
          *            string filename = "aaa.aspx";        
          *            string s = EC.FileObj.GetPostfixStr(filename);         
         *****************************************/
        /// <summary>
        /// 取后缀名
        /// </summary>
        /// <param name="filename">文件名</param>
        /// <returns>.gif|.html格式</returns>
        public static string GetPostfixStr(string filename)
        {
            //string extension = System.IO.Path.GetExtension(filename).ToLower();//返回.jpg等
            int start = filename.LastIndexOf(".");
            int length = filename.Length;
            string postfix = filename.Substring(start, length - start);
            return postfix;
        }
        #endregion

        #region 写文件
        /****************************************
          * 函数名称：WriteFile
          * 功能说明：写文件,会覆盖掉以前的内容
          * 参     数：Path:文件路径,Strings:文本内容
          * 调用示列：
          *            string Path = Server.MapPath("Default2.aspx");       
          *            string Strings = "这是我写的内容啊";
          *            EC.FileObj.WriteFile(Path,Strings);
         *****************************************/
        /// <summary>
        /// 写文件
        /// </summary>
        /// <param name="Path">文件路径</param>
        /// <param name="Strings">文件内容</param>
        public static void WriteFile(string Path, string Strings)
        {
            if (!System.IO.File.Exists(Path))
            {
                System.IO.FileStream f = System.IO.File.Create(Path);
                f.Close();
            }
            System.IO.StreamWriter f2 = new System.IO.StreamWriter(Path, false, System.Text.Encoding.GetEncoding("gb2312"));
            f2.Write(Strings);
            f2.Close();
            f2.Dispose();
        }
        #endregion

        #region 读文件
        /****************************************
          * 函数名称：ReadFile
          * 功能说明：读取文本内容
          * 参     数：Path:文件路径
          * 调用示列：
          *            string Path = Server.MapPath("Default2.aspx");
          *            string s = EC.FileObj.ReadFile(Path);
         *****************************************/
        /// <summary>
        /// 读文件
        /// </summary>
        /// <param name="Path">文件路径</param>
        /// <returns></returns>
        public static string ReadFile(string Path)
        {
            string s = "";
            if (!System.IO.File.Exists(Path))
                s = "不存在相应的目录";
            else
            {
                StreamReader f2 = new StreamReader(Path, System.Text.Encoding.GetEncoding("UTF-8"));
                s = f2.ReadToEnd();
                f2.Close();
                f2.Dispose();
            }

            return s;
        }
        #endregion

        #region 追加文件
        /****************************************
          * 函数名称：FileAdd
          * 功能说明：追加文件内容
          * 参     数：Path:文件路径,strings:内容
          * 调用示列：
          *            string Path = Server.MapPath("Default2.aspx");     
          *            string Strings = "新追加内容";
          *            EC.FileObj.FileAdd(Path, Strings);
         *****************************************/
        /// <summary>
        /// 追加文件
        /// </summary>
        /// <param name="Path">文件路径</param>
        /// <param name="strings">内容</param>
        public static void FileAdd(string Path, string strings)
        {
            StreamWriter sw = File.AppendText(Path);
            sw.Write(strings);
            sw.Flush();
            sw.Close();
        }
        #endregion

        #region 拷贝文件
        /****************************************
          * 函数名称：FileCoppy
          * 功能说明：拷贝文件
          * 参     数：OrignFile:原始文件,NewFile:新文件路径
          * 调用示列：
          *            string orignFile = Server.MapPath("Default2.aspx");     
          *            string NewFile = Server.MapPath("Default3.aspx");
          *            EC.FileObj.FileCoppy(OrignFile, NewFile);
         *****************************************/
        /// <summary>
        /// 拷贝文件
        /// </summary>
        /// <param name="orignFile">原始文件</param>
        /// <param name="NewFile">新文件路径</param>
        public static void FileCoppy(string orignFile, string NewFile)
        {
            File.Copy(orignFile, NewFile, true);
        }

        #endregion

        #region 删除文件
        /****************************************
          * 函数名称：FileDel
          * 功能说明：删除文件
          * 参     数：Path:文件路径
          * 调用示列：
          *            string Path = Server.MapPath("Default3.aspx");    
          *            EC.FileObj.FileDel(Path);
         *****************************************/
        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="Path">路径</param>
        public static void FileDel(string Path)
        {
            string file = HttpContext.Current.Server.MapPath(Path);

            if (File.Exists(file))
            {
                File.Delete(file);
            }
        }
        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="Path">完整路径</param>
        public static void FileDel1(string Path)
        {
            if (File.Exists(Path))
            {
                File.Delete(Path);
            }
        }
        #endregion

        #region 移动文件
        /****************************************
          * 函数名称：FileMove
          * 功能说明：移动文件
          * 参     数：OrignFile:原始路径,NewFile:新文件路径
          * 调用示列：
          *             string orignFile = Server.MapPath("../说明.txt");    
          *             string NewFile = Server.MapPath("../../说明.txt");
          *             EC.FileObj.FileMove(OrignFile, NewFile);
         *****************************************/
        /// <summary>
        /// 移动文件
        /// </summary>
        /// <param name="orignFile">原始路径</param>
        /// <param name="NewFile">新路径</param>
        public static void FileMove(string orignFile, string NewFile)
        {
            File.Move(orignFile, NewFile);
        }
        #endregion

        #region 在当前目录下创建目录
        /****************************************
          * 函数名称：FolderCreate
          * 功能说明：在当前目录下创建目录
          * 参     数：OrignFolder:当前目录,NewFloder:新目录
          * 调用示列：
          *            string orignFolder = Server.MapPath("test/");    
          *            string NewFloder = "new";
          *            EC.FileObj.FolderCreate(OrignFolder, NewFloder); 
         *****************************************/
        /// <summary>
        /// 在当前目录下创建目录
        /// </summary>
        /// <param name="orignFolder">当前目录</param>
        /// <param name="NewFloder">新目录</param>
        public static void FolderCreate(string orignFolder, string NewFloder)
        {
            Directory.SetCurrentDirectory(orignFolder);
            Directory.CreateDirectory(NewFloder);
        }
        #endregion

        #region 递归删除文件夹目录及文件
        /****************************************
          * 函数名称：DeleteFolder
          * 功能说明：递归删除文件夹目录及文件
          * 参     数：dir:文件夹路径
          * 调用示列：
          *            string dir = Server.MapPath("test/");  
          *            EC.FileObj.DeleteFolder(dir);       
         *****************************************/
        /// <summary>
        /// 递归删除文件夹目录及文件
        /// </summary>
        /// <param name="dir"></param>
        /// <returns></returns>
        public static void DeleteFolder(string dir)
        {
            if (Directory.Exists(dir)) //如果存在这个文件夹删除之 
            {
                foreach (string d in Directory.GetFileSystemEntries(dir))
                {
                    if (File.Exists(d))
                        File.Delete(d); //直接删除其中的文件 
                    else
                        DeleteFolder(d); //递归删除子文件夹 
                }
                Directory.Delete(dir); //删除已空文件夹 
            }

        }

        #endregion

        #region 将指定文件夹下面的所有内容copy到目标文件夹下面 果目标文件夹为只读属性就会报错。
        /****************************************
          * 函数名称：CopyDir
          * 功能说明：将指定文件夹下面的所有内容copy到目标文件夹下面 果目标文件夹为只读属性就会报错。
          * 参     数：srcPath:原始路径,aimPath:目标文件夹
          * 调用示列：
          *            string srcPath = Server.MapPath("test/");  
          *            string aimPath = Server.MapPath("test1/");
          *            EC.FileObj.CopyDir(srcPath,aimPath);   
         *****************************************/
        /// <summary>
        /// 指定文件夹下面的所有内容copy到目标文件夹下面
        /// </summary>
        /// <param name="srcPath">原始路径</param>
        /// <param name="aimPath">目标文件夹</param>
        public static void CopyDir(string srcPath, string aimPath)
        {
            try
            {
                // 检查目标目录是否以目录分割字符结束如果不是则添加之
                if (aimPath[aimPath.Length - 1] != Path.DirectorySeparatorChar)
                    aimPath += Path.DirectorySeparatorChar;
                // 判断目标目录是否存在如果不存在则新建之
                if (!Directory.Exists(aimPath))
                    Directory.CreateDirectory(aimPath);
                // 得到源目录的文件列表，该里面是包含文件以及目录路径的一个数组
                //如果你指向copy目标文件下面的文件而不包含目录请使用下面的方法
                //string[] fileList = Directory.GetFiles(srcPath);
                string[] fileList = Directory.GetFileSystemEntries(srcPath);
                //遍历所有的文件和目录
                foreach (string file in fileList)
                {
                    //先当作目录处理如果存在这个目录就递归Copy该目录下面的文件

                    if (Directory.Exists(file))
                        CopyDir(file, aimPath + Path.GetFileName(file));
                    //否则直接Copy文件
                    else
                        File.Copy(file, aimPath + Path.GetFileName(file), true);
                }

            }
            catch (Exception ee)
            {
                throw new Exception(ee.ToString());
            }
        }


        #endregion

        #region Kill文件进程
        /// <summary>
        /// Kill文件进程
        /// </summary>
        /// <param name="processName">进程名称</param>
        private void KillFileProcess(string processName)
        {
            System.Diagnostics.Process[] ps = System.Diagnostics.Process.GetProcesses();
            foreach (System.Diagnostics.Process p in ps)
            {
                if (p.ProcessName.ToLower().Equals(processName.ToLower()))
                    p.Kill();
            }
        }
        #endregion

        #region 保存文件处理(打开-保存-取消)
        /// <summary>
        /// 保存文件处理(打开-保存-取消)
        /// </summary>
        /// <param name="page">一般为this</param>
        /// <param name="isThisDirectory">是否在服务器端指定已存在文件路径</param>
        public void SaveFileDialog(System.Web.UI.Page page)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                if (!string.IsNullOrEmpty(ServerFilePath))
                {
                    FileName = page.Server.UrlEncode(FileName);
                    string filePath = ServerFilePath;//服务器端路径
                    FileInfo fileInfo = new FileInfo(filePath);
                    page.Response.Clear();
                    page.Response.ClearContent();
                    page.Response.ClearHeaders();
                    page.Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                    page.Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                    page.Response.AddHeader("Content-Transfer-Encoding", "binary");
                    page.Response.ContentType = "application/octet-stream";
                    page.Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                    page.Response.WriteFile(fileInfo.FullName);
                    page.Response.Flush();
                }
                else
                {
                    if (!page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), "checkServerFilePathJs"))
                    {
                        string checkServerFilePath = "<script type='text/javascript'>alert('请先设置服务器端暂时保存的文件路径!');</script>";
                        page.ClientScript.RegisterStartupScript(page.GetType(), "checkServerFilePathJs", checkServerFilePath);
                    }
                }
            }
            else
            {
                if (!page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), "checkFileNameJs"))
                {
                    string checkFileName = "<script type='text/javascript'>alert('请设定下载或保存的默认文件名!');</script>";
                    page.ClientScript.RegisterStartupScript(page.GetType(), "checkFileNameJs", checkFileName);
                }
            }
        }

        private string _fileName = string.Empty;
        /// <summary>
        /// 获取或设置客户端保存文件名
        /// </summary>
        public string FileName
        {
            set { _fileName = value; }
            get { return _fileName; }
        }

        private string _serverFilePath = string.Empty;
        /// <summary>
        /// 获取或设置服务器端的文件路径
        /// </summary>
        public string ServerFilePath
        {
            set { _serverFilePath = value; }
            get { return _serverFilePath; }
        }
        #endregion

        #region 根据目录查找文件
        /// <summary>
        /// 参数为指定的目录
        /// </summary>
        /// <param name="dir">目录，例：c:\\test\\</param>
        /// <param name="fileName">文件名称，例：rain.jpg</param>
        /// <param name="searchPattern">匹配类型，例：*.*/*.jpg</param>
        /// <returns></returns>
        public static string FindFile(string dir, string fileName, string searchPattern)
        {
            //在指定目录及子目录下查找文件,在listBox1中列出子目录及文件 
            DirectoryInfo Dir = new DirectoryInfo(dir);
            try
            {
                foreach (DirectoryInfo d in Dir.GetDirectories()) //查找子目录 
                {
                    var result = FindFile(Dir + d.ToString() + "\\", fileName, searchPattern);
                    if (result != "")
                    {
                        return result;
                    }
                }
                foreach (FileInfo f in Dir.GetFiles(searchPattern)) //查找文件 
                {
                    if (f.Name == fileName) return f.FullName;
                }
            }
            catch (Exception e)
            {
                return "error:" + e.Message;
            }
            return "";
        }
        /// <summary>
        /// 参数为指定的目录
        /// </summary>
        /// <param name="dir">目录，例：c:\\test\\</param>
        /// <param name="fileName">文件名称，例：rain.jpg</param>
        /// <returns></returns>
        public static string FindFile(string dir, string fileName)
        {
            return FindFile(dir, fileName, "*.*");
        }
        #endregion

        #region Stream 和 byte[] 之间的转换
        /// 将 Stream 转成 byte[]
        public byte[] StreamToBytes(Stream stream)
        {
            byte[] bytes = new byte[stream.Length];
            stream.Read(bytes, 0, bytes.Length);
            // 设置当前流的位置为流的开始
            stream.Seek(0, SeekOrigin.Begin);
            return bytes;
        }

        /// 将 byte[] 转成 Stream
        public Stream BytesToStream(byte[] bytes)
        {
            Stream stream = new MemoryStream(bytes);
            return stream;
        }
        /// <summary>
        /// 将 Stream 写入文件
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="fileName"></param>
        public static void StreamToFile(Stream stream, string fileName)
        {
            // 把 Stream 转换成 byte[]
            byte[] bytes = new byte[stream.Length];
            stream.Read(bytes, 0, bytes.Length);
            // 设置当前流的位置为流的开始
            stream.Seek(0, SeekOrigin.Begin);
            // 把 byte[] 写入文件
            FileStream fs = new FileStream(fileName, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(bytes);
            bw.Close();
            fs.Close();
        }

        /// <summary>
        /// 从文件读取 Stream
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static Stream FileToStream(string fileName)
        {
            // 打开文件
            FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            // 读取文件的 byte[]
            byte[] bytes = new byte[fileStream.Length];
            fileStream.Read(bytes, 0, bytes.Length);
            fileStream.Close();
            // 把 byte[] 转换成 Stream
            Stream stream = new MemoryStream(bytes);
            return stream;
        }


        public string binaryReadFile(string filepath)
        {
            //.net下面是用HttpUtility.UrlEncode()和HttpUtility.UrlDecode()来实现url的encode和decode操作
            //Jquery解码：decodeURIComponent(url);
            //Jquery编码：encodeURIComponent(url);
            filepath = HttpUtility.UrlDecode(filepath);
            //把文件转成二进制
            //FileStream fs = new FileStream(Server.MapPath("~" + filepath), FileMode.Open);
            //BinaryReader br = new BinaryReader(fs);  
            //Byte[] byData = br.ReadBytes((int)fs.Length);  
            //fs.Close();
            //string ret = Convert.ToBase64String(byData, Base64FormattingOptions.None);//byte[]转string

            byte[] s = System.IO.File.ReadAllBytes(filepath);
            //FileStream fs = System.IO.File.OpenRead(Server.MapPath("~" + filepath));// 我没有你说的二进制文件，我就读个本地的照片，以二进制的方式读取的，所以这里你可以理解成你从下位机获取二进制流
            //byte[] s = new byte[fs.Length]; // string.ToCharArray方法，其实我感觉你从下位机直接接收成char[] 更好
            //fs.Read(s, 0, s.Length);// 这是将文件写入s，byte[] 和char[]你可以理解成是一样的，但是byte更好些，因为byte是uchar，char是有符号的，可能理解为负数
            //FileStream fs2 = new FileStream(@"D:\123.hex", FileMode.CreateNew);// 要保存的文件，完全路径（路径+文件名称），新建模式
            //fs2.Write(s, 0, s.Length);// 写入文件了

            //string ret = System.Text.Encoding.ASCII.GetString(s);//byte[]转成string
            //string ret = Convert.ToBase64String(s, Base64FormattingOptions.None);//byte[]转string
            StringBuilder sb = new StringBuilder();
            foreach (var a in s)
            {
                sb.Append(a.ToString("x2"));
            }
            return sb.ToString();
            //return Json(new { result = true, msg = sb.ToString() });
        }
        #endregion


        #region asp.net中使用ffmpeg

        public static bool ConvertMp3(string oldfilepath, string newfilepath)
        {
            bool result = false;
            try
            {
                //string LocalPath = Server.MapPath("~/");
                //oldfilepath = @"C:\data.wav";
                //newfilepath = @"d:\1.mp3";
                string exepath = System.Web.HttpContext.Current.Server.MapPath(BSFConfig.DownloadFile + @"/ffmpeg.exe");
                string FFmpegArguments = string.Format(@" -i {0} -ab 56 -ar 22050 -b 500 -r 15 -s 320x240 {1} ", oldfilepath, newfilepath);
                //string FFmpegArguments = "-y -i c:\\data.amr c:\\1.mp3";
                ExcuteProcess(exepath, FFmpegArguments, (s, e) => Console.WriteLine(e.Data));
                result = true;
            }
            catch
            {
                result = false;
            }
            return result;
        }

        private static bool ExcuteProcess(string exe, string arg, DataReceivedEventHandler output)
        {
            using (var p = new Process())//建立外部调用线程
            {
                try
                {

                    p.StartInfo.FileName = exe;//要调用外部程序的绝对路径
                    p.StartInfo.Arguments = arg;//参数(这里就是FFMPEG的参数了)

                    p.StartInfo.UseShellExecute = false;//输出信息重定向,不使用操作系统外壳程序启动线程(一定为FALSE,详细的请看MSDN)
                    p.StartInfo.CreateNoWindow = true;//不创建进程窗口
                    p.StartInfo.RedirectStandardError = true;//把外部程序错误输出写到StandardError流中(这个一定要注意,FFMPEG的所有输出信息,都为错误输出流,用StandardOutput是捕获不到任何消息的...这是我耗费了2个多月得出来的经验...mencoder就是用standardOutput来捕获的)
                    //p.StartInfo.RedirectStandardInput = true;//重定向標準輸入
                    p.StartInfo.RedirectStandardOutput = true;//重定向標準輸出

                    //p.OutputDataReceived += output;
                    ////p.ErrorDataReceived += output;//外部程序(这里是FFMPEG)输出流时候产生的事件,这里是把流的处理过程转移到下面的方法中,详细请查阅MSDN
                    //p.ErrorDataReceived += new DataReceivedEventHandler(Output);//外部程序(这里是FFMPEG)输出流时候产生的事件,这里是把流的处理过程转移到下面的方法中,详细请查阅MSDN

                    p.Start();//启动线程
                    //p.BeginOutputReadLine();
                    //p.BeginErrorReadLine();//开始异步读取
                    p.WaitForExit();//等待进程结束
                    //p.StandardError.ReadToEnd();//开始同步读取

                    //p.Close();//关闭进程
                    //p.Dispose();//释放资源

                    return true;
                }
                catch
                {
                    return false;
                }
                finally
                {
                    p.Close();//关闭进程
                    p.Dispose();//释放资源
                }
            }
        }

        private void Output(object sendProcess, DataReceivedEventArgs output)
        {
            if (!String.IsNullOrEmpty(output.Data))
            {
                //处理方法...
            }
        }

        /// 视频(avi,mov等等格式)转换为flv格式视频
        /// </summary>
        /// <param name="FromName">被转换的视频文件</param>
        /// <param name="ExportName">转换flv后的文件名</param>
        /// <param name="ExportName">视频大小的尺寸</param>
        /// <returns></returns>
        public string VideoConvertFlv(string FromName, string ExportName, string WidthAndHeight)
        {
            string ffmpeg = @"D:\ffmpeg\ffmpeg.exe";
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo(ffmpeg);
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            //startInfo.Arguments = " -i " + Server.MapPath(FromName) + " -ab 56 -ar 22050 -b 500 -r 15 -s "+WidthAndHeight+" "+Server.MapPath(ExportName);
            startInfo.Arguments = " -i " + FromName + " -ab 56 -ar 22050 -b 500 -r 15 -s " + WidthAndHeight + " " + ExportName;
            try
            {
                System.Diagnostics.Process.Start(startInfo);
                return ExportName;
            }
            catch (Exception err)
            {
                return err.Message;
            }
        }

        /// <summary>
        /// 从视频画面中截取一帧画面为图片
        /// </summary>
        /// <param name="VideoName">视频文件pic/guiyu.mov</param>
        /// <param name="WidthAndHeight">图片的尺寸如:240*180</param>
        /// <param name="CutTimeFrame">开始截取的时间如:"1"</param>
        /// <returns></returns>
        public string GetPicFromVideo(string VideoName, string WidthAndHeight, string CutTimeFrame)
        {

            string ffmpeg = @"C:Inetpubwwwrootdemopic fmpeg.exe";
            string PicName = System.Web.HttpContext.Current.Server.MapPath(Guid.NewGuid().ToString().Replace("-", "") + ".jpg");
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo(ffmpeg);
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.Arguments = " -i " + System.Web.HttpContext.Current.Server.MapPath(VideoName) + " -y -f image2 -ss " + CutTimeFrame + " -t 0.001 -s " + WidthAndHeight + " " + PicName;
            try
            {
                System.Diagnostics.Process.Start(startInfo);
                return PicName;
            }
            catch (Exception err)
            {
                return err.Message;
            }
        }

        #endregion

        /// <summary>
        /// 获取远程图片的数据流
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static Stream GetYCStream(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            MemoryStream ms = new MemoryStream();
            return response.GetResponseStream();
        }

        /// <summary>
        /// 图片流保存为图片
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static string SaveYCStream(Stream stream, Dictionary<string, string> filename)
        {
            Bitmap _Bitmap = (Bitmap)Bitmap.FromStream(stream);
            MemoryStream ms = new MemoryStream();
            _Bitmap.Save(ms, ImageFormat.Jpeg);
            string _sPath = filename["dirpath"].ToString();
            string _filename = filename["filename"].ToString();
            if (!Directory.Exists(_sPath))
            {
                Directory.CreateDirectory(_sPath);
            }
            using (Stream localFile = new FileStream(_sPath + _filename, FileMode.OpenOrCreate))
            {
                //ms.ToArray()转换为字节数组就是想要的图片源字节
                localFile.Write(ms.ToArray(), 0, (int)ms.Length);
            }
            return _sPath + _filename;
        }

        /// <summary>
        /// 数据流保存成文件
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="filename">文件路径+文件名</param>
        public static bool WriteFile(System.IO.Stream stream, string filename)
        {
            try
            {
                using (System.IO.StreamWriter objwrite = new System.IO.StreamWriter(filename))
                {
                    int k = 0;
                    while (k != -1)
                    {
                        k = stream.ReadByte();
                        if (k != -1)
                        {
                            objwrite.BaseStream.WriteByte((byte)k);
                        }
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
