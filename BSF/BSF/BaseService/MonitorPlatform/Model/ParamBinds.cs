﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSF.BaseService.MonitorPlatform.Model
{
    /// <summary>
    /// 传参绑定类
    /// </summary>
    public class ParamBinds
    {
        
        /// <summary>
        /// 手机唯一标识
        /// </summary>
        public string Uniquetag { set; get; }

        /// <summary>
        /// 手机端版本号
        /// </summary>
        public string Phoneversion { set; get; }

        /// <summary>
        /// 手机设备号
        /// </summary>
        public string Phonedevice { set; get; }

        /// <summary>
        /// 用户token
        /// </summary>
        public string Token { set; get; }

        /// <summary>
        /// 请求Url
        /// </summary>
        public string RequseUrl { set; get; }

        /// <summary>
        /// 请求Ip
        /// </summary>
        public string Ip { set; get; }

        /// <summary>
        /// 消息
        /// </summary>
        public string Msg { set; get; }

        /// <summary>
        /// 服务器IP
        /// </summary>
        public string ServerIp { get; set; }
    }
}