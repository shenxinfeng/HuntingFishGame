﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BSF.BaseService.MonitorPlatform.Model
{
    /// <summary>
    /// 记录耗时日志Model,参考ParamBinds,手机端必传参数
    /// </summary>
    public class BindParameter
    {
        #region 必须参数
        /// <summary>
        /// 设备类型,DeviceTypeEnum
        /// </summary>
        public string Devicetype { get; set; }

        /// <summary>
        /// 手机端版本号，例如：1.0.5
        /// </summary>
        public string Version { set; get; }

        /// <summary>
        /// 手机唯一标识，例如：AtxS9hYU26D__TZ7D08Qr5MYjY3Ny4uMfLWS32zwn1aC
        /// </summary>
        public string Uniquetag { set; get; }

        /// <summary>
        /// 手机系统版本号，例如：5.0.2
        /// </summary>
        public string Phoneversion { set; get; }

        /// <summary>
        /// 手机设备类型，例如：Redmi Note 3
        /// </summary>
        public string Phonedevice { set; get; }

        #endregion

        /// <summary>
        /// 用户token
        /// </summary>
        public string Token { set; get; }

        /// <summary>
        /// 请求Url
        /// </summary>
        public string RequseUrl { set; get; }

        /// <summary>
        /// 请求Ip
        /// </summary>
        public string Ip { set; get; }

        /// <summary>
        /// 消息
        /// </summary>
        public string Msg { set; get; }

        /// <summary>
        /// 服务器IP
        /// </summary>
        public string ServerIp { get; set; }

        /// <summary>
        /// 其他参数
        /// </summary>
        public Dictionary<string, string> dict { get; set; }
    }
}