﻿namespace BSF.Enums
{
    /// <summary>
    /// sns api返回码
    /// 所有的api返回码应该都注册到返回码列表里面  
    /// 100一个业务区间 之后增加code码需增加在所在的业务区间
    /// </summary>
    public enum BSFAPICode
    {
        #region 系统级别参数

        /// <summary>
        /// access_token过期失效 -9
        /// </summary>
        TokenExpire = -9,

        /// <summary>
        /// access_token无效 -8
        /// </summary>
        TokenInvalid = -8,


        /// <summary>
        /// 请求超时 -7
        /// </summary>
        ReqOverTime = -7,
        /// <summary>
        /// 参数不存在 -6
        /// </summary>
        ParamNoFull = -6,
        /// <summary>
        /// 未找到或不正确 -5
        /// </summary>
        NotExist = -5,

        /// <summary>
        /// sign不正确 -4
        /// </summary>
        SignErr = -4,

        /// <summary>
        /// 签名验证失败 -3
        /// </summary>
        SignInvalid = -3,

        /// <summary>
        /// 缺少参数 -2
        /// </summary>
        MisParameter = -2,

        /// <summary>
        /// 普通错误 -1
        /// </summary>
        NormalError = -1,

        /// <summary>
        /// 正常返回 1
        /// </summary>
        Success = 1,
        /// <summary>
        /// 特殊错误
        /// </summary>
        SpctialError = -500,

        #endregion
    }
}
