﻿using System.ComponentModel;

namespace BSF.Enums
{
    #region 系统消息枚举
    public enum SystemNewsTypeEnum
    {
        [Description("删除")]
        Delete = -1,
        /// <summary>
        /// 未查看
        /// </summary>
        [Description("未查看")]
        Unread = 0,

        /// <summary>
        /// 已读
        /// </summary>
        [Description("已读")]
        Read = 1,
    }

    /// <summary>
    /// 发送对象(1用户端 2 手机端 3 个人推送) 
    /// 接单等消息：f_fsdx=3,f_fslx=1
    /// 现金券消息：f_fsdx=1,f_fslx=2
    /// </summary>
    public enum PushFsdx
    {
        [Description("用户端")]
        Customer = 1,
        [Description("手机端")]
        Mobile = 2,
        [Description("个人推送")]
        PersonPush = 3,
    }

    /// <summary>
    /// 消息类型(1系统发送 2 人工发送)
    /// </summary>
    public enum PushFslx
    {
        [Description("系统发送")]
        System = 1,
        [Description("人工发送")]
        Artificial = 2,
    }


    public class EnumPhonePush
    {
        /// <summary>
        /// 触发推送的条件
        /// </summary>
        public enum EnumPush
        {
            /// <summary>
            /// 其他
            /// </summary>
            Rest = 0,
            /// <summary>
            /// 邀请好友
            /// </summary>
            Invitation = 1,
            /// <summary>
            /// 点赞
            /// </summary>
            Zan = 2,
        }

        public enum EnumJumpAddress
        {
            /// <summary>
            /// 消息列表
            /// </summary>
            NewsList = 0,
        }

    }
    #endregion

    #region 短信枚举
    /// <summary>
    /// 验证码来源枚举
    /// </summary>
    public enum ValidateLYEnum
    {
        /// <summary>
        /// 钱包密码
        /// </summary>
        WalletPass = 1,

        /// <summary>
        /// 钱包验证短信
        /// </summary>
        WalletSMS = 2,

        /// <summary>
        /// 钱包密保
        /// </summary>
        WalletEncrypted = 3
    }

    public enum ReceiptTypeEnum
    {
        /// <summary>
        /// 系统模板
        /// </summary>
        [Description("系统模板")]
        SystemReceipt = 1,
        /// <summary>
        /// 自定义模版
        /// </summary>
        [Description("自定义模版")]
        CustomReceipt = 2
    }

    public enum SendType
    {
        [Description("普通短信")]
        TextMSM = 0,
        [Description("语音短信")]
        VoiceMSM = 1,
    }

    public enum SendStatus
    {
        [Description("失败")]
        Fail = 0,
        [Description("成功")]
        Success = 1,
    }

    #endregion

    #region 设备枚举
    

    /// <summary>
    /// App按钮枚举
    /// </summary>
    public enum AppRedbagBtnConfigTypeEnum
    {
        [Description("IOS6")]
        IosSix = 1,
        [Description("非IOS6")]
        UNIosSix = 2,
        [Description("安卓")]
        Android = 3,
    }

    /// <summary>
    /// 安卓包名枚举
    /// </summary>
    public enum AndroidPKGEnum
    {
        [Description("默认")]
        Defalut = 1,
        [Description("com.dianyadian.consumer")]
        CDC = 2,
        [Description("com.dianyadian.personal")]
        CDP = 3,
        [Description("com.dianyadian.delivery")]
        DMCDD = 101,
    }

    /// <summary>
    /// 用户的 客户端来源
    /// </summary>
    public enum CustomerKHDLYEnum
    {
        /// <summary>
        /// 默认
        /// </summary>
        defalut = 0,

        /// <summary>
        /// ios对应来源APP市场
        /// </summary>
        V20 = 20,

        /// <summary>
        /// ios对应来源 网页下载
        /// </summary>
        V20_2 = 22,

        /// <summary>
        /// 来自安卓 小米3
        /// </summary>
        V30 = 30,

        /// <summary>
        /// 安卓用户端 个推
        /// </summary>
        V30GeTui = 31,

        /// <summary>
        /// 安卓用户端 小米推送
        /// </summary>
        V30XiaoMi = 32,

        /// <summary>
        /// 来自CDC包名的安卓
        /// </summary>
        V30CDC = 330,

        /// <summary>
        /// 安卓用户端 个推 CDC包名版本
        /// </summary>
        V30GeTuiCDC = 331,

        /// <summary>
        /// 安卓用户端 小米推送 CDC包名版本
        /// </summary>
        V30XiaoMiCDC = 332,

        /// <summary>
        /// 来自CDP包名的安卓
        /// </summary>
        V30CDP = 430,

        /// <summary>
        /// 安卓用户端 个推 CDP包名版本
        /// </summary>
        V30GeTuiCDP = 431,

        /// <summary>
        /// 安卓用户端 小米推送 CDP包名版本
        /// </summary>
        V30XiaoMiCDP = 432,

        /// <summary>
        /// IOS 送货员
        /// </summary>
        DeliveryIOS = 1001,

        /// <summary>
        /// 送货员app 极光推送
        /// </summary>
        DeliveryJG = 1010,
        /// <summary>
        /// 送货员app 个推推送
        /// </summary>
        DeliveryGeTui = 1011,

        /// <summary>
        /// 送货员app 小米推送
        /// </summary>
        DeliveryXiaoMI = 1012,
    }
    #endregion
}
