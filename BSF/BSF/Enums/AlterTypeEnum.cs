﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSF.Enums
{
    public enum AlterTypeEnum
    {
        /// <summary>
        /// 不升级 0
        /// </summary>
        [Description("不升级")]
        NoUpdate = 0,
        /// <summary>
        /// 提示升级 1
        /// </summary>
        [Description("提示升级")]
        AlterUpdate,
        /// <summary>
        /// 强制升级 2
        /// </summary>
        [Description("强制升级")]
        ForcedUpdate
    }

    /// <summary>
    /// 是否启用
    /// </summary>
    public enum IsEnabledEnum
    {
        /// <summary>
        /// 否
        /// </summary>
        [Description("否")]
        否 = 0,
        /// <summary>
        /// 是
        /// </summary>
        [Description("是")]
        是 = 1
    }

}
