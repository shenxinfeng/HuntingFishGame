﻿using System.ComponentModel;

namespace BSF.Enums
{
    /// <summary>
    /// 设备类型：设备类型(0 未知1：Android设备；2：iOS设备；3：Windows Phone设备；4：PC设备；5：浏览器设备；)
    /// </summary>
    public enum DeviceTypeEnum
    {
        [Description("默认 不能够推送")]
        Default = 0,
        [Description("安卓")]
        Android = 1,
        [Description("IOS")]
        IOS = 2,
        [Description("浏览器设备")]
        Web = 5
    }

}
