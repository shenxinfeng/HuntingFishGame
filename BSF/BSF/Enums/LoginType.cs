﻿using System.ComponentModel;

namespace BSF.Enums
{
    /// <summary>
    /// 登入方式
    /// </summary>
    [Description("登入方式")]
    public enum LoginType
    {
        [Description("Web登入")]
        Web = 1,
        [Description("Android登入")]
        Android = 2,
        [Description("IOS登入")]
        Ios = 3,
        [Description("微信登入")]
        WeChat = 4,
        [Description("其他")]
        Other = 0
    }
}
