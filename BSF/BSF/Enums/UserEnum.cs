﻿namespace BSF.Enums
{
    //用户枚举

    /// <summary>
    /// 用户类型枚举
    /// </summary>
    public enum UserTypeEnum
    {
        /// <summary>
        /// 默认:根据token查用户,不管是什么角色,取缓存时先取学生，在取家长缓存，最后取老师缓存
        /// </summary>
        sns_default = 0,
        /// <summary>
        /// 学生
        /// </summary>
        sns_student = 1,

        /// <summary>
        /// 家长
        /// </summary>
        sns_parent = 2,

        /// <summary>
        /// 老师
        /// </summary>
        sns_teacher = 3,

        /// <summary>
        /// 无 不需要验证
        /// </summary>
        none = -1
    }

}
