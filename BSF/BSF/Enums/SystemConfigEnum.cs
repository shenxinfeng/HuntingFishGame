﻿using System.ComponentModel;

namespace BSF.Enums
{
    //系统配置枚举

    /// <summary>
    /// 系统配置分类枚举(参数类型)
    /// </summary>
    public enum SystemConfigCategoryEnum
    {
        /// <summary>
        /// 0.默认
        /// </summary>
        Default = 0,
        /// <summary>
        /// 1. 城市功能
        /// </summary>
        CityFeatured = 1,
        /// <summary>
        /// 2.城市参数
        /// </summary>
        CityParams = 2,
        /// <summary>
        /// 4.全局参数
        /// </summary>
        SystemParams = 3
    }

    /// <summary>
    /// 系统配置业务类型枚举(业务类型)
    /// </summary>
    public enum SystemConfigServiceTypeEnum
    {
        /// <summary>
        /// 0 默认 不允许配置为默认
        /// </summary>
        [Description("默认")]
        Default = 0,
        /// <summary>
        /// 3 APP端相关配置
        /// </summary>
        [Description("APP端相关配置")]
        APP = 1,
        /// <summary>
        /// 4 安装更新相关配置
        /// </summary>
        [Description("安装更新相关配置")]
        Update = 2,
        /// <summary>
        /// 2 邀请好友相关配置
        /// </summary>
        [Description("邀请好友相关配置")]
        Invitation = 3,
        /// <summary>
        /// 5 积分相关配置
        /// </summary>
        [Description("积分相关配置")]
        Integral = 5,
        /// <summary>
        /// 6 充值钱包相关配置
        /// </summary>
        [Description("充值钱包相关配置")]
        Wallet = 6,
        /// <summary>
        /// 7 分享相关配置
        /// </summary>
        [Description("分享相关配置")]
        Share = 7,
        /// <summary>
        ///8 测试相关配置
        /// </summary>
        [Description("测试相关配置")]
        Test = 8,
        /// <summary>
        /// 9 无效配置
        /// </summary>
        [Description("无效配置")]
        Invalid = 9,

        [Description("任务调度服务配置")]
        TaskService = 10
    }
    /// <summary>
    /// 试用平台
    /// </summary>
    public enum SystemConfigControlTypeEnum
    {
        /// <summary>
        /// 默认 不限
        /// </summary>
        [Description("全部")]
        Default = 0,

        /// <summary>
        /// Capi使用
        /// </summary>
        [Description("CAPI")]
        CAPI = 1,

        /// <summary>
        /// APP使用
        /// </summary>
        [Description("SNSAPP")]
        SNSAPP = 1,



    }

}
