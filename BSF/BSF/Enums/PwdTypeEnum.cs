﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSF.Enums
{
    /// <summary>
    /// 密码类型
    /// </summary>
    public enum PwdTypeEnum
    {
        /// <summary>
        /// 游戏账号密码
        /// </summary>
        [Description("游戏账号密码")]
        GamePwd = 0,
        /// <summary>
        /// 游戏银行密码
        /// </summary>
        [Description("游戏银行密码")]
        GameBankPwd = 1,
    }
}
