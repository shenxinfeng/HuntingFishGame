﻿namespace BSF.Enums
{
    public enum LogLevelEnum
    {
        Other = -1,
        Trace = 0,
        Debug = 1,
        Info = 2,
        Warn = 3,
        Error = 4,
        Fatal = 5,
        Off = 6
    }
}
