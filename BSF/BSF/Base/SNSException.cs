﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSF.Enums;

namespace BSF.Base
{
    /// <summary>
    /// dyd错误
    /// </summary>
    public class SNSException : Exception
    {
        public BSFAPICode Code = BSFAPICode.NormalError;//自定义错误码

        public string Message = "";

        public object Data = "";

        public SNSException(BSFAPICode Code, string Message)
        {
            this.Code = Code;
            this.Message = Message;
            this.Data = null;
        }

        public SNSException(BSFAPICode Code, string Message, object Data)
        {
            this.Code = Code;
            this.Message = Message;
            this.Data = Data;
        }
    }
}
