﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSF.Queues
{
    public interface IActionBlock<T>//internal
    {
        void Enqueue(T item);

        void Flush();
    }
}
