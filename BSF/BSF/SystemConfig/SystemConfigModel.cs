﻿namespace BSF.SystemConfig
{
    public class SystemConfigModel
    {
        public string f_key { get; set; }
        public string f_value { get; set; }
        public int f_area { get; set; }
        public int f_category { get; set; }
        public int f_Control { get; set; }
    }
}
