﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;

namespace BSF.SystemConfig
{
    /// <summary>
    /// 邀请有礼配置对象
    /// </summary>
    public class InvitationModel
    {
        /// <summary>
        /// 活动开始时间
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// 活动结束时间
        /// </summary>
        public DateTime EndTime { get; set; }
        /// <summary>
        /// 有效天数(在有效天数内必须完成任务,使用优惠券，否则将过期)
        /// </summary>
        public int EffectiveDays { get; set; }
        /// <summary>
        /// 邀请人数达到数量时，发放奖励
        /// </summary>
        public int InvitedNum { get; set; }
        /// <summary>
        /// 邀请人奖励金额
        /// </summary>
        public int InvitedRewardAmount { get; set; }
        /// <summary>
        /// 被邀请人奖励金额
        /// </summary>
        public int BeInvitedRewardAmount { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 简介
        /// </summary>
        public string Summary { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        public string Pic { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 奖励规则地址
        /// </summary>
        public string RuleUrl { get; set; }
        /// <summary>
        /// 邀请的列表地址->邀请页面底部内容跳转地址
        /// </summary>
        public string InvitedListUrl { get; set; }
        /// <summary>
        /// 邀请页面上面的标题->邀请你身边的老师加入并成功激活班级...
        /// </summary>
        public string InvitedTitle { get; set; }
        /// <summary>
        /// 邀请页面底部内容->已成功邀请0人,累计获得奖励0元
        /// </summary>
        public string InvitedContent { get; set; }
        /// <summary>
        /// 邀请短信内容
        /// </summary>
        public string SmsContent { get; set; }
    }
}