﻿using System;
using BSF.Enums;

namespace BSF.SystemConfig
{
    /// <summary>
    /// 业务配置文件
    /// 该类使用tb_system_config下面的配置表里面的信息，同时会有一小段时间的缓存
    /// 举例:new tb_system_config_dal().GetCache(SnsProcessConfig.OneInvitedRegisterMoneyCoupons, 0)
    /// </summary>
    public class SnsProcessConfig
    {

        /// <summary>
        /// 省市区配置
        /// </summary>
        public static SnsProcessConfigInfo ProvinceCityConfig = new SnsProcessConfigInfo("ProvinceCityConfig", "http://192.168.17.201/provincecityarea.json?md5=9bcf24ba0fbeed7e0d555cd45c59c3a3", "省市区配置", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.APP, SystemConfigControlTypeEnum.SNSAPP);

        /// <summary>
        /// 分布式缓存开关,true=开启缓存,false-关闭缓存
        /// </summary>
        public static SnsProcessConfigInfo DistributedCacheSwitch = new SnsProcessConfigInfo("DistributedCacheSwitch", "false", "分布式缓存开关,true=开启缓存,false-关闭缓存", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.APP, SystemConfigControlTypeEnum.CAPI);

        /// <summary>
        /// 日志开关 true启用 false不启用 默认true（商户类目）
        /// </summary>
        public static SnsProcessConfigInfo LogSwitch = new SnsProcessConfigInfo("LogSwitch", "true", "日志开关 true启用 false不启用 默认true（所有日志）", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.APP, SystemConfigControlTypeEnum.CAPI);

        /// <summary>
        /// 小红点开关
        /// </summary>
        public static SnsProcessConfigInfo XHDSwitch = new SnsProcessConfigInfo("XHDSwitch", "false", "小红点开关", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.APP, SystemConfigControlTypeEnum.CAPI);

        /// <summary>
        /// 是否打开降级方案
        /// </summary>
        public static SnsProcessConfigInfo IsOpenLowerSolution = new SnsProcessConfigInfo("IsOpenLowerSolution", "false", "是否允许版本降级(启用缓存数据库)的开关（false表示不允许，true表示允许）", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.Update, SystemConfigControlTypeEnum.CAPI);

        /// <summary>
        /// 降级方案配置
        /// </summary>
        public static SnsProcessConfigInfo JJFAConfigJson = new SnsProcessConfigInfo("JJFAConfigJson", "{\"isopen\":false,\"levelnum\":1," +
            "\"levelone\":[{\"key\":\"tjsp\",\"value\":\"true\"},{\"key\":\"sssp\",\"value\":\"true\"},{\"key\":\"sybk\",\"value\":\"true\"},{\"key\":\"cnxh\",\"value\":\"true\"},{\"key\":\"syrz\",\"value\":\"true\"},{\"key\":\"sysp\",\"value\":\"true\"},{\"key\":\"worklog\",\"value\":\"true\"},{\"key\":\"pjjj\",\"value\":\"true\"},{\"key\":\"xhd\",\"value\":\"true\"},{\"key\":\"qjs\",\"value\":\"true\"},{\"key\":\"ddjj\",\"value\":\"true\"},{\"key\":\"syphjj\",\"value\":\"true\"}]," +
            "\"leveltwo\":[{\"key\":\"spms\",\"value\":\"true\"},{\"key\":\"wd\",\"value\":\"true\"},{\"key\":\"sjddlb\",\"value\":\"true\"},{\"key\":\"sjddfb\",\"value\":\"true\"},{\"key\":\"sjxsehz\",\"value\":\"true\"},{\"key\":\"sjpj\",\"value\":\"true\"},{\"key\":\"sjddtx\",\"value\":\"true\"}]}", "降级方案配置", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.APP, SystemConfigControlTypeEnum.CAPI);

        /// <summary>
        /// 消息通知插入方式：0-MQ，1-Sql
        /// </summary>
        public static SnsProcessConfigInfo MsgremindInsertTypeConfig = new SnsProcessConfigInfo("MsgremindInsertTypeConfig", "1", "消息通知插入方式：0-MQ，1-Sql", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.APP, SystemConfigControlTypeEnum.CAPI);

        /// <summary>
        /// 消息写入方式(写入方式:0-直接保存到数据库,1-添加到MQ)
        /// </summary>
        public static SnsProcessConfigInfo SystemNewsWriteType = new SnsProcessConfigInfo("SystemNewsWriteType", "0", "消息写入方式(写入方式:0-直接保存到数据库,1-添加到MQ)", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.APP, SystemConfigControlTypeEnum.CAPI);
        
        /// <summary>
        /// 测试用户
        /// </summary>
        public static SnsProcessConfigInfo ViewYh = new SnsProcessConfigInfo("ViewYh", "18668086801，18676708659，15868152697", "测试用户的登录账号（手机号码）", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.Test, SystemConfigControlTypeEnum.CAPI);
        
        #region 积分业务分配
        /// <summary>
        /// 积分签到得分
        /// </summary>
        public static SnsProcessConfigInfo IntegralSignInValue = new SnsProcessConfigInfo("IntegralSignInValue", "5", "1、未开启连续签到，表示每次签到可以获得的积分值（每次签到获得5积分）。2、开启连续签到，表示首次签到的初始值（连续签到，第一次获得5积分）", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.Integral);
        /// <summary>
        /// 积分签到增量值
        /// </summary>
        public static SnsProcessConfigInfo IntegralSignInIncreasingValue = new SnsProcessConfigInfo("IntegralIncreasingValue", "5", "连续签到，积分增值（参数为5，表示每连续签到一天，签到获得的积分都会比前一天多5分）", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.Integral);
        /// <summary>
        /// 积分连续签到开关
        /// </summary>
        public static SnsProcessConfigInfo IntegralSeriesSigninSwitch = new SnsProcessConfigInfo("IntegralSeriesSigninSwitch", "true", "积分签到开关，（true表示当前可以使用，false表示当前不能使用）", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.Integral);
        /// <summary>
        /// 积分签到顶峰值
        /// </summary>
        public static SnsProcessConfigInfo IntegralSignInMaxValue = new SnsProcessConfigInfo("IntegralSignInMaxValue", "50", "每日积分签到，能达到的最大值（表示最高每日签到能获得25积分）", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.Integral);
        /// <summary>
        /// 积分每日最高限额
        /// </summary>
        public static SnsProcessConfigInfo IntegralLimitValue = new SnsProcessConfigInfo("IntegralLimitValue", "50", "每日可以获得积分的上限（表示每天最多能获得1000积分）", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.Integral);
        /// <summary>
        /// 积分商城H5页面配置地址
        /// </summary>
        public static SnsProcessConfigInfo IntegralMallUrl = new SnsProcessConfigInfo("IntegralMallUrl", "http://192.168.17.244:8004/integral/mall", "积分商城h5页面的链接地址", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.APP, SystemConfigControlTypeEnum.CAPI);
        #endregion

        #region 全局参数

        /// <summary>
        /// 是否开启安全签名验证
        /// </summary>
        public static SnsProcessConfigInfo IsOpenSecureSignatureVerification = new SnsProcessConfigInfo("IsOpenSecureSignatureVerification", "false", "是否开启安全签名验证", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.APP, SystemConfigControlTypeEnum.CAPI);

        /// <summary>
        /// 安全签名超时时间 单位 分钟
        /// </summary>
        public static SnsProcessConfigInfo SecureSignatureVerificationIntervalTime = new SnsProcessConfigInfo("SecureSignatureVerificationIntervalTime", "5", "安全签名超时时间 单位 分钟", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.APP, SystemConfigControlTypeEnum.CAPI);

        /// <summary>
        /// 是否开启限流(阻止部分用户访问 用于减轻服务器压力)
        /// </summary>
        public static SnsProcessConfigInfo IsOpenRestrictor = new SnsProcessConfigInfo("IsOpenRestrictor", "false", "是否开启限流(阻止部分用户访问 用于减轻服务器压力)", SystemConfigCategoryEnum.CityParams, SystemConfigServiceTypeEnum.APP);

        /// <summary>
        /// 限流规则 (阻止部分用户访问 用于减轻服务器压力)   -1-未登录用户,0,1,2-对应的用户手机尾号,值示例:0,1,-1 
        /// </summary>
        public static SnsProcessConfigInfo RestrictorRule = new SnsProcessConfigInfo("RestrictorRule", "", "限流规则 (阻止部分用户访问 用于减轻服务器压力)  -1-未登录用户,0,1,2-对应的用户手机尾号,值示例:0,1,-1 ", SystemConfigCategoryEnum.CityParams, SystemConfigServiceTypeEnum.APP);

        /// <summary>
        /// 限流规则中的控制器配置 (阻止部分用户访问 用于减轻服务器压力)  英文逗号分隔 
        /// </summary>
        public static SnsProcessConfigInfo RestictorControllerRule = new SnsProcessConfigInfo("RestictorControllerRule", "goods/getshopcateandgood", "限流规则中的控制器配置 (阻止部分用户访问 用于减轻服务器压力)  英文逗号分隔", SystemConfigCategoryEnum.CityParams, SystemConfigServiceTypeEnum.APP);
        
        /// <summary>
        /// 加盟热线  (首页无店铺的时候显示)
        /// </summary>
        public static SnsProcessConfigInfo JoinHotline = new SnsProcessConfigInfo("JoinHotline", "", "加盟热线  (首页无店铺的时候显示)", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.APP, SystemConfigControlTypeEnum.SNSAPP);

        /// <summary>
        /// 加盟热线是否显示  (0-不显示，1-显示)
        /// </summary>
        public static SnsProcessConfigInfo JoinHotlineShow = new SnsProcessConfigInfo("JoinHotlineShow", "", "加盟热线是否显示:0-不显示,1-显示", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.APP, SystemConfigControlTypeEnum.SNSAPP);

        //超级用户 无需验证验证码 逗号分隔
        public static SnsProcessConfigInfo SuperYhzh = new SnsProcessConfigInfo("SuperYhzh", "", "超级用户 无需验证验证码", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.APP, SystemConfigControlTypeEnum.SNSAPP);

        /// <summary>
        /// 一个设备号能注册的用户数量
        /// </summary>
        public static SnsProcessConfigInfo invitecodeNumber = new SnsProcessConfigInfo("invitecodeNumber", "10", "一个设备号能注册的用户数量（参数1，表示1台手机只能注册1个账号）", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.APP, SystemConfigControlTypeEnum.SNSAPP);

        /// <summary>
        /// 反馈时间间隔（分）
        /// </summary>
        public static SnsProcessConfigInfo FeedbackInterval = new SnsProcessConfigInfo("FeedbackInterval", "10", "手机端-我的-用户反馈，用户每条反馈消息的间隔时间（10表示用户最少需要间隔10分钟，才能发送一条反馈消息给点呀点）", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.APP, SystemConfigControlTypeEnum.SNSAPP);

        /// <summary>
        /// 验证码失败次数
        /// </summary>
        public static SnsProcessConfigInfo VerifiyCodeTryNums = new SnsProcessConfigInfo("VerifiyCodeTryNums", "10", "最多允许用户输错验证码的次数（参数为10，表示用户最多能输错10次）", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.APP, SystemConfigControlTypeEnum.CAPI);
        
        /// <summary>
        /// 默认采用的地图 0高德|1腾讯
        /// </summary>
        public static SnsProcessConfigInfo MapUser = new SnsProcessConfigInfo("MapUser", "0高德|1腾讯", "默认使用的地图（0表示高德地图，1表示腾讯地图）", SystemConfigCategoryEnum.SystemParams, SystemConfigServiceTypeEnum.APP, SystemConfigControlTypeEnum.Default);
        #endregion
    }

    public class SnsProcessConfigInfo
    {
        public string Key { get; set; }//key值
        public string DefaultValue { get; set; }//如果配置不在数据库中，则自动填充到数据库的默认值,如果在数据库中存在，则使用数据库的配置
        public string Remark { get; set; }//备注信息
        public SystemConfigCategoryEnum SystemConfigCategory { get; set; }//系统配置分类（用于人工在后台分类）
        public SystemConfigServiceTypeEnum SystemConfigServiceType { get; set; }//系统配置业务类型枚举
        public SystemConfigControlTypeEnum SystemConfigControlType { get; set; }//系统配置业务控制枚举

        /// <summary>
        /// 业务配置信息
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultvalue">如果配置不在数据库中，则自动填充到数据库的默认值,如果在数据库中存在，则使用数据库的配置</param>
        /// <param name="remark"></param>
        /// <param name="systemconfigcategory"></param>
        public SnsProcessConfigInfo(string key, string defaultvalue, string remark, SystemConfigCategoryEnum systemconfigcategory, SystemConfigServiceTypeEnum serviceType)
            : this(key, defaultvalue, remark, systemconfigcategory, serviceType, SystemConfigControlTypeEnum.Default)
        {

        }

        /// <summary>
        /// 业务配置信息
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultvalue">如果配置不在数据库中，则自动填充到数据库的默认值,如果在数据库中存在，则使用数据库的配置</param>
        /// <param name="remark"></param>
        /// <param name="systemconfigcategory"></param>
        public SnsProcessConfigInfo(string key, string defaultvalue, string remark, SystemConfigCategoryEnum systemconfigcategory, SystemConfigServiceTypeEnum serviceType, SystemConfigControlTypeEnum controlType)
        {
            Key = key;
            DefaultValue = defaultvalue;
            Remark = remark;
            SystemConfigCategory = systemconfigcategory;
            SystemConfigServiceType = serviceType;
            SystemConfigControlType = controlType;
            if (serviceType == SystemConfigServiceTypeEnum.Default)
            {
                throw new Exception("未设置系统设置业务类型枚举:" + key);
            }
            if (systemconfigcategory == SystemConfigCategoryEnum.Default)
            {
                throw new Exception("未设置系统参数类型:" + key);
            }
        }

    }
}
