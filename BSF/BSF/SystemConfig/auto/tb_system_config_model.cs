using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.ComponentModel;

namespace BSF.SystemConfig
{
    /// <summary>
    /// tb_system_config Data Structure.
    /// </summary>
    [Serializable]
    public partial class tb_system_config_model
    {
	/*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦*/
        
        /// <summary>
        /// 关键词
        /// </summary>
        public string key { get; set; }
        
        /// <summary>
        /// 值
        /// </summary>
        public string value { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public string remark { get; set; }
        
        /// <summary>
        /// 地区 0为全国
        /// </summary>
        public int? area { get; set; }
        
        /// <summary>
        /// 0.默认 1. 城市功能 2.城市参数 3.商家参数 4.全局参数
        /// </summary>
        public Byte category { get; set; }

        /// <summary>
        /// 业务类型 0未知 1.商家后台相关配置 2.邀请好友相关配置 3.APP端相关配置 4.安装更新相关配置 5.积分相关配置
        /// 6.充值钱包相关配置 7.分享相关配置 8.测试相关配置 9.失效配置
        /// </summary>
        public byte serviceType { get; set; }

        /// <summary>
        /// 最后更新时间
        /// </summary>
        public DateTime lastUpdateTime { get; set; }

        /// <summary>
        /// 是否删除
        /// </summary>
        public bool isDel { get; set; }

        /// <summary>
        /// 控制层
        /// </summary>
        public int control { get; set; }
    }

   
}