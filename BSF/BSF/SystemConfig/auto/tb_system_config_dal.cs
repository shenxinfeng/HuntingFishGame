using System.Data;
using System.Text;
using BSF.Db;
using BSF.Extensions;

namespace BSF.SystemConfig
{
	/*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦*/
	public partial class tb_system_config_dal
    {
        public virtual bool Add(DbConn pubConn, tb_system_config_model model)
        {
            return SqlHelper.Visit(ps =>
            {
                //List<ProcedureParameter> Par = new List<ProcedureParameter>() {};

                //值
                ps.Add("f_value", model.value);
                //备注
                ps.Add("f_remark", model.remark);
                //地区 0为全国
                ps.Add("f_area", model.area);
                //0.默认 1. 城市功能 2.城市参数 3.商家参数 4.全局参数
                ps.Add("f_category", model.category);

                int rev = pubConn.ExecuteSql(@"insert into tb_system_config(f_value,f_remark,f_area,f_category)
										   values(@f_value,@f_remark,@f_area,@f_category)", ps.ToParameters());
                return rev == 1;
            });
        }

        public virtual bool Edit(DbConn pubConn, tb_system_config_model model)
        {
            return SqlHelper.Visit(ps =>
            {
                //List<ProcedureParameter> Par = new List<ProcedureParameter>() {};

                //值
                ps.Add("f_value", model.value);
                //备注
                ps.Add("f_remark", model.remark);
                //地区 0为全国
                ps.Add("f_area", model.area);
                //0.默认 1. 城市功能 2.城市参数 3.商家参数 4.全局参数
                ps.Add("f_category", model.category);

                ps.Add("f_key", model.key);

                int rev = pubConn.ExecuteSql("update tb_system_config set f_value=@f_value,f_remark=@f_remark,f_area=@f_area,f_category=@f_category where f_key=@f_key", ps.ToParameters());
                return rev == 1;
            });
        }

        public virtual bool Delete(DbConn pubConn, string f_key)
        {
            return SqlHelper.Visit(ps =>
            {
            //List<ProcedureParameter> Par = new List<ProcedureParameter>();
            ps.Add("f_key",  f_key);
            string Sql = "delete tb_system_config where f_key=@f_key";
            int rev = pubConn.ExecuteSql(Sql, ps.ToParameters());
            if (rev == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
            });
        }

        public virtual tb_system_config_model Get(DbConn pubConn, string f_key)
        {
            return SqlHelper.Visit(ps =>
            {
                //List<ProcedureParameter> Par = new List<ProcedureParameter>();
                ps.Add("f_key", f_key);
                StringBuilder stringSql = new StringBuilder();
                stringSql.Append(@"select s.* from tb_system_config s where s.f_key=@f_key");
                //pubConn.ExecuteSql(stringSql.ToString().ToOracleSql(), Par);
                DataSet ds = new DataSet();
                pubConn.SqlToDataSet(ds, stringSql.ToString(), ps.ToParameters());
                if (ds != null && ds.Tables.Count > 0)
                {
                    return CreateModel(ds.Tables[0].Rows[0]);
                }
                return null;
            });
        }

		public static tb_system_config_model CreateModel(DataRow dr)
        {
            var o = new tb_system_config_model();
			
			//关键词
			if(dr.Table.Columns.Contains("f_key"))
			{
				o.key = dr["f_key"].Tostring();
			}
			//值
			if(dr.Table.Columns.Contains("f_value"))
			{
				o.value = dr["f_value"].Tostring();
			}
			//备注
			if(dr.Table.Columns.Contains("f_remark"))
			{
				o.remark = dr["f_remark"].Tostring();
			}
			//地区 0为全国
			if(dr.Table.Columns.Contains("f_area"))
			{
				o.area = dr["f_area"].Toint();
			}
			//0.默认 1. 城市功能 2.城市参数 3.商家参数 4.全局参数
			if(dr.Table.Columns.Contains("f_category"))
			{
				o.category = dr["f_category"].ToByte();
			}
            //最后更新时间
            if (dr.Table.Columns.Contains("f_last_update_time"))
            {
                o.lastUpdateTime =LibConvert.ObjToDateTime(dr["f_last_update_time"]);
            }
            //是否删除
            if (dr.Table.Columns.Contains("f_isdel"))
            {
                o.isDel = LibConvert.ObjToBool(dr["f_isdel"]);
            }
			return o;
        }
    }
}