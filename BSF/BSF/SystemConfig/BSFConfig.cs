﻿using System;
using System.Linq;
using BSF.Tool;

namespace BSF.SystemConfig
{
    /// <summary>
    /// 项目全局配置
    /// </summary>
    public static class BSFConfig
    {
        static BSFConfig()
        {
            //try
            //{
            //    ConfigInit();
            //}
            //catch { }
        }

        #region 写日志
        /// <summary>
        /// 是否写错误日志
        /// </summary>
        public static bool IsWriteErrorLog { get { return Convert.ToBoolean(Get("IsWriteErrorLog", "true")); } }
        /// <summary>
        /// 错误日志是否写入监控平台
        /// </summary>
        public static bool IsWriteErrorLogToMonitorPlatform { get { return Convert.ToBoolean(Get("IsWriteErrorLogToMonitorPlatform", "false")); } }
        /// <summary>
        /// 错误日志是否写入本地文件
        /// </summary>
        public static bool IsWriteErrorLogToLocalFile { get { return Convert.ToBoolean(Get("IsWriteErrorLogToLocalFile", "true")); } }
        /// <summary>
        /// 是否写常用日志
        /// </summary>
        public static bool IsWriteCommonLog { get { return Convert.ToBoolean(Get("IsWriteCommonLog", "true")); } }
        /// <summary>
        /// 常用日志是否写入监控平台
        /// </summary>
        public static bool IsWriteCommonLogToMonitorPlatform { get { return Convert.ToBoolean(Get("IsWriteCommonLogToMonitorPlatform", "false")); } }
        /// <summary>
        /// 常用日志是否写入本地文件
        /// </summary>
        public static bool IsWriteCommonLogToLocalFile { get { return Convert.ToBoolean(Get("IsWriteCommonLogToLocalFile", "true")); } }
        /// <summary>
        /// 是否写耗时日志
        /// </summary>
        public static bool IsWriteTimeWatchLog { get { return Convert.ToBoolean(Get("IsWriteTimeWatchLog", "false")); } }
        /// <summary>
        /// 耗时日志是否写入监控平台
        /// </summary>
        public static bool IsWriteTimeWatchLogToMonitorPlatform { get { return Convert.ToBoolean(Get("IsWriteTimeWatchLogToMonitorPlatform", "false")); } }
        /// <summary>
        /// 耗时日志是否写入本地文件
        /// </summary>
        public static bool IsWriteTimeWatchLogToLocalFile { get { return Convert.ToBoolean(Get("IsWriteTimeWatchLogToLocalFile", "false")); } }
        #endregion

        #region 数据库连接
        /// <summary>
        /// 是否拦截访问日志(直接根据TimeWatchConnectionString是否为空，判断是否写入接口访问日志)
        /// </summary>
        //public static bool IsWriteVisitLog { get { return Convert.ToBoolean(Get("IsWriteVisitLog", "false")); } }

        /// <summary>
        /// 耗时库连接
        /// </summary>
        //public static string TimeWatchConnectString { get { return Get("TimeWatchConnectString", ""); } }

        /// <summary>
        /// 耗时监控数据库连接
        /// </summary>
        public static string TimeWatchConnectionString { get { return Get("TimeWatchConnectionString", ""); } }

        /// <summary>
        /// 监控平台数据库连接
        /// </summary>
        public static string MonitorPlatformConnectionString { get { return Get("MonitorPlatformConnectionString", ""); } }//server=192.168.17.200;Initial Catalog=bs_MonitorPlatform;User ID=sa;Password=Xx~!@#;

        /// <summary>
        /// 主库数据库连接(取配置中心配置)
        /// </summary>
        //public static string MainConnectString { get { return Get("MainConnectString", ""); } }//server=192.168.17.200;Initial Catalog=dyd_new_main;User ID=sa;Password=Xx~!@#;
        public static string MainConnectString { get; set; }

        /// <summary>
        /// 游戏库连接
        /// </summary>
        public static string GameConnectString { get; set; }

        ///// <summary>
        ///// 配置数据库连接
        ///// </summary>
        //public static string ConfigConnectString { get { return _ConfigConnectString == null ? Get("ConfigConnectString", "") : _ConfigConnectString; } set { _ConfigConnectString = value; } }//server=192.168.17.200;Initial Catalog=dyd_new_main;User ID=sa;Password=Xx~!@#;
        //private static string _ConfigConnectString;//用于兼容“任务调度中心”的使用

        /// <summary>
        /// 当前项目名称
        /// </summary>
        //public static string ProjectName = Get("ProjectName", "未命名项目");
        public static string ProjectName { get { return _projectName ?? Get("ProjectName", ""); } set { _projectName = value; } }
        private static string _projectName;//用于兼容“任务调度中心”的使用

        /// <summary>
        /// 统一配置中心数据库连接
        /// </summary>
        //public static string ConfigManagerConnectString = Get("ConfigManagerConnectString", "");//server=192.168.17.200;Initial Catalog=dyd_new_main;User ID=sa;Password=Xx~!@#;
        public static string ConfigManagerConnectString { get { return _configManagerConnectString ?? Get("ConfigManagerConnectString", ""); } set { _configManagerConnectString = value; } }
        private static string _configManagerConnectString;//用于兼容“任务调度中心”的使用
        #endregion

        #region 其他配置
        /// <summary>
        /// 当前项目默认开发人员
        /// </summary>
        public static string ProjectDeveloper { get { return ConfigHelper.Get("ProjectDeveloper", ""); } }

        /// <summary>
        /// 集群性能监控库连接
        /// </summary>
        public static string ClusterConnectString { get { return ConfigHelper.Get("ClusterConnectString", ""); } }

        /// <summary>
        /// 集群性能监控库连接
        /// </summary>
        public static string PlatformManageConnectString { get { return ConfigHelper.Get("PlatformManageConnectString", ""); } }

        /// <summary>
        /// 集群性能监控库连接
        /// </summary>
        public static string UnityLogConnectString { get { return ConfigHelper.Get("UnityLogConnectString", ""); } }

        /// <summary>
        /// 业务消息推送平台连接
        /// </summary>
        public static string NotifyPushConnectString { get { return ConfigHelper.Get("NotifyPushConnectString", ""); } }

        /// <summary>
        /// 创建月表OR日表SQL路径
        /// </summary>
        public static string TableCreateSqlTxtUrl { get { return Get("DayTableCreateSqlTxtUrl", ""); } }

        /// <summary>
        /// 创建DLL执行创建的类型（Day：日表,Month：月表）
        /// </summary>
        public static string TableCreateType { get { return Get("TableCreateType", ""); } }

        #region
        /// <summary>
        /// 是否开启安全签名验证
        /// </summary>
        public static bool IsOpenSecureSignatureVerification { get { return Convert.ToBoolean(Get("IsOpenSecureSignatureVerification", "false")); } }
        /// <summary>
        /// 安全签名超时时间 单位 分钟
        /// </summary>
        public static int SecureSignatureVerificationIntervalTime { get { return Convert.ToInt32(Get("SecureSignatureVerificationIntervalTime", "5")); } }
        #endregion

        #endregion

        #region 微信配置
        /// <summary>
        /// 微信配置信息(未使用)
        /// </summary>
        public static string WeChatConfig
        {
            get
            {
                return ConfigHelper.Get("WeChatConfig", "[{\"AppID\":\"wx0848e6965d142f2e\"," +
                                                        "\"AppSecret\":\"d4624c36b6795d1d99dcf0547af5443d\"" +
                                                        "\"WeChatCallBackHost\":\"http://www.baidu.com\"" +
                                                        "\"WeixinType\":\"1\"}]");
            }
        }

        /// <summary>
        /// 二维码保存路径
        /// </summary>
        public static string QRImage { get { return Get("QRImage", "/UploadFile/QRImage"); } }

        /// <summary>
        /// 下载多媒体文件目录
        /// </summary>
        public static string DownloadFile { get { return Get("DownloadFile", "/UploadFile/DownloadFile"); } }
        /// <summary>
        /// 消息转发到多客服系统时间，"09:00-20:00"
        /// </summary>
        public static string AutoDKFTimeInterval { get { return Get("AutoDKFTimeInterval", "00:00-00:01"); } }

        /// <summary>
        /// 不在客服工作时间,同时也未找到匹配的自动回复关键词时，自动回复的内容
        /// </summary>
        public static string AutoDKFText
        {
            get
            {
                return Get("AutoText", @"亲爱的点友，感谢您的反馈。
为更快更及时受理您的需求，请直接致电：4001618618，或加在线QQ：4008261418。
PS：客服软妹纸糯糯的声音已完爆志玲姐姐哦！么么哒~");
            }
        }

        /// <summary>
        /// 联系客服自动回复内容
        /// </summary>
        public static string AutolLXKFText
        {
            get
            {
                return Get("AutolLXKFText", @"亲爱的点友，输入文字后会进入人工客服哦！么么哒~"); }
        }

        /// <summary>
        /// 微信模版
        /// </summary>
        public static string WeChatTemplate { get { return Get("WeChatTemplate", "{\"TemplateId\":\"-hoLYFbzjdg4RxXUgr9smwzantzCC75sAx8k1CZ1Ckw\"," +
                                                                                 "\"TemplateContent\":\"您的验证码是:{{verifyCode.DATA}}到账时间: {{arrivedTime.DATA}}{{remark.DATA}}\"," +
                                                                                 "\"TemplateUrl\":\"http://www.baidu.com/\"," +
                                                                                 "\"TopColor\":\"#173177\"}"); } }

        #endregion

        #region H5地址配置
        public static string H5HostUrl { get; set; }
        #endregion

        /// <summary>
        /// 读取配置
        /// 1.没有配置配置中心地址时，读取配置文件
        /// 2.优先读取配置文件配置
        /// 3.配置文件不存在,读取配置中心配置
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultvalue"></param>
        /// <returns></returns>
        public static string Get(string key, string defaultvalue = "")
        {

            //没有配置配置中心地址时，读取配置文件
            if (key == "ConfigManagerConnectString" || key == "ProjectName" || ProjectName == "未命名项目" || string.IsNullOrWhiteSpace(ProjectName) || string.IsNullOrWhiteSpace(ConfigManagerConnectString))
            {
                if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains(key))
                {
                    return System.Configuration.ConfigurationManager.AppSettings[key];
                }
            }
            else
            {
                //优先读取配置文件配置,配置文件不存在,读取配置中心配置
                if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains(key))
                {
                    return System.Configuration.ConfigurationManager.AppSettings[key];
                }
                else
                {
                    //TODO modify by shenxf
                    //读取配置中心
                    //try
                    //{
                    //    var value = ConfigManagerHelper.Get<string>(key);
                    //    if (value != null)
                    //        return value;
                    //}
                    //catch
                    //{
                    return defaultvalue;
                    //}
                }
            }
            return defaultvalue;
        }

        /// <summary>
        /// 只读取配置文件的配置
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="defaultvalue">默认值</param>
        /// <returns></returns>
        public static string GetConfig(string key, string defaultvalue = "")
        {
            if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains(key))
                return System.Configuration.ConfigurationManager.AppSettings[key];
            else
                return defaultvalue;
        }
    }
}
