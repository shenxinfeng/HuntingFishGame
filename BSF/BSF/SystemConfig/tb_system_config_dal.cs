using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using BSF.Base;
using BSF.Caches;
using BSF.Db;
using BSF.Enums;
using BSF.Extensions;

namespace BSF.SystemConfig
{
    public partial class tb_system_config_dal
    {
        private static readonly string configKey = "sns_system_conifg";
        private static readonly int intervalTime = 20;
        public static ConfigCacheProvider<List<tb_system_config_model>> configCacheProvider = new ConfigCacheProvider<List<tb_system_config_model>>(configKey, RefreshData, RefreshDataByLocalFile, intervalTime);
        public static Dictionary<string, tb_system_config_model> configData = new Dictionary<string, tb_system_config_model>();

        /// <summary>
        /// 刷新数据从数据库
        /// </summary>
        /// <returns></returns>
        private static List<tb_system_config_model> RefreshData(DateTime lastRefeshTime)
        {
            List<tb_system_config_model> models = new List<tb_system_config_model>();
            using (DbConn pubConn = DbConn.CreateConn(BSFConfig.MainConnectString))
            {
                pubConn.Open();
                if (lastRefeshTime != default(DateTime))
                {
                    lastRefeshTime = lastRefeshTime.AddSeconds(-intervalTime);
                }
                //如果没有数据则继续进行全量更新
                if (configData == null || configData.Count == 0)
                {
                    models = List(pubConn);
                }
                else
                {
                    models = List(pubConn, lastRefeshTime);
                }

                if (configData == null)
                {
                    configData = new Dictionary<string, tb_system_config_model>();
                }
                foreach (var m in models)
                {
                    string dicKey = GetDicKey(m.key, m.area.Tostring());
                    if (m.isDel)
                    {
                        string realKey = m.key.Replace("Del_", "");
                        //是否有一个存活着的相同key的数据
                        bool hasAliveKey = false;
                        foreach (var n in models)
                        {
                            if (n.key == realKey && n.area == n.area)
                            {
                                hasAliveKey = true;
                                break;
                            }
                        }
                        //如果没有 则清除缓存里的数据
                        if (!hasAliveKey)
                        {
                            dicKey = GetDicKey(realKey, m.area.Tostring());
                            configData.Remove(dicKey);
                        }
                        //从数据库中删除这条逻辑删除的数据
                        //  DeleteConfig(pubConn, m.key);
                    }
                    else
                    {
                        if (configData.ContainsKey(dicKey))
                        {
                            configData[dicKey] = m;
                        }
                        else
                        {
                            configData.Add(dicKey, m);
                        }
                    }
                }
            }
            List<tb_system_config_model> data = new List<tb_system_config_model>(configData.Values);
            return data;
        }

        /// <summary>
        /// 刷新数据从本地文件
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        private static void RefreshDataByLocalFile(List<tb_system_config_model> dic)
        {
            if (dic != null && dic.Count > 0)
            {
                configData.Clear();
                foreach (var m in dic)
                {
                    configData.Add(GetDicKey(m.key, m.area.Tostring()), m);
                }
            }
        }

        /// <summary>
        /// 得到key值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="dqbm"></param>
        /// <returns></returns>
        private static string GetDicKey(string key, string dqbm)
        {
            string keyTemp = "SystemConfig_{0}_{1}";
            return string.Format(keyTemp, key, dqbm);
        }


        //禁止增加获取全局缓存配置的 逻辑  ！！！ 已无此概念
        /// <summary>
        /// 获取全局缓存配置文件 默认缓存60秒
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        //public virtual string GetCache(SnsProcessConfigInfo info)
        //{
        //    return GetCache(info, 0);
        //}


        /// <summary>
        /// 获取城市缓存配置文件 默认缓存10秒
        /// </summary>
        /// <param name="info"></param>
        /// <param name="configAreaCode">配置项的地区编码</param>
        /// <returns></returns>
        public virtual string GetCache(SnsProcessConfigInfo info, int configAreaCode)
        {
            //获取对应的地区参数
            string cacheKey = GetDicKey(info.Key, configAreaCode.Tostring());
            if (configData.ContainsKey(cacheKey))
            {
                return configData[cacheKey].value;
            }

            //没有地区参数且地区编码不为全国 获取全国参数
            cacheKey = GetDicKey(info.Key, "0");
            if (configAreaCode != 0)
            {
                if (configData.ContainsKey(cacheKey))
                {
                    return configData[cacheKey].value;
                }
            }

            //没有全国参数 则新增一条全国参数
            tb_system_config_model c = new tb_system_config_model();
            using (var pubConn = DbConn.CreateConn(BSFConfig.MainConnectString))
            {
                pubConn.Open();
                c = Get(pubConn, info.Key, 0);
                if (c == null)
                {
                    AddOrEdit(pubConn, new tb_system_config_model() { key = info.Key, value = info.DefaultValue, category = (byte)info.SystemConfigCategory, remark = info.Remark, area = 0, serviceType = (byte)info.SystemConfigServiceType, lastUpdateTime = DateTime.Now, control = (int)info.SystemConfigControlType });
                }
            }
            configCacheProvider.ReRefresh();//调用刷新配置文件线程，刷新本地缓存文件
            if (configData.ContainsKey(cacheKey))
            {
                return configData[cacheKey].value;
            }
            else
            {
                throw new SNSException(BSFAPICode.NormalError, "数据库系统配置表中无该参数，且无法设置该参数默认值！");
            }
        }

        //public virtual string Get(SnsProcessConfigInfo info, int dqbm = 0)
        //{

        //    using (var pubConn = DbConn.CreateConn(Dyd.Core.DydConfig.ConfigConnectString))
        //    {
        //        pubConn.Open();
        //        var c = Get(pubConn, info.Key, dqbm);

        //        //数据库里面没有 则取一条全国配置
        //        c = Get(pubConn, info.Key, 0);

        //        //数据库里面没有全国配置 则新增一条全国配置
        //        if (c == null)
        //        {
        //            AddOrEdit(pubConn, new tb_system_config_model() { key = info.Key, value = info.DefaultValue, category = (byte)info.SystemConfigCategory, remark = info.Remark, area = 0 });
        //            c = Get(pubConn, info.Key, 0);
        //        }

        //        if (c == null)
        //            throw new DydException(DydAPICode.NormalError,"数据库系统配置表中无该参数，且无法设置该参数默认值！");
        //        return c.value;
        //    }
        //}


        #region 底层数据库
        public virtual tb_system_config_model Get(DbConn pubConn, string f_key, int f_area)
        {
            return SqlHelper.Visit<tb_system_config_model>(ps =>
            {
                //List<ProcedureParameter> Par = new List<ProcedureParameter>();
                ps.Add("f_key", f_key);
                ps.Add("f_area", f_area);
                StringBuilder stringSql = new StringBuilder();
                stringSql.Append(@"select s.* from tb_system_config s where s.f_key=@f_key and s.f_area=@f_area");
                DataSet ds = new DataSet();
                pubConn.SqlToDataSet(ds, stringSql.ToString(), ps.ToParameters());
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    return CreateModel(ds.Tables[0].Rows[0]);
                }
                return null;
            });
        }

        public virtual bool AddOrEdit(DbConn pubConn, tb_system_config_model model)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>()
                {
					//关键词
					new ProcedureParameter("@f_key",    model.key),
					//值
					new ProcedureParameter("@f_value",    model.value),
					//备注
					new ProcedureParameter("@f_remark",    model.remark),
					//地区 0为全国
					new ProcedureParameter("@f_area",    model.area),
					//0.默认 1. 城市功能 2.城市参数 3.商家参数 4.全局参数
					new ProcedureParameter("@f_category",    model.category),
                    new ProcedureParameter("@f_servicetype",    model.serviceType),
                    new ProcedureParameter("@lastUpdateTime",  model.lastUpdateTime)  ,
                    new ProcedureParameter("@control", model.control)  ,
                    new ProcedureParameter("@isDel",  0)  

                };

            int r = pubConn.ExecuteSql("update tb_system_config set f_value=@f_value,f_remark=@f_remark,f_category=@f_category,f_servicetype=@f_servicetype,f_last_update_time=@lastUpdateTime,f_control=@control where f_key=@f_key and f_area=@f_area", Par);
            if (r == 0)
            {
                pubConn.ExecuteSql(@"insert into tb_system_config(f_key,f_value,f_remark,f_area,f_category,f_servicetype,f_control,f_last_update_time,f_isdel)
										   values(@f_key,@f_value,@f_remark,@f_area,@f_category,@f_servicetype,@control,@lastUpdateTime,@isDel)", Par);
            }
            return true;

        }

        public static List<tb_system_config_model> List(DbConn pubConn, DateTime lastTime)
        {
            return SqlHelper.Visit(ps =>
            {
                string sql = "";
                if (lastTime == default(DateTime))
                {
                    sql = "select * from tb_system_config s";
                }
                else
                {
                    sql = "select * from tb_system_config s where s.f_last_update_time>=@lastTime";
                    ps.Add("lastTime", lastTime);
                }

                DataSet ds = new DataSet();
                List<tb_system_config_model> models = new List<tb_system_config_model>();
                pubConn.SqlToDataSet(ds, sql, ps.ToParameters());
                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow m in ds.Tables[0].Rows)
                    {
                        models.Add(CreateModel(m));
                    }
                }
                return models;
            });
        }

        public static List<tb_system_config_model> List(DbConn pubConn)
        {
            return SqlHelper.Visit(ps =>
            {
                string sql = "";
                sql = "select * from tb_system_config s";
                DataSet ds = new DataSet();
                List<tb_system_config_model> models = new List<tb_system_config_model>();
                pubConn.SqlToDataSet(ds, sql, ps.ToParameters());
                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow m in ds.Tables[0].Rows)
                    {
                        models.Add(CreateModel(m));
                    }
                }
                return models;
            });
        }

        public static bool DeleteConfig(DbConn pubConn, string f_key)
        {
            return SqlHelper.Visit(ps =>
            {
                List<ProcedureParameter> Par = new List<ProcedureParameter>();
                Par.Add(new ProcedureParameter("@f_key", f_key));
                string Sql = "delete tb_system_config where f_key=@f_key";
                int rev = pubConn.ExecuteSql(Sql, Par);
                if (rev == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            });
        }
        #endregion

        #region 获取业务配置表
        /// <summary>
        /// 获取业务配置表
        /// </summary>
        /// <param name="pubConn"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public virtual List<SystemConfigModel> FGetSystemConfig(DbConn pubConn, string key, int? dqbm)
        {
            return SqlHelper.Visit(ps =>
              {
                  var par = new List<ProcedureParameter>();
                  var stringSql = new StringBuilder();
                  stringSql.Append("select t.f_key,t.f_value,t.f_area,t.f_category,t.f_control from tb_system_config t where 1=1 ");
                  if (!string.IsNullOrWhiteSpace(key))
                  {
                      string[] keys = key.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                      stringSql.Append(" and t.f_key in (");
                      for (int i = 0; i < keys.Length; i++)
                      {
                          stringSql.Append("@f_key" + i + ",");
                          par.Add(new ProcedureParameter("@f_key" + i, keys[i].Trim()));
                      }
                      stringSql.Remove(stringSql.Length - 1, 1);
                      stringSql.Append(" ) ");
                  }

                  var ds = new DataSet();
                  pubConn.SqlToDataSet(ds, stringSql + "", par);
                  List<SystemConfigModel> systemConfigList = new List<SystemConfigModel>();
                  foreach (DataRow dr in ds.Tables[0].Rows)
                  {
                      systemConfigList.Add(CreateSystemConfigModel(dr));
                  }
                  return systemConfigList;
              });
        }

        public SystemConfigModel CreateSystemConfigModel(DataRow dr)
        {
            return new SystemConfigModel()
            {
                f_key = dr["f_key"].ToString(),
                f_value = dr["f_value"].ToString(),
                f_area = Convert.ToInt32(dr["f_area"]),
                f_category = Convert.ToInt32(dr["f_category"]),
                f_Control = Convert.ToInt32(dr["f_control"])
            };
        }

        #endregion
    }
}