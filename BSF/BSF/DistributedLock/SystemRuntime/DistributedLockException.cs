﻿using System;

namespace BSF.DistributedLock.SystemRuntime
{
    /// <summary>
    /// 分布式锁错误
    /// </summary>
    public class DistributedLockException : Exception
    {
        public DistributedLockException(string message)
            : base(message)
        {

        }
    }
}
