﻿using System;
using System.Collections;
using System.Web;

namespace BSF.Caches
{
    /// <summary>
    /// 系统内置缓存帮助类
    /// </summary>
    public class RuntimeCacheHelper
    {
        /// <summary>
        /// 获取数据缓存
        /// </summary>
        /// <param name="CacheKey">键</param>
        public static object GetCache(string CacheKey)
        {
            System.Web.Caching.Cache objCache = HttpRuntime.Cache;
            return objCache[CacheKey];
        }

        /// <summary>
        /// 获取缓存对象
        /// </summary>
        /// <typeparam name="T">缓存对象</typeparam>
        /// <param name="key">缓存Key</param>
        /// <returns></returns>
        public static T GetCache<T>(string CacheKey) where T : class
        {
            object obj = GetCache(CacheKey);
            return obj == null ? default(T) : (T)obj;
        }


        /// <summary>
        /// 获取缓存，如果不存在，加载缓存
        /// </summary>
        /// <typeparam name="T">缓存对象T</typeparam>
        /// <param name="key">缓存Key</param>
        /// <param name="slidingExpiration">过期时间</param>
        /// <param name="acquire">如果缓存不存在，加载</param>
        /// <returns>缓存对象</returns>
        public static T GetCache<T>(string key, TimeSpan slidingExpiration, Func<T> acquire) where T : class
        {
            if (IsSet(key))
            {
                return GetCache<T>(key);
            }
            else
            {
                var result = acquire();
                SetCache(key, result, slidingExpiration);
                return result;
            }
        }

        /// <summary>
        /// 设置数据缓存
        /// </summary>
        public static void SetCache(string CacheKey, object objObject)
        {
            System.Web.Caching.Cache objCache = HttpRuntime.Cache;
            objCache.Insert(CacheKey, objObject);
        }

        /// <summary>
        /// 设置数据缓存
        /// </summary>
        public static void SetCache(string CacheKey, object objObject, TimeSpan Timeout)
        {
            System.Web.Caching.Cache objCache = HttpRuntime.Cache;
            objCache.Insert(CacheKey, objObject, null, DateTime.MaxValue, Timeout, System.Web.Caching.CacheItemPriority.NotRemovable, null);
            //System.Web.Caching.Cache cache = HttpRuntime.Cache;
            ////滑动过期时间的时间为1分钟，只要一直刷新一直有，如果刷新间隔超过一分钟的缓存就没啦
            //cache.Insert("MyData1", "1", null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromSeconds(30));//new TimeSpan(0, 0, 60)
            //cache.Insert("MyData2", "2", null, DateTime.Now.AddMinutes(2), System.Web.Caching.Cache.NoSlidingExpiration);
            ////绝对过期时间的时间为30s，过完30s之后就没有缓存
            //cache.Insert("Tiger", "老虎", null, DateTime.Now.AddSeconds(30), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.High, null);
        }

        /// <summary>
        /// 设置数据缓存
        /// </summary>
        public static void SetCache(string CacheKey, object objObject, DateTime absoluteExpiration, TimeSpan slidingExpiration)
        {
            System.Web.Caching.Cache objCache = HttpRuntime.Cache;
            objCache.Insert(CacheKey, objObject, null, absoluteExpiration, slidingExpiration);
        }

        /// <summary>
        /// 移除指定数据缓存
        /// </summary>
        public static void RemoveAllCache(string CacheKey)
        {
            System.Web.Caching.Cache _cache = HttpRuntime.Cache;
            _cache.Remove(CacheKey);
        }

        /// <summary>
        /// 移除全部缓存
        /// </summary>
        public static void RemoveAllCache()
        {
            System.Web.Caching.Cache _cache = HttpRuntime.Cache;
            IDictionaryEnumerator CacheEnum = _cache.GetEnumerator();
            while (CacheEnum.MoveNext())
            {
                _cache.Remove(CacheEnum.Key.ToString());
            }
        }

        /// <summary>
        /// 缓存
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <returns></returns>
        public static bool IsSet(string key)
        {
            object obj = GetCache(key);
            return obj == null ? false : true;
        }
    }
}