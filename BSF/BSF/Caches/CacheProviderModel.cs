﻿using System;

namespace BSF.Caches
{
    public class CacheProviderModel
    {
        /// <summary>
        /// 配置Key
        /// </summary>
        public string ConfigKey { get; set; }

        /// <summary>
        /// 缓存回调数据刷新函数
        /// </summary>
        public Action RefreshMethod { get; set; }

        /// <summary>
        /// 间隔时间 秒为单位,（系统自身刷新频率为5秒，故可能有5秒延迟）
        /// </summary>
        public int IntervalTime { get; set; }

        /// <summary>
        /// 上一次缓存刷新时间
        /// </summary>
        public DateTime LastRefreshTime { get; set; }
    }
}
