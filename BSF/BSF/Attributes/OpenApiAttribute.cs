﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSF.Attributes
{
    /// <summary>
    /// openapi特性 直接公开方法至controller层
    /// </summary>
    public class OpenApiAttribute : System.Attribute 
    {

    }
}
