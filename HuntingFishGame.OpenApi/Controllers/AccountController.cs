﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.IO;
using Hangfire;
using Hangfire.Storage;
using Microsoft.CodeAnalysis;
using Microsoft.Extensions.Options;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HuntingFishGame.OpenApi.Controllers
{
    /// <summary>
    /// 调试:右击docker-compose设为启动项,然后选择需要调试的项目,点击调试项目
    /// 调试地址:
    /// http://localhost:12225/swagger/v1/swagger.json
    /// http://localhost:12225/swagger/ui/index.html
    /// </summary>
    [Route("account")]
    public class AccountController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ILogger<AccountController> _logger;

        /// <summary>
        /// 读取appsettings.json里面的appsetting配置
        /// </summary>
        private readonly IOptions<AppSettings> settings;

        public AccountController(ILogger<AccountController> logger, IOptions<AppSettings> settings)
        {
            _logger = logger;
            this.settings = settings;
        }

        //[Route("Index")]
        //// GET: /<controller>/
        //public IActionResult Index()
        //{
        //    _logger.LogInformation("你访问了首页");
        //    _logger.LogWarning("警告信息");
        //    _logger.LogError("错误信息");

        //    //ILoggerRepository repository = LogManager.CreateRepository("NETCoreRepository");
        //    //// 默认简单配置，输出至控制台
        //    ////BasicConfigurator.Configure(repository);
        //    //XmlConfigurator.Configure(repository, new FileInfo("log4net.config"));
        //    //ILog log = LogManager.GetLogger(repository.Name, "NETCorelog4net");

        //    //log.Info("NETCorelog4net log");
        //    //log.Info("test log");
        //    //log.Error("error");
        //    //log.Info("linezero");
        //    return View();
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// ASP.NET Core 1.0 中使用 Swagger 生成文档 http://www.cnblogs.com/Irving/p/5205298.html
        /// Asp.net core WebApi 使用Swagger生成帮助页 http://www.cnblogs.com/suxinlcq/p/6757556.html
        /// 更多关于Swagger的用法可以参考：https://github.com/domaindrivendev/Swashbuckle.AspNetCore 以及微软文档：https://docs.microsoft.com/zh-cn/aspnet/core/tutorials/web-api-help-pages-using-swagger
        /// </remarks>
        [HttpGet]
        [Route("info")]
        public async Task<ActionResult> Info()
        {
            //.NET Core开源组件:后台任务利器之Hangfire http://www.0735sh.com/chenug/p/6655636.html
            using (var connection = JobStorage.Current.GetConnection())
            {
                var storageConnection = connection as JobStorageConnection;
                if (storageConnection != null)
                {
                    //立即启动
                    var jobId = BackgroundJob.Enqueue(() => Console.WriteLine("Fire-and-forget!"));
                }
            }
            //BackgroundJob.Enqueue<SomeClass>(i => i.SomeMethod(someParams));
            ////设置任务队列
            //[Queue("test")]
            //public void TestQueue() { }


            return await Task.Run(() =>
            {
                return Json(new { name = "irving", age = 25 });
            }).ContinueWith(t => t.Result);

        }

    }
}
